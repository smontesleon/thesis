\section{Conclusions}
\label{sec:conclusion}
This thesis aimed to address several research objectives and make significant contributions to the identification of FLOSS license diversity.

To achieve these objectives, a Systematic Literature Review (SLR) and an empirical study were conducted, forming the core of the research:

\begin{itemize}
	\item A case study on the practice of Empirical Software Engineering (ESE) was conducted through an SLR, focusing on the evaluation, data collection methods, and other aspects related to the identification and classification of FLOSS licenses. This analysis reviewed a set of \num{1790} articles, from which, through specific strategies, $43$ relevant studies were selected.
	\item The SLR highlights some of the main challenges in identifying FLOSS licenses, as well as the techniques and methodologies employed. Publications in this field suffer from reproducibility issues, mainly due to the lack of availability of the tools and datasets used. As a result of the SLR, an artifact (dataset) called ``Metrics\_all\_Documents.ods''\footnote{\url{https://shorturl.at/bjlv1}} was generated, facilitating the extraction of relevant data.
	
	\item Regarding FLOSS license identification, most researchers rely on existing tools, while only a limited number have developed new tools, and few research groups are involved in this area.
	\item During the manuscript analysis, a total of $ 42 $ software tools were identified, of which $ 38 $ are accessible for use.
	\item Regarding FLOSS license classification, tools like Ninka, FOSSology, and Scancode were identified as capable of performing this task. However, the SLR focused on the use of ML techniques for classification, and only one article specifically addresses this topic, indicating that research in this area is still in its early stages.
	
	\item A large-scale dataset of open-source license texts from the ``Software Heritage Initiative''~\cite{Gonzalez2023} was obtained. This collection consists of approximately $\DataBlobCountApproxM{}$ million documents. These documents include files whose names are related to software license terms. The associated metadata has simplified the study by providing detailed information on various aspects of the licenses.
	% (repeated)
	% \item The manually annotated random sample is suitable for both validation and training purposes. Its detailed description not only facilitates replication but also enables its use in further research by third parties.
	\item The manually annotated random sample is suitable for validation and training, and its detailed description facilitates replication and future use in research. Initial examples from the dataset confirm its usefulness for the intended purpose.
	
	\item This thesis has identified that FLOSS license diversity is the result of the evolution and adaptation of the free software ecosystem to various legal, cultural, and technical contexts. The analysis of this diversity has provided a deeper understanding of the complexity of the current FLOSS licensing landscape and its impact on the development and sustainability of free software.
	\item This thesis has identified that the diversity of FLOSS licenses is the result of the evolution and adaptation of the free software ecosystem to various legal, cultural, and technical contexts. The analysis and classification of a large and diverse set of FLOSS licenses, as reflected in~\cite{GonzalezBarahona2023}, have provided a deeper understanding of the complexity of the current FLOSS licensing landscape and its impact on the development and sustainability of free software.
	
\end{itemize}
However, this research faces certain limitations that should be considered. Although the dataset utilized encompasses a significant number of documents from Software Heritage, it might not include less common licenses, thereby limiting the comprehensiveness of the results. Additionally, reliance on existing tools such as ScanCode imposes constraints in terms of precision and scope. Finally, while the manual annotation approach proves effective for detailed analysis, it demands substantial resources and time, restricting its application to a larger number of documents within the Software Heritage collection.

\section{Recommendations}
\label{sec:recommendation}
Based on the results obtained in this research, the following recommendations are presented to improve the management and understanding of FLOSS licenses. These proposals are aimed at developers and project managers, with the goal of establishing good practices in the writing, reading, and interpretation of licenses.

\subsection{Writing licenses}
\label{subsec:writing_license}

For proper license drafting, it is suggested:

\begin{itemize}
	\item Use recognized standards, such as those provided by SPDX or the Open Source Initiative (OSI), instead of creating licenses that diverge significantly from existing ones.
	\item If a custom license is required, clearly inform about the changes made compared to standard licenses, and assign a specific title to facilitate its identification.
	\item Verify if a similar license already exists before drafting a new one, to avoid redundancies or confusion.
	\item Properly reference the license within the project's files, ensuring its correct location and identification by users.
\end{itemize}

\subsection{Reading and interpreting licenses}
\label{subsec:reading_license}

To facilitate the reading and interpretation of licenses, it is recommended:

\begin{itemize}
	\item Thoroughly read the license to fully understand its terms and conditions.
	\item Verify if the license is recognized as a standard by SPDX or certified by the OSI.
	\item Check the license's presence in official listings, such as those of SPDX or OSI, to validate its legitimacy.
	\item Determine whether the license is truly open source and meets the project's objectives, avoiding incompatible restrictions.
	\item Review the correct project files, such as \texttt{LICENSE} or \texttt{COPYING}, where the license is typically specified.
\end{itemize}

These recommendations aim to establish a practical framework for managing FLOSS licenses, promoting uniformity and adherence to good practices, both in their drafting and analysis.




\section{Future work}
\label{sec:Trabajofuturo}

%As future work, the following is proposed: i) update the SLR to submit to a journal indexed in JCR, ii) maintain the dataset updated in collaboration with Software Heritage, and iii) evaluate licenses using generative AI (e.g., ChatGPT) in an automated manner on the Software Heritage document collection, to compare the results with the manually annotated dataset and improve the validation process.
As future work, the following actions are planned: i) updating the SLR for submission to a JCR-indexed journal, ii) maintaining the dataset in collaboration with Software Heritage, and iii) evaluating licenses using generative artificial intelligence (e.g., ChatGPT) to automate the analysis of the Software Heritage document collection. This will enable a comparison of the results with the manually annotated dataset and enhance the validation process.

These actions address the limitations identified in this research, particularly the reliance on existing tools and the resource-intensive nature of manual annotation. By developing new automated approaches based on advanced generative artificial intelligence techniques, the aim is to improve both the identification and classification of FLOSS licenses, enabling a more comprehensive and representative analysis of license diversity.

