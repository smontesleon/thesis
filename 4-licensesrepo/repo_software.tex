\section{Introducción}
\label{sec:intro_repo}
%A lo largo de la historia, el ser humano ha demostrado un interés constante por el almacenamiento y la preservación del conocimiento. Desde los primeros grabados rupestres hasta la sofisticada infraestructura de almacenamiento digital en la nube (``\textit{Cloud}''), la capacidad de preservar y transmitir información ha sido clave para el desarrollo de las sociedades.
A lo largo de la historia, la humanidad ha mostrado un interés persistente en la conservación y almacenamiento del conocimiento. Desde los antiguos grabados en rocas hasta las avanzadas plataformas de almacenamiento en la nube \textit{``Cloud''}, la habilidad de preservar y compartir información ha sido fundamental para el progreso de las civilizaciones.

%En el caso de los proyectos FLOSS, el objetivo no es solo la creación de herramientas tecnológicas, sino también la promoción de la colaboración y la participación de los usuarios en el proceso de desarrollo. Plataformas de desarrollo colaborativo como GitHub, GitLab y Bitbucket funcionan como ``repositorios digitales'', donde se almacena y comparte de manera accesible y transparente el código fuente de estos proyectos.

En los proyectos FLOSS, el propósito no se limita únicamente a la creación de herramientas tecnológicas, sino que también busca fomentar la colaboración y la participación activa de los usuarios en el proceso de desarrollo. Plataformas colaborativas como GitHub, GitLab y Bitbucket actúan como ``repositorios digitales'', permitiendo el almacenamiento y la distribución del código fuente de manera accesible y transparente para todos.

Sin embargo, este proceso presenta varios retos que deben ser resueltos. En primer lugar, la necesidad de almacenar el software para su descarga, originalmente facilitada por protocolos como FTP. En segundo lugar, el reto de trabajar de manera colaborativa, que en sus inicios se gestionaba en sitios como SourceForge, donde se distribuía el software libre. Finalmente, surge la necesidad de preservar no solo los resultados de la investigación, sino también el propio software, tarea que actualmente se aborda a través de iniciativas como Software Heritage.

A continuación, abordaremos estos tres problemas en detalle dentro del ecosistema FLOSS, y como complemento adicional ``repositorios de datos''.

\section{Repositorios de distribución}
\label{sec:repos_distribution}
Los repositorios de distribución juegan un papel clave en la difusión del software, ya que aseguran que esté disponible para su descarga y uso por cualquier persona. A lo largo de los años, han surgido plataformas especializadas en la distribución de software libre y de código abierto, como SourceForge, que fue pionera en este ámbito. Otros repositorios más enfocados en la distribución incluyen plataformas como FossHub, GNU Savannah y el portal de software libre de Debian, las cuales se centran exclusivamente en ofrecer software para descarga, asegurando que los usuarios puedan acceder a versiones estables, actualizaciones y archivos de instalación~\cite{Brickner2005}.
Los repositorios de distribución están optimizados para ofrecer software final listo para el usuario~\cite{Cen2016}. También permiten la integración con sistemas de gestión de paquetes, facilitando la instalación automatizada de software en distribuciones GNU/Linux como Debian\footnote{\url{https://www.debian.org/distrib/}}, Ubuntu, Fedora, SourceForge, Google Play Store~(software para móviles), F-Droid~(dispositivos Android), etc.

A continuación, en la Tabla~\ref{table:distribution_repositories}, se resumen las principales características de los repositorios de distribución discutidos en esta sección.

\begin{table}[htbp]
	\centering
	\caption[Repositorios de distribución]{Características principales de los repositorios de distribución.}
	\label{table:distribution_repositories}
	\renewcommand{\arraystretch}{1.2}
	\adjustbox{width=\textwidth}{
		\begin{tabular}{lccl}
			\toprule
			\textbf{Repositorio} & \textbf{Año de creación} & \textbf{Plataformas soportadas} & \textbf{Características destacadas} \\
			\midrule
			Ubuntu & 2004 & GNU/Linux & Gestión de paquetes APT/dpkg. \\
			SourceForge & 1999 & Multiplataforma & Alojamiento de proyectos FLOSS. \\
			Google Play Store & 2008 & Android & Distribución de aplicaciones móviles. \\
			F-Droid & 2010 & Android & Repositorio de software FLOSS para móviles. \\
			\bottomrule
	\end{tabular}}
\end{table}


\subsection{Ubuntu}
\label{subsec:ububtu}
\textbf{Ubuntu},\footnote{\url{https://ubuntu.com/}} al igual que \textbf{Debian},\footnote{\url{https://www.debian.org/}} cuenta con repositorios que almacena paquetes y programas, sean propios o de terceros, para que el usuario pueda instalarlos en distribuciones GNU/Linux; que tengan gestión de paquetes APT/dpkg.
 
\subsection{SourceForge}
\label{subsec:sourceforce}
\textbf{SourceForge}\footnote{\url{SourceForge.net}} es una plataforma web dedicada al alojamiento de proyectos de software libre y de código abierto, lanzada en 1999. En solo dos años, ya apoyaba el desarrollo de $ 27000 $ proyectos de software y tenía más de 4 millones de desarrolladores activos a nivel global~\cite{Augustin2002}.


\subsection{Google Play Store}
\label{subsec:googleplay}
\textbf{Google Play Store}\footnote{\url{https://play.google.com/}} es una plataforma dedicada a la distribución de aplicaciones para dispositivos móviles que utilizan el sistema operativo Android. Administrada por Google, esta tienda permite a los usuarios explorar y descargar aplicaciones, juegos, música, libros, revistas y películas. Ha estado operativa desde su lanzamiento el 28 de agosto de 2008.
  

\subsection{F-Droid}
\label{subsec:fdroid}
\textbf{F-Droid}\footnote{\url{https://f-droid.org/}} es un repositorio de software que tiene a disposición una gran cantidad de aplicaciones de Android maduras y populares que son gratuitas y de código abierto. Muchas aplicaciones en F-Droid se mantienen activamente y también se publican en Google Play Store, y son utilizadas por muchos usuarios~\cite{Zeng2019}, su lanzamiento el 29 de septiembre de 2010.

\subsection{Repositorios de paquetes software }
\label{subsec:Repsoftwarepaquetes}
Los sistemas grandes no se desarrollaron como una estructura monolítica, sino que se componen de varios componentes de software. Estos componentes~(ya sea código de biblioteca o programas ejecutables) generalmente se denominan \textit{paquetes} y a menudo constan internamente de muchos módulos.

Los ``paquetes'' son un medio de distribución de código, por lo que si bien los módulos se pueden modificar in situ, los paquetes tienen versiones para respaldar su evolución. Los metadatos del paquete expresan cómo los paquetes dependen unos de otros, a menudo a través de rangos de versiones. Estos metadatos se utilizan en la gestión de paquetes (instalación, actualización o eliminación de paquetes). Los paquetes se comparten a través de ``repositorios centrales'' y la resolución de dependencias decide qué paquetes del repositorio deben estar disponible en el sistema de un usuario al instalar o actualizar~\cite{Florisson2017}.

Muchas distribuciones de FLOSS del Sistema Operativo GNU/Linux como RedHat, Debian, Ubuntu, OpenSuse, Fedora, etc., son repositorios con una fuerte relación a un sistema de administración de paquetes~\cite{Claes2015}, conocidos como~(Gestión y/o dependencias de paquetes).

La mayor parte de sistema de gestión de paquetes actualmente surge de un sistema empleado por la distribución Debian GNU/Linux. Los archivos del paquete Debian suelen tener la extensión~(.deb) y que normalmente existen en repositorios que son colecciones de paquetes que se encuentran en línea o en medios físicos, como discos CD-ROM. Los paquetes normalmente están en formato binario precompilado; por tanto, la instalación es rápida y no requiere compilación de software~\cite{Hegde2021}, como se ha mencionado anteriormente con respecto a los repositorios como por ejemplo: PyPI, (fuente) Debian, NPM. 

A continuación, en la Tabla~\ref{table:package_repositories} proporciona una visión comparativa de los repositorios más relevantes, incluyendo el lenguaje al que están asociados, sus gestores principales y las características que los hacen únicos.

\begin{table}[htbp]
	\centering
	\caption[Repositorios de paquetes]{Comparativa de repositorios de paquetes.}
	\label{table:package_repositories}
	\renewcommand{\arraystretch}{1.2}
	\adjustbox{width=\textwidth}{
		\begin{tabular}{lcccl}
			\toprule
			\textbf{Repositorio} & \textbf{Año de creación} & \textbf{Lenguaje asociado} & \textbf{Gestor de paquetes} & \textbf{Características destacadas} \\
			\midrule
			PyPI                & 2003 & Python    & pip          & Repositorio oficial de paquetes Python. \\
			CRAN                & 1997 & R         & install.packages & Biblioteca extensiva de paquetes estadísticos. \\
			npm                 & 2010 & JavaScript & npm          & Principal repositorio para Node.js. \\
			Maven Central       & 2004 & Java      & Maven        & Gestión de dependencias para proyectos Java. \\
			RubyGems            & 2004 & Ruby      & gem          & Biblioteca oficial para Ruby. \\
			CPAN                & 1995 & Perl      & cpan         & Repositorio principal para Perl. \\
			Cargo               & 2014 & Rust      & cargo        & Gestión de dependencias para Rust. \\
			\bottomrule
	\end{tabular}}
\end{table}



\subsubsection{Python Package Index~(PyPI)}
\label{subsubsec:pypi}
\textbf{Python Package Index~(PyPI)} es el repositorio oficial de software donde se alojan aplicaciones y paquetes de terceros desarrollados en el lenguaje de programación Python.
PyPI se creó en 2001 y empezó a funcionar en 2003. El proyecto se inspiró en los proyectos de la Comprehensive TeX Archive Network (CTAN) de la comunidad TeX y la Comprehensive Perl Archive Network~(CPAN) de la comunidad Perl ~\cite{bommarito2019}. El repositorio tiene alrededor de \num{532682} proyectos y ofrece mucha información y/o metadatos sobre el desarrollo de los mismos~\cite{Pypi2024}.

\subsubsection{RPM Package Manager (RPM)}
\label{subsubsec:rpm}
El \textbf{RPM Package Manager~(RPM)}~(inicialmente conocido como Red Hat Package Manager, ahora un acrónimo recursivo) es un sistema de gestión de paquetes FLOSS y un formato de paquete estándar en la Linux Standard Base, con la extensión .rpm. Aunque RPM fue creado originalmente para Red Hat Linux, en la actualidad es utilizado en diversas distribuciones de Linux como Fedora, CentOS, OpenSUSE, Open-Mandriva y Oracle Linux.

\subsubsection{Node Package Manager~(npm)}
\label{subsubsec:nmp}
\textbf{Node.js} fue creado con la idea de que la reutilización de código en el desarrollo de software trae consigo múltiples beneficios, tales como una mayor calidad del software y un incremento en la productividad. Para apoyar esta filosofía, se introdujo la plataforma Node Package Manager~(npm), que incentiva el uso de paquetes triviales~\cite{Abdalkareem2017}. Esta práctica ha ganado gran popularidad en el ámbito del desarrollo de software y es ampliamente adoptada en aplicaciones que utilizan Node.js.


\section{Forjas de desarrollo}
\label{subsec:repos_development}
Las forjas, también conocidas como repositorios de desarrollo, son plataformas fundamentales dentro del ecosistema FLOSS \cite{Constantopoulos1993}, ya que proporcionan las herramientas necesarias para el desarrollo, evolución y distribución del software en un entorno colaborativo \cite{Squire2012}. Estas plataformas no solo almacenan el código fuente, sino que también ofrecen control de versiones, lo que permite que múltiples desarrolladores colaboren en un mismo proyecto sin generar conflictos ~\cite{Zolkifli2018}. Herramientas como Git, junto con plataformas como GitHub, GitLab y Bitbucket, permiten un seguimiento detallado de los cambios en el código, facilitando la revisión de contribuciones, la integración de nuevas funcionalidades y la resolución de problemas.


Aunque su principal función es apoyar el desarrollo colaborativo, las forjas también permiten la distribución de software, facilitando que los usuarios descarguen versiones oficiales o incluso compilen el software directamente desde el código fuente. Esta dualidad de desarrollo y distribución convierte a las forjas en espacios integrales donde tanto los desarrolladores como los usuarios pueden interactuar con el software en distintas fases de su ciclo de vida.

Además, las forjas no solo se utilizan para el desarrollo de software. También pueden ser empleadas en la gestión de proyectos de diversa índole, como la creación de documentación, la colaboración en recursos educativos, o incluso en proyectos científicos. Esto amplía su alcance más allá del código, permitiendo la colaboración abierta en una variedad de iniciativas.

Los repositorios de desarrollo de software~(código fuente) contienen una gran cantidad de información histórica de software, datos que pueden incluir información valiosa sobre el código fuente, defectos y otros problemas como nuevas funciones; por lo que se surgió un campo muy importante de estudio denominado \textit{Mining Software Repositories}~(MSR) en el área de la Ingeniería de Software~(SE)~\cite{defarias2016}.

A continuación, la Tabla~\ref{table:development_forges} presenta una comparativa de las forjas de desarrollo que a continuación se discuten, destacando sus funcionalidades principales.

\begin{table}[htbp]
	\centering
	\caption[Forjas de desarrollo]{Comparativa de forjas de desarrollo.}
	\label{table:development_forges}
	\renewcommand{\arraystretch}{1.2}
	\adjustbox{width=\textwidth}{
		\begin{tabular}{lccl}
			\toprule
			\textbf{Forja} & \textbf{Año de creación} & \textbf{Control de versiones} & \textbf{Características destacadas} \\
			\midrule
			GitHub & 2008 & Git & Plataforma colaborativa con integración CI/CD. \\
			GitLab & 2011 & Git & Gestión de proyectos y ciclo de vida DevOps. \\
			Bitbucket & 2008 & Git y Mercurial & Integración con Jira y Trello. \\
			\bottomrule
	\end{tabular}}
\end{table}


\subsection{GitHub}
\label{subsubsec:github}
\textbf{GitHub}\footnote{\url{github.com }} es una forja colaborativa centrada en el desarrollo de proyectos de software, que emplea Git como sistema de control de versiones distribuido. Fue creada en 2008 utilizando el framework Ruby on Rails. En la actualidad, es propiedad de Microsoft, que adquirió la plataforma el 4 de junio de 2018.

GitHub tiene usuarios, pero a diferencia de otras forjas, los proyectos de GitHub~(llamados ``repositorios'' o ``repos'') sólo existen cuando los usuarios los crean o los bifurcan de otros. Cada repositorio público pertenece a un usuario determinado y cualquier otro usuario puede bifurcarlo y modificarlo, momento en el cual los cambios pueden ser aceptados para su uso por parte del usuario original o no~\cite{Squire2014}. Como dato primordial GitHub, en sus inicios permitía a un usuario iniciar un repositorio público sin especificar una licencia de software, actualmente cuando se crea sugiere colocar una licencia para el código y aún así alrededor del 50\% no tiene licencia.

\subsection{GitLab}
\label{subsubsec:gitlab}
GitLab es un una forja web de desarrollo software de forma rápida, provee servicios de control de versiones~(Git) entre otros,y está basada en GitLab software que se publicó por primera vez en octubre de 2011. Desde entonces se actualiza cada día veintidós del mes. Fue lanzado bajo la licencia del MIT. Se alojó en GitHub, pero desde enero de 2014, su principal fuente de alojamiento es \url{gitlab.com}. GitLab fue fundado por Dmitriy Zaporozhets en 2013~\cite{van2014}.

GitLab comenzó como un administrador de repositorio Git de código abierto basado en la web y rápidamente creció hasta convertirse en una aplicación única para todo el ciclo de vida de DevOps.\footnote{\url{https://es.wikipedia.org/wiki/DevOps}} GitLab proporciona a las organizaciones gestión de código, proyectos y operaciones, junto con herramientas de seguridad y monitoreo~\cite{vesselov2019}.

\subsection{Bitbucket}
\label{subsubsec:bitbucket}
Bitbucket es un una forja web de desarrollo software, provee servicios de control de versiones (Git), Mercurial entre otras como Jira y Trello. Es similar a GitHub, viene funcionando desde el año 2008 y es muy utilizada por muchos desarrolladores~\cite{yang2023}. En cuestión de los términos de desarrollo algunos son propios de Git, mientras que otros son específicos de Bitbucket. 

\section{Repositorios de preservación}
\label{sec:preservation_floss}
La preservación del software FLOSS es fundamental para garantizar que los proyectos se mantengan accesibles y utilizables a lo largo del tiempo. A diferencia de los repositorios de desarrollo y distribución, que se centran en la creación y acceso inmediato al software, los repositorios de preservación aseguran que el código fuente y su documentación estén almacenados de forma segura y disponibles para futuras generaciones. Este tipo de preservación es crucial no solo para evitar la pérdida de proyectos que dejan de ser mantenidos, sino también para permitir que su estudio y reutilización continúe, tanto en el ámbito académico como en el tecnológico.

Además, existen múltiples repositorios y bibliotecas digitales dedicadas específicamente a la preservación de software. Iniciativas como \textit{Software Heritage}, el \textit{Internet Archive Software Collection}, el \textit{NASA's Software Catalog}, \textit{Papers With Code} y el \textit{Google Code Archive} ofrecen plataformas accesibles donde los usuarios pueden consultar y descargar software preservado. Estas bibliotecas no solo se enfocan en conservar el código fuente, sino que también proporcionan recursos valiosos para la investigación y la reutilización de proyectos FLOSS. Algunos de estos repositorios permiten acceder al software sin necesidad de registro, lo que facilita aún más su uso por parte de desarrolladores, investigadores y estudiantes~\cite{matthews2009}~\cite{matthews2010}.

Gracias a estas iniciativas, la preservación digital garantiza que el conocimiento y los recursos almacenados sigan siendo accesibles a lo largo del tiempo. Al actuar como ``bibliotecas de software'', estos repositorios no solo ayudan a recuperar versiones anteriores de proyectos, sino que también contribuyen al análisis histórico y científico del desarrollo del software FLOSS. A continuación, se revisarán algunas de las principales iniciativas que lideran los esfuerzos de preservación en este ámbito. 


A continuación, en la Tabla~\ref{table:preservation_repositories}, se comparan los principales repositorios de preservación, destacando sus objetivos, años de creación y características principales.

\begin{table}[htbp]
	\centering
	\caption[Repositorios de preservación]{Comparativa de repositorios de preservación.}
	\label{table:preservation_repositories}
	\renewcommand{\arraystretch}{1.2}
	\adjustbox{width=\textwidth}{
		\begin{tabular}{lccl}
			\toprule
			\textbf{Repositorio} & \textbf{Año de creación} & \textbf{Objetivo principal} & \textbf{Ejemplo de uso} \\
			\midrule
			Software Heritage & 2015 & Preservación de código fuente & Biblioteca de software para reproducibilidad científica. \\
			Internet Archive & 1996 & Archivo digital general & Almacenamiento de software histórico. \\
			Papers With Code & 2018 & Enlace entre artículos académicos y código fuente & Reproducibilidad en Machine Learning. \\
			\bottomrule
	\end{tabular}}
\end{table}



\subsection{Biosig}
\label{subsubsec:Biosig}
\textbf{Biosig}\footnote{\url{https://biosig.sourceforge.net/}} fue iniciado en 2003 y es una biblioteca de software libre y de código abierto que ofrece herramientas para el procesamiento de señales biomédicas~\cite{vidaurre2011}.

Esta biblioteca incluye herramientas que permiten el análisis de bioseñales como electroencefalograma~(EEG), electrocorticograma~(ECoG), electrocardiograma~(ECG), electro-oculograma~(EOG), electromiograma~(EMG), señales respiratorias, entre otras. Las principales áreas de aplicación de estas herramientas son: neuroinformática, interfaces cerebro-computadora~(BCI), neurofisiología, psicología, sistemas cardiovasculares e investigación del sueño. El objetivo del proyecto BioSig es apoyar la investigación en el procesamiento de señales biomédicas proporcionando herramientas de software de código abierto aplicables a una variedad de campos.


\subsection{Papers With Code}
\label{subsubsec:PaperswithCode}
Papers With Code\footnote{\url{paperswithcode.com}} es un repositorio web centralizado creado en el 2018 que alberga artículos académicos con énfasis en \textit{Machine Learning}~(ML) que comparten su software de respaldo para que los experimentos puedan reproducirse de manera exacta. 

La iniciativa ahora proporciona clasificación de categorías y referencias de códigos para artículos en la base de datos \textit{arXiv}~\cite{daw2022}.\footnote{\url{arxiv.org}~(arXiv es un archivo en línea para las prepublicaciones de artículos científicos en el campo de las matemáticas, física, ciencias de la computación y biología cuantitativa)}

\subsection{Software Heritage}
\label{subsubsec:Repsoftheritage}
Software Heritage\footnote{\url{softwareheritage.org}} es un proyecto de investigación con la misión de recopilar y preservar todo el código fuente abierto jamás disponible en el mundo. La iniciaron en París dos investigadores y académicos: Roberto Di Cosmo y Stefano Zacchiroli, en el año 2015. 

La intención de la iniciativa fue evitar una pérdida significativa de legado digital que podría servir como una infraestructura eficaz para preservar el software en el futuro como una especie de Biblioteca de Alejandría. Roberto Di Cosmo y su equipo consiguieron que en 2017 que la UNESCO reconociera al ``software'' como patrimonio cultural de la humanidad, equiparándolo a la música y la literatura en términos de preservación. Software Heritage se dedica precisamente a este propósito: ``preservar el software FLOSS''~\cite{cacm-2018-software-heritage}.

El principal objetivo de la plataforma Software Heritage es recopilar, organizar y preservar los códigos fuente de todo el software disponible en Internet, ya sea sistemas de código abierto pequeños o grandes. Esto se hace mediante el uso de métodos de seguimiento y guardado, que ayudan a obtener y mantener una réplica de todas las versiones disponibles públicamente del programa en una multitud de lugares de almacenamiento que hayan usado un sistema de control de versiones como: Git, Mercurial, Subversion, etc.

Hemos enfatizado en apartados anteriores que la preservación contribuye a un proceso de creación de conocimiento; sin embargo, también cabe señalar que la preservación fomenta la transparencia, la reproducibilidad y la colaboración en el ámbito del desarrollo de software. Por lo tanto, Software Heritage se crea para salvaguardar el software garantizando que toda la información y el ingenio integrados en los códigos permanezcan accesibles durante generaciones. La iniciativa no solo se centra en archivar código fuente, sino que también ofrece herramientas valiosas que son útiles para investigadores, desarrolladores y otras partes interesadas que buscan o analizan código archivado~\cite{DiCosmo2017}.

Software Heritage reune la mayor colección de código fuente de software disponible públicamente, con un total de 186 millones de orígenes de software públicos, incluidos repositorios públicos de Git (de GitHub y GitLab), distribuciones de software libre (por ejemplo, Debian) y repositorios de administradores de paquetes (por ejemplo, PyPI, NPM). 

\section{Repositorios de conjuntos de datos}
\label{sec:repsodatossoftware}
Los repositorios de software, al contener una gran cantidad de información histórica sobre el desarrollo, distribución y actualización de software, entre otros aspectos, son adecuados para llevar a cabo MSR, lo que permite obtener diversos conjuntos de datos~\cite{robbes2011}. A partir de estos conjuntos de datos es posible obtener métricas que describan la evolución de estos procesos en términos de resultados esperados, como el tiempo, el costo, la calidad, entre otros~\cite{ramirez2010modelo}.

El campo de estudio de ``Minería de Repositorios de Software''~(MSR) ha permitido a la Ingeniería de Software~(SE) realizar nuevos estudios empíricos. Uno de los primeros estudios empíricos de desarrollo de software fue el estudio sobre el OS/360 de IBM en los años 70~\cite{Lehman1980}. Este estudio fue el origen las denominadas ``leyes de Lehman'' sobre la evolución del software~\cite{Lehman1985}. En este contexto, desde 2004 se celebra la Conferencia sobre MSR~(MSRConf), la cual proporciona un espacio para publicar y discutir dichos estudios~\cite{defarias2016}. 

Es importante destacar que, para llevar a cabo MSR, la industria y los investigadores han desarrollado herramientas de software para recopilar, obtener e interpretar datos, es decir, análisis y métricas, de los repositorios de software. Frecuentemente, estos conjuntos de datos suelen ser compartidos como datos abiertos a través de ``repositorios de datos de software'' de acceso público.

Hoy en día existen muchas iniciativas que publican conjunto de datos de software ya sea de estudios realizados sobre MSR u otros. Mencionaremos algunos de estas iniciativas: OpenHUb, Papers With Code,World of code, Zenodo, GHTorrent dataset, Github Archive, FLOSSMETRICS, Software Heritage Datasets, kaggle, etc. 

Los repositorios de conjuntos de datos relacionados con software y datos abiertos se resumen en la Tabla~\ref{table:dataset_repositories}, lo que permite una visión general de sus características distintivas.

\begin{table}[htbp]
	\centering
	\caption[Repositorios de conjuntos de datos]{Comparativa de repositorios de conjuntos de datos relacionados con software y datos abiertos.}
	\label{table:dataset_repositories}
	\renewcommand{\arraystretch}{1.2}
	\adjustbox{width=\textwidth}{
		\begin{tabular}{lcccl}
			\toprule
			\textbf{Repositorio} & \textbf{Año de creación} & \textbf{Enfoque principal} & \textbf{Acceso} & \textbf{Características destacadas} \\
			\midrule
			OpenHub            & 2004 & Métricas de proyectos FLOSS & Público & Análisis histórico y tendencias de proyectos de software. \\
			Papers With Code   & 2018 & Machine Learning & Público & Vincula artículos científicos con implementaciones de código. \\
			World of Code      & 2012  & Historial de desarrollo de software & Público & Rastreo de dependencias y evolución del software. \\
			Zenodo             & 2013 & Repositorio de investigación general & Público y privado & Integración con GitHub y preservación a largo plazo. \\
			GHTorrent Dataset  & 2013 & Datos de GitHub & Público & Dataset sobre actividades y metadatos en GitHub. \\
			GitHub Archive     & 2012 & Eventos públicos de GitHub & Público & Agregado de eventos públicos para análisis de tendencias. \\
			FLOSSMETRICS       & 2006 & Proyectos FLOSS & Público & Análisis de licencias y métricas relacionadas con software FLOSS. \\
			Software Heritage Datasets & 2015 & Archivo de código fuente & Público & Preservación universal del código fuente y su historia. \\
			Kaggle             & 2010 & Ciencia de datos & Público & Competencias y conjuntos de datos para análisis de datos. \\
			\bottomrule
	\end{tabular}}
\end{table}



\subsection{OpenHub}
\label{subsec:openhub}
OpenHub\footnote{\url{https://openhub.net/}} es un sistema con interfaz que brinda un acceso a información (datos de confianza) sobre millones de repositorios FLOSS y su rendimiento en diferentes atributos de calidad. La información que brinda proviene del GitHub, Google Code, Bitbucket y SourceForge que usan un sistema de control de versiones. La información está disponible de manera pública por lo que no sólo arquitectos de software y programadores pueden consultar y revisar la indexación de \num{252869} proyectos a 22 de abril del 2024~\cite{Chelkowski2021}~\cite{Open2024}.

\subsection{World of Code}
\label{subsec:woc}
World of Code (WoC)\footnote{\url{https://worldofcode.org/}} es una infraestructura que ofrece conjuntos de datos para distintos análisis. Los datos están disponibles de manera pública para investigadores y otros interesados que deseen realizar minería de datos en código público. Su objetivo es proporcionar una visión completa y detallada de los proyectos de software libre y de código abierto~\cite{Ma2021}. Además, WoC recopila y actualiza datos de VCS en ecosistemas FLOSS, facilitando el descubrimiento de dependencias técnicas y flujos de código.

\subsection{Zenodo}
\label{subsec:zenodo}  
Zenodo\footnote{\url{https://zenodo.org/}} es una plataforma de repositorio de acceso abierto que permite a los investigadores, científicos y académicos depositar y compartir una amplia gama de tipos de datos y resultados de investigación~\cite{potter2015}. Fue lanzada en 2013 por el European Organization for Nuclear Research~(CERN),\footnote{\url{https://home.cern/}} en el marco del proyecto europeo OpenAIRE, con el objetivo de proporcionar un lugar donde cualquier investigador pueda publicar sus datos de forma abierta y gratuita.

\subsection{Boa}
\label{subsec:boa}
Boa\footnote{\url{https://boa.cs.iastate.edu/}} es una infraestructura y un conjunto de conjuntos de datos acompañantes para analizar artefactos de código fuente provenientes de código público a gran escala. Algunos de los conjuntos de datos alojados en Boa contienen archivos de código fuente e información de confirmación proveniente de VCS disponibles públicamente, en particular aquellos alojados en GitHub~\cite{Dyer2015}. Además, Boa proporciona una plataforma eficiente para la minería de datos en código público, facilitando el análisis de tendencias y patrones en el desarrollo FLOSS.

\subsection{GHTorrent}
\label{subsec:Ghtorrent}
GHTorrent\footnote{\url{https://github.com/ghtorrent/ghtorrent.org}} es un conjunto de datos de eventos archivados de la API REST de GitHub. Contiene información sobre proyectos públicos de GitHub, pero hasta la fecha de hoy no incluye la licencia que GitHub detecta como la licencia principal de un proyecto dado. Dado que el código fuente está fuera del alcance de GHTorrent, los textos de licencia no pueden encontrarse tampoco en este conjunto de datos~\cite{Georgios2012}. A pesar de estas limitaciones, GHTorrent sigue siendo una herramienta valiosa para los investigadores, puesto que proporciona una gran cantidad de información sobre la actividad y las interacciones en los proyectos de GitHub. Su capacidad para archivar eventos de la API REST de GitHub permite un análisis detallado de la dinámica del desarrollo de software en la plataforma.

\subsection{FLOSSMetrics}
\label{subsec:flossmetrics}
\textbf{FLOSSMetrics}\footnote{\url{http://www.flossmetrics.org/}} tuvo como objetivo principal crear, publicar y analizar una base de datos a gran escala con información y métricas sobre el desarrollo de FLOSS, recopiladas de varios miles de proyectos de software, y hacerlos disponibles en el sitio web de Melquiades.\footnote{\url{http://melquiades.flossmetrics.org/}} Esta iniciativa fue un proyecto de investigación financiado por el Sexto Programa Marco de la Comisión Europea en 2009~\cite{herraiz2009}~\cite{Gonzalez2010}.


\subsection{Software Heritage Datasets}
\label{subsec:swheritagedataset}
Se ha mencionado anteriormente que Software Heritage se enfoca a la preservación del software. Además, mediante el desarrollo o uso de herramientas específicas, se extraen grandes cantidades de datos, los cuales son posteriormente publicados con acceso abierto, permitiendo su uso por parte del público en general~\cite{Pietri2020}. Este enfoque ha despertado un notable interés entre la comunidad de investigadores de software FLOSS. Para acceder a los conjunto de datos hay que visitar la página web de Software  Heritage\footnote{\url{https://annex.softwareheritage.org/public/},}~\footnote{\url{https://docs.softwareheritage.org/devel/swh-dataset/index.html}}~\cite{Bhattacharjee2020}.  
