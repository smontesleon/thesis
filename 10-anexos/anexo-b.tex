%\section{Ejemplos prácticos de uso con el conjuntos de datos}
%\label{sec:usage}
\section{Introducción}
\label{sec:introusodata}
%Aquí se proporciona algunos ejemplos prácticos del uso del conjunto de datos
En este apéndice se proporcionan, a modo de ejemplo, algunos casos prácticos de análisis de la ``colección de licencias'' de Software Heritage, y de la muestra anotada de la misma, utilizando el lenguaje Python y las bibliotecas de ciencia de datos Pandas~\cite{McKinney2011} y Scikit-learn~\cite{Pedregosa2011}. 

En primer lugar, se muestran algunos ejemplos de cómo los archivos de metadatos pueden ser utilizados para obtener una caracterización de ciertos parámetros básicos de la colección de documentos. A continuación, se trabaja con los documentos de la colección para obtener la distribución de frecuencia de palabras en todos los documentos en formato de texto plano.

Finalmente, se utiliza la ``muestra anotada'' para entrenar y probar un clasificador de bosques aleatorios diseñado para predecir cuándo un documento incluye una sola licencia de texto completo. 

Estos son solo ejemplos de cómo trabajar con el conjunto de datos, pero proporcionan información valiosa: el primer ejemplo muestra cómo se realizó la mayor parte de la caracterización en la Sección~\nameref{sec:result}; el segundo ilustra cómo pueden procesarse los documentos; y el tercero exhibe el tipo de análisis para el cual se produjo la Muestra Anotada: para ser utilizado como verdad fundamental para clasificadores y otros procesadores que podrían trabajar con toda la colección.

\section{Trabajando con metadatos de archivos}
\label{sec:metadatos}
Algunas informaciones pueden extraerse de los ``archivos de metadatos'' con herramientas de línea de comandos simples. Por ejemplo, el número de documentos con información de licencia según ScanCode, presentado en la subsección~\nameref{sec:characterization-licenses-found}, se obtuvo del archivo CVS de \texttt{scancode} de la siguiente manera:

\begin{lstlisting}[basicstyle=\ttfamily\small]
	cut -d, -f1 blobs-scancode.csv  | uniq | wc
\end{lstlisting}

\noindent
Para análisis más complejos, generalmente es más conveniente utilizar scripts en algún lenguaje de programación. Por ejemplo, para tener una idea general del conjunto de datos, se puede examinar el archivo CVS de \texttt{fileinfo} en los ``archivos de metadatos'' y producir algunas estadísticas descriptivas al respecto de la siguiente manera (en Python):


\begin{lstlisting}[style=myPython]
	import os
	import subprocess
	import pandas as pd
	
	stats_csv = f"{dataset_dir}/blobs-fileinfo.csv"
	if not os.path.isfile(stats_csv):
	subprocess.run(["unzstd", "--force", stats_csv + ".zst"], \
	check=True)
	stats = pd.read_csv(stats_csv)
	stats.describe()
\end{lstlisting}

Donde \texttt{dataset\_dir} es una variable que apunta al directorio local de descarga del conjunto de datos. Observa cómo nos encargamos de descomprimir el archivo CSV relevante de la distribución del conjunto de datos. Los resultados obtenidos se verán así:


\begin{center}\small
	\begin{BVerbatim}
			line_count    word_count          size
		count  5.667116e+06  5.667116e+06  6.859190e+06
		mean   1.307410e+02  8.610119e+02  1.015964e+04
		std    4.511294e+03  1.450112e+04  2.450215e+05
		min    1.000000e+00  0.000000e+00  0.000000e+00
		25%    2.000000e+01  1.680000e+02  1.065000e+03
		50%    2.100000e+01  1.690000e+02  1.080000e+03
		75%    2.700000e+01  1.890000e+02  2.241000e+03
		max    6.373094e+06  7.374871e+06  1.909773e+08
	\end{BVerbatim}
\end{center}


Proporcionan una descripción estadística preliminar de las diferentes métricas de tamaño de todos los documentos en el conjunto de datos (consulte la Sección~\ref{sec:resppreguntasidfloss} para un análisis más detallado).

La lista de los tipos de archivo principales en el conjunto de datos, análoga a lo que se informa en la Figura~\ref{fig:documents-filetypes}, se puede obtener en forma tabular analizando los mismos metadatos que antes, de la siguiente manera:

\begin{lstlisting}[style=myPython]
	mime_top = stats["mime_type"].value_counts()\
	.nlargest(20).rename_axis('mime_type').reset_index(name='counts')
	mime_top.to_csv(out_data_dir + "/mime_top.csv", index=False)
	mime_top
\end{lstlisting}
\begin{center}\small
	\begin{BVerbatim}
		mime_type   counts
		0                  text/plain  5721424
		1                   text/html   723593
		2                  text/x-php    65275
		3                 text/x-java    61554
		4    application/octet-stream    49195
		5                    text/xml    49101
		6                   image/png    22912
		7            application/json    20327
		8        text/x-script.python    15703
		9            application/gzip    12367
		10                   text/rtf    11956
		11                text/x-ruby    11646
		12                  text/x-po    11102
		13                   text/x-c    11084
		14                 text/x-c++    10549
		15  application/x-java-applet     6584
		16              image/svg+xml     6493
		17                 text/x-tex     6286
		18     text/x-bytecode.python     4701
		19            application/csv     4674
	\end{BVerbatim}
\end{center}

\noindent
Un análisis similar para las codificaciones de caracteres sería:

\begin{lstlisting}[style=myPython]
	encoding_top = stats["encoding"].value_counts()\
	.rename_axis('encoding').reset_index(name='counts')
	encoding_top.to_csv(out_data_dir + "/encoding_top.csv", index=False)
	encoding_top
\end{lstlisting}
\begin{center}\small
	\begin{BVerbatim}
		encoding   counts
		0      us-ascii  5517915
		1         utf-8  1154191
		2        binary   121966
		3    iso-8859-1    49251
		4  unknown-8bit    13597
		5      utf-16le     2125
		6      utf-16be      143
		7        ebcdic        2
	\end{BVerbatim}
\end{center}

\section{Trabajando con la colección de documentos}
\label{sec:usage-collection}

Como ejemplo de cómo analizar los documentos de licencia reales, en lugar de solo los metadatos asociados, mostramos cómo obtener la distribución de frecuencia de palabras de todo el conjunto de datos:

\begin{lstlisting}[style=myPython]
	def mine_word_frequency(df, out_fname):
	from collections import Counter
	import re
	import string
	
	WORD_SEP = re.compile(r"\W+")
	word_freqs = Counter()
	text_blobs = df[(df.mime_type == "text/plain") \
	& (df.encoding.isin(["us-ascii", "utf-8", "iso-8859-1"]))]
	for sha1 in text_blobs["sha1"]:
	fname = f"{dataset_dir}/blobs/{sha1[0:2]}/{sha1[2:4]}/{sha1}"
	try:
	with open(fname, encoding="utf-8") as f:
	for line in f:
	word_freqs.update(
	(word
	for word in WORD_SEP.split(line.lower())
	if word)
	)
	except ValueError:  # decoding errors
	continue
	
	with open(out_fname, "w") as csv:
	csv.write("word,frequency\n")
	for (word, freq) in word_freqs.items():
	csv.write(f"{word},{freq}\n")
	
	words_csv = "blobs-wordfreqs.csv"
	mine_word_frequency(stats, words_csv)
	
	import nltk
	import string
	from nltk.corpus import stopwords
	
	nltk.download("stopwords")
	stop_words = stopwords.words('english')
	stop_words.extend(string.digits)
	stop_words.extend(string.ascii_lowercase)
	
	words = pd.read_csv(words_csv)\
	.sort_values(by="frequency", ascending=False)
	interesting_words = words[~words["word"].isin(stop_words)]
	interesting_words
\end{lstlisting}

\noindent
El código anterior asume que \emph{todos} los fragmentos de licencia ya han sido descomprimidos en el subdirectorio \texttt{blobs/} del directorio de descarga del conjunto de datos.
Hacerlo es trivial, pero tenga en cuenta que creará aproximadamente $\DataBlobCountApprox{}$ archivos en el sistema de archivos y requerirá aproximadamente \DataTarExpandedSizeApprox{} de espacio en disco.

La función principal es \texttt{mine\_word\_frequency}, que recorrerá todos los documentos de licencia en el conjunto de datos para recopilar la frecuencia de todas las palabras (incluidas las palabras vacías).
Tenga en cuenta que el enfoque aquí es muy ingenuo, por ejemplo, sin paralelismo involucrado; soluciones mejores se pueden encontrar en marcos de procesamiento de NLP como Gensim~\cite{SrinivasaDesikan2018}.
Al final de la extracción, los resultados se serializan en formato CSV en \texttt{blobs-wordfreqs.csv}.
Ese archivo a su vez puede cargarse e inspeccionarse, después de eliminar las palabras vacías en inglés (según NLTK~\cite{Bird2006}) y las palabras de un solo carácter (aunque los resultados pueden variar).
Los resultados fueron los mostrados antes en la Tabla~\ref{tab:top-words}.

\section{Trabajando con la muestra anotada}
\label{sec:usage-annotated-sample}

Como ejemplo de posibles usos de la Muestra Anotada, se muestra a continuación un programa simple para producir un modelo de bosque aleatorio entrenado para clasificar documentos en dos categorías: aquellos que contienen el texto completo de una sola licencia y el resto. Este código asume que los módulos \texttt{licen} y \texttt{licens}\footnote{\texttt{licen} y \texttt{licens} son módulos de Python para tratar con la Colección de Documentos.} están disponibles en el \texttt{sys.path} de Python, que la función \texttt{path\_from\_filename}\footnote{\texttt{path\_from\_filename} es una función que devuelve la ruta de un documento en la colección, dado su nombre (SHA1)} está disponible para el intérprete \revchange{(ten en cuenta que este es solo un ejemplo ilustrativo, no destinado a un análisis real, y por lo tanto no incluye técnicas de optimización que mejorarían los resultados)}.\footnote{Para un programa completo y listo para funcionar, verifica el archivo \texttt{truth/random\_forest.py} en el conjunto de datos}. En este código, \texttt{licenses\_dir} apunta a la Colección de Documentos y \texttt{truth.csv} a la ``muestra anotada''.

El programa comienza importando todos los módulos necesarios:

\begin{lstlisting}[style=myPython]
	import os
	import pandas as pd
	from sklearn.feature_extraction.text import CountVectorizer
	from sklearn.feature_extraction.text import TfidfTransformer
	from sklearn.metrics import (classification_report,
	confusion_matrix, accuracy_score)
	from sklearn.model_selection import train_test_split
	from sklearn.ensemble import RandomForestClassifier
	import licens
	import licen
\end{lstlisting}

Luego, se carga la ``muestra anotada'' en un dataframe, se obtiene todos los documentos en él de la ``colección de documentos'' y produce una lista con cadenas correspondientes a la versión normalizada de todos esos documentos. La normalización incluye: eliminación de todos los espacios en blanco, pasar todo a minúsculas y eliminación de líneas que contienen avisos de derechos de autor simples:


\begin{lstlisting}[style=myPython]
	# Read the Annotated Sample (one line per document) into a dataframe
	truth_df = pd.read_csv('truth.csv')
	# Compute normalized versions of all license documents in the sample
	paths = [path_from_name(name, licenses_dir)
	for name in truth_df['name']]
	documents = licens.Licenses(paths,
	cache=None, rebuild_cache = True,
	comps=[licen.CompUnified, licen.CompIdentify])
	# Get a list with the normalized text for all plain text documents
	text_licen = [licen
	for licen in documents.get_licenses()
	if (licen.utext is not None) \
	and (licen.kind == 'text/plain')]
	udocuments = [documents.get_license(licen.filename, licen.name).utext
	for licen in text_licen]
\end{lstlisting}

\noindent
Ahora se produce los valores de origen (X) y destino (y) para entrenar y probar el modelo de bosque aleatorio, y se los divide en conjuntos de entrenamiento y prueba:


\begin{lstlisting}[style=myPython]
	# Source values: Vectorized normalized documents (TF-IDF representation)
	vectorizer = CountVectorizer(max_features=1500, min_df=5, max_df=0.7)
	X = vectorizer.fit_transform(udocuments).toarray()
	tfidfconverter = TfidfTransformer()
	X = tfidfconverter.fit_transform(X).toarray()
	# Target values: True if document includes a singl full-text license
	y = [truth_df.loc[truth_df['name'] == doc.name, 'licen'].values[0]
	for doc in text_licen]
	# Get training and testing sets
	X_train, X_test, y_train, y_test = \
	train_test_split(X, y, test_size=0.2, random_state=0)
\end{lstlisting}

\noindent
Finalmente, se entrena y prueba el modelo de ``Random Forest Classifier'' (bosque aleatorio):


\begin{lstlisting}[style=myPython]
	# Train a random forest classifier
	classifier = RandomForestClassifier(
	n_estimators=1000, random_state=0, n_jobs=-1)
	classifier.fit(X_train, y_train)
	# Predict which files are full-text licenses for testing sample
	y_pred = classifier.predict(X_test)
	# Print results of the prediction
	print(confusion_matrix(y_test,y_pred))
	print(classification_report(y_test,y_pred))
	print(accuracy_score(y_test, y_pred))
\end{lstlisting}

\noindent
El resultado de ejecutar este programa se muestra en la Tabla~\ref{tab:random-forest-results}, obteniendo una precisión del $91.7$\%.
%, ¡lo cual no está nada mal para un primer intento!


\begin{table}
	\caption[Entrena y evalúa un clasificador]{Los resultados de ejecutar el programa que entrena y evalúa un clasificador de modelo aleatorio para predecir cuándo un documento incluye una sola licencia de full-text. La precisión del modelo se evalúa en $0.917$.}
	\label{tab:random-forest-results}
	\centering
	\begin{tabular}{l|r|r}
		& \textbf{Negatives} & \textbf{Positives} \\ \hline
		\textbf{True} & \num{371}  & \num{85} \\
		\textbf{False} & \num{19} & \num{782} \\
	\end{tabular}
	\vspace{2ex}
	
	\begin{tabular}{l|c|c|c|c}
		& \textbf{Precision} & \textbf{Recall} & \textbf{F1-score} & \textbf{Support} \\ \hline
		\textbf{False}    &   $0.95$  &    $0.81$  &    $0.88$  &     $456$ \\
		\textbf{True}    &   $0.90$  &    $0.98$  &    $0.94$  &     $801$ \\
	\end{tabular}
\end{table}

