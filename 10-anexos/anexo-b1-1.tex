\section{Introducción}
\label{sec:res_intro}

Durante la década de 1960, cuando industrias e instituciones empezaron a adquirir computadores, estos solían incluir software de manera inseparable del hardware. Sin embargo, este enfoque cambió en 1969 cuando IBM separó el software del hardware, permitiendo que los programas informáticos comenzaran a comercializarse bajo diferentes licencias~\cite{Haigh2002Software}. Este cambio marcó el inicio del software privativo, que restringía los derechos de uso del software por parte de los usuarios.

En respuesta a esto, en 1984, Richard Stallman fundó la Free Software Foundation, promoviendo el concepto de software libre, basado en la libertad de los usuarios para ejecutar, estudiar, compartir y modificar el software~\cite{Stallman2002}. Este movimiento fue formalizado con la Licencia Pública General GNU (GNU-GPL) en 1989, la cual garantizaba estas libertades~\cite{lawrence2004open}. 

Desde entonces, surgieron otras iniciativas como la Open Source Initiative y la Linux Foundation, lo que llevó al desarrollo de un gran número de licencias libres y de código abierto (FLOSS). Estas licencias canónicas permitieron la adaptación de los términos de distribución a las necesidades de los desarrolladores, generando una vasta diversidad de licencias~\cite{laurent2004}. Hoy en día, más del 50\% del software contiene código FLOSS~\cite{Ballhausen2019}, lo que hace crucial su correcta identificación en repositorios de software.

A pesar de esto, existen pocos estudios que se dediquen a la clasificación e identificación de licencias FLOSS, lo que motivó esta investigación~\cite{GonzalezBarahona2023}.

%\subsection{Contexto}
%El creciente uso del software libre y de código abierto ha generado un amplio ecosistema de licencias. Estas licencias, además de definir cómo se puede utilizar el software, son clave para garantizar la legalidad y los derechos de autor de los proyectos. Plataformas como GitHub, GitLab y Bitbucket se han convertido en repositorios fundamentales para el almacenamiento de código, pero al mismo tiempo han planteado nuevos retos en términos de gestión y cumplimiento de las licencias~\cite{GonzalezBarahona2023}. Dada la magnitud del código FLOSS y la diversidad de licencias, es vital contar con herramientas y estudios que permitan gestionar esta complejidad de forma eficiente~\cite{Laurent2004}.
\subsection*{Motivación}
\label{cenatic2009sec:res_motivacion}

En el contexto del desarrollo de software libre y de código abierto (FLOSS), es habitual la reutilización de componentes o la incorporación de fragmentos de código fuente, lo que aporta valor en términos de funcionalidad y calidad. Sin embargo, esto también implica desafíos relacionados con las licencias de software, ya que es necesario respetar los términos de uso y los derechos de autor correspondientes. 

El incumplimiento de las licencias FLOSS puede tener consecuencias legales graves para las organizaciones, así como pérdidas de reputación. Aunque existen diversas herramientas para identificar el cumplimiento de las licencias, no siempre está claro cuáles son las más robustas y adecuadas para esta tarea. Muchas de estas herramientas se basan en la iniciativa SPDX, que agrupa las licencias canónicas más reconocidas, pero se ha detectado que no abordan adecuadamente los términos de licencias en contextos más amplios, lo que dificulta su identificación y clasificación en repositorios de software~\cite{laurent2004}.

La presente tesis busca llenar este vacío de conocimiento mediante el desarrollo de un estudio exhaustivo sobre la identificación y clasificación de licencias FLOSS, de manera especial del por qué hay tanta diversidad de licencias FLOSS. Así proporcionar una base sólida que ayude a la academia y la industria a gestionar mejor las licencias en el desarrollo de software FLOSS, minimizando los riesgos legales y fomentando la innovación y la colaboración~\cite{GonzalezBarahona2023}.

\subsection*{Objetivos de la Tesis}

El objetivo general de esta tesis es realizar un análisis exhaustivo de la diversidad existente en las licencias FLOSS, describiendo y clasificando la mayor cantidad posible de licencias, con el fin de identificar las diferencias clave entre ellas y comprender su impacto en el uso, distribución y desarrollo de proyectos FLOSS.

\textbf{Objetivos específicos}:
\begin{itemize}
	\item Describir el estado del arte respecto a la identificación y clasificación de licencias FLOSS.
	\item Conocer la evolución de las técnicas aplicadas para la identificación de licencias FLOSS.
	\item Identificar las herramientas existentes para la identificación de licencias.
	\item Analizar un conjunto de documentos que contengan tantas licencias FLOSS como sea posible.
	\item Clasificar los documentos de licencias FLOSS con anotaciones que contengan una sola licencia.
	\item Obtener un conjunto de datos con un análisis detallado de las licencias FLOSS.
\end{itemize}

\section{Contexto: Propiedad Intelectual y Software Libre}
\label{sec:res_contexto}

La presente tesis, al centrarse en el análisis de la diversidad de licencias, considera fundamental abordar los principales conceptos que sustentan el software libre y de código abierto, conocidos colectivamente como FLOSS (Free/Libre and Open Source Software)~\cite{laurent2004}. Estos términos buscan neutralizar la división ideológica entre ambos enfoques y se examina la interacción del FLOSS con los marcos jurídicos de la propiedad intelectual, un aspecto crucial para comprender su funcionamiento y adopción.

El concepto de propiedad intelectual fue reconocido en tratados internacionales como la Convención de París (1883) y la Convención de Berna (1886), siendo clave para la protección de obras intelectuales, incluidas las invenciones tecnológicas y el software~\cite{Wipo2022}. Los derechos de propiedad intelectual (DPI) permiten a los creadores beneficiarse económicamente de su trabajo y se encuentran contemplados en la Declaración Universal de los Derechos Humanos, garantizando tanto la protección de los intereses morales como materiales derivados de la autoría~\cite{tuunanen2021}.

\subsection*{Licencias FLOSS}
Las licencias de software son contratos legales que definen los derechos de uso y distribución del software. A grandes rasgos, se distinguen entre licencias privativas y licencias libres. Mientras que las primeras restringen el acceso al código fuente y su modificación, las licencias libres otorgan a los usuarios las libertades para ejecutar, estudiar, modificar y redistribuir el software~\cite{cenatic2009}. Entre las licencias libres más comunes se encuentran la GNU General Public License (GPL), la Licencia MIT y la Licencia Apache, todas ellas respetan las cuatro libertades fundamentales del software libre~\cite{Stallman2002}.

\subsection*{Proliferación de licencias}
En los últimos años, ha habido una proliferación de licencias FLOSS, muchas de las cuales han surgido para abordar problemáticas específicas que no cubren las licencias tradicionales. Sin embargo, esta proliferación ha generado confusión y complejidad dentro de la comunidad FLOSS, complicando la gestión de los proyectos y provocando problemas de incompatibilidad entre licencias~\cite{GonzalezBarahona2023}.

\subsection*{Compatibilidad de licencias}
Uno de los mayores desafíos en el desarrollo de software libre es la compatibilidad entre diferentes licencias. Algunas licencias, como las de tipo copyleft, obligan a que las versiones derivadas se distribuyan bajo las mismas condiciones, mientras que las licencias permisivas permiten mayor flexibilidad en las obras derivadas. Esta divergencia plantea conflictos cuando se combinan componentes de software bajo diferentes licencias, limitando las opciones de distribución~\cite{laurent2004}.

\section{Contexto: Repositorios y Publicación de Software}
\label{sec:res_repositorios}

El almacenamiento y preservación del conocimiento, incluyendo el software, ha sido un objetivo clave para el desarrollo de las sociedades. En el caso del software libre y de código abierto (FLOSS), los repositorios no solo almacenan y distribuyen software, sino que también facilitan la colaboración y el desarrollo abierto~\cite{Constantopoulos1993}. Plataformas como GitHub, GitLab y Bitbucket han sido fundamentales para la expansión del ecosistema FLOSS, permitiendo a los desarrolladores trabajar en proyectos de manera colaborativa y transparente, mientras que iniciativas como Software Heritage han asumido la responsabilidad de preservar el código para futuras generaciones~\cite{DiCosmo2017}.

\subsection*{Repositorios de Distribución}
Los repositorios de distribución permiten que el software FLOSS esté disponible para los usuarios de manera fácil y rápida. Algunos de los repositorios más conocidos incluyen SourceForge, F-Droid, y Google Play Store, que distribuyen software tanto para dispositivos móviles como para entornos de escritorio~\cite{Cen2016}. Estos repositorios también están integrados con sistemas de gestión de paquetes como APT y RPM, que automatizan la instalación y actualización de software en sistemas operativos basados en GNU/Linux~\cite{Florisson2017}. 

\subsection*{Forjas de Desarrollo}
Las forjas de desarrollo, como GitHub, GitLab y Bitbucket, no solo almacenan el código fuente de proyectos FLOSS, sino que también permiten el control de versiones y la colaboración en equipo. Estas plataformas ofrecen herramientas como la revisión de código, seguimiento de problemas y control de versiones distribuidas (por ejemplo, Git), facilitando que múltiples desarrolladores trabajen simultáneamente en el mismo proyecto sin conflictos~\cite{Zolkifli2018}.

Además de ser esenciales para el desarrollo, las forjas también actúan como puntos de distribución, permitiendo que los usuarios descarguen y compilen versiones del software directamente desde el código fuente, lo que refuerza los principios de transparencia y colaboración del software libre~\cite{Squire2014}.

\subsection*{Repositorios de Preservación}
Los repositorios de preservación, como Software Heritage, se centran en garantizar que el código fuente y la documentación asociada a los proyectos FLOSS estén disponibles a largo plazo~\cite{DiCosmo2017}. Este tipo de repositorios es crucial para evitar la pérdida de proyectos que dejan de ser mantenidos, asegurando que el conocimiento y las innovaciones incorporadas en el código se mantengan accesibles para futuras generaciones de desarrolladores y académicos.

Iniciativas como el Internet Archive Software Collection o el NASA's Software Catalog también desempeñan un papel importante en la preservación del software, proporcionando un archivo digital de proyectos históricos y actuales~\cite{matthews2010}. Estos repositorios actúan como bibliotecas de software que fomentan la transparencia y reproducibilidad en el ámbito de la investigación y el desarrollo tecnológico.

\subsection*{Repositorios de Conjunto de Datos}
Los repositorios de conjunto de datos, como OpenHub, World of Code, Zenodo y Software Heritage, permiten a los investigadores analizar grandes volúmenes de datos históricos sobre el desarrollo de software. Estos repositorios ofrecen métricas detalladas sobre la evolución de los proyectos FLOSS, como la calidad del código, las dependencias entre proyectos y los patrones de colaboración entre desarrolladores~\cite{robbes2011}. A través del análisis de estos datos, es posible obtener una comprensión más profunda de las dinámicas del desarrollo colaborativo de software libre. 

\section{Revisión Sistemática de la Literatura (SLR)}
\label{sec:res_slr}

La Revisión Sistemática de la Literatura (SLR) es una metodología clave en esta tesis, la cual tiene como objetivo recopilar, analizar y sintetizar los estudios previos sobre la identificación y clasificación de licencias FLOSS. El propósito de la SLR es proporcionar un panorama detallado del estado del arte en este campo de estudio, identificando las principales técnicas utilizadas para la detección y clasificación de licencias, así como las herramientas desarrolladas para apoyar este proceso~\cite{GonzalezBarahona2023}.

\subsection{Metodología}

La SLR realizada sigue un protocolo riguroso para garantizar la validez y exhaustividad de los resultados siguiendo el modelo de procesos propuesto por~\cite{Tebes2019} (ver Figura~\ref{fig:srl_tebes}). El protocolo incluye la definición de preguntas de investigación específicas, la selección de bases de datos digitales para realizar la búsqueda de artículos relevantes y la aplicación de criterios de inclusión y exclusión para filtrar los estudios primarios~\cite{laurent2004}. Véase la Tabla~\ref{tab:SLR-A1-A2} para un resumen del protocolo de búsqueda empleado.

Las preguntas de investigación que guiaron la SLR incluyen las siguientes:
\begin{itemize}
	\item \textbf{RQ1}: ¿Qué técnicas se utilizan para la identificación de licencias FLOSS?
	\item \textbf{RQ2}: ¿Cómo se evalúan estas técnicas en los estudios primarios?
	\item \textbf{RQ3}: ¿Qué herramientas existen para la identificación de licencias FLOSS y qué tan efectivas son?
\end{itemize}

\subsection{Resultados}

La revisión identificó un total de 42 estudios primarios, los cuales fueron analizados en profundidad. Las técnicas de identificación más comunes se basan en el uso de herramientas automatizadas como ScanCode y FOSSology, las cuales permiten realizar un escaneo de los archivos de código fuente para detectar licencias~\cite{nexB2022a}. No obstante, se observó que estas herramientas presentan limitaciones, especialmente en la detección de licencias no canónicas o modificadas~\cite{Ombredanne2020}. Véase la Tabla~\ref{table:tools_available} para un resumen de las herramientas más mencionadas y su frecuencia de uso en los estudios revisados.

Además, la SLR reveló que las técnicas de evaluación más utilizadas son las pruebas empíricas basadas en conjuntos de datos públicos, donde se mide la precisión de las herramientas en la identificación correcta de licencias~\cite{GonzalezBarahona2023}. Se detectó una carencia de estudios que evalúen la compatibilidad entre licencias, lo que representa una brecha importante en la literatura actual.

\subsection{Limitaciones de la SLR}

A pesar de los hallazgos relevantes, la SLR presenta algunas limitaciones. La principal es la falta de estudios que aborden el problema de la diversidad de licencias de manera integral. Muchos estudios se centran en un conjunto limitado de licencias, lo que reduce la aplicabilidad de sus resultados a un contexto más amplio~\cite{GonzalezBarahona2023}. Además, las herramientas automatizadas aún requieren intervención manual en casos donde las licencias presentan modificaciones o no están bien documentadas, lo que limita su uso en proyectos de gran escala~\cite{laurent2004}.

\section{Fuente de Datos}
\label{sec:res_fuente}

Este capítulo describe la fuente de datos utilizada en esta investigación, basada principalmente en la colección de documentos de licencias FLOSS extraída del repositorio de Software Heritage. Este repositorio es una fuente clave para el estudio de la diversidad de licencias debido a su vasta colección de código fuente y archivos relacionados~\cite{Zacchiroli2022}.

\subsection{Obtención de la Colección de Documentos}
El proceso de obtención de la colección de documentos se realizó mediante una consulta a la base de datos de Software Heritage, utilizando una expresión regular que permitiera identificar archivos comúnmente asociados con licencias, como LICENSE.txt, COPYING, o NOTICE. Esta consulta recuperó más de 6 millones de archivos de licencias, que fueron filtrados y organizados para su posterior análisis~\cite{Pietri2019}. La estructura de los archivos y su clasificación se detallan en la Tabla~\ref{tab:size-distribution}.

\subsection{Descripción de la Colección de Documentos}
Los documentos obtenidos varían en tamaño, formato y contenido. La mayoría son archivos de texto plano, pero también se encontraron documentos en formatos HTML y PDF. Algunos de estos documentos contenían términos de licencia incrustados o referencias a otras licencias. Véase la Figura~\ref{fig:documents-filetypes} para una distribución de los formatos de archivo encontrados.

La clasificación de los documentos se realizó utilizando metadatos, que incluyeron información sobre el tamaño del archivo, el tipo de licencia y el proyecto de origen. Estos metadatos fueron procesados con herramientas como Pandas, lo que permitió una caracterización detallada de cada archivo. El modelo relacional utilizado para la descripción de los metadatos se muestra en la Figura~\ref{fig:metadata-schema}.

\subsection{Identificación Automática de Licencias con ScanCode}
Para la identificación automática de licencias en los archivos, se utilizó ScanCode, una herramienta ampliamente utilizada en la comunidad FLOSS. Esta herramienta detecta las licencias presentes en los archivos y proporciona un nivel de confianza para cada una. Los resultados del escaneo se almacenaron en formato JSON y se analizaron para identificar las licencias más comunes, así como variaciones de las licencias tradicionales~\cite{nexB2022}. Véase la Tabla~\ref{tab:licenses-scancode-occurrences} para una lista de las licencias más frecuentes identificadas.

\subsection{Ocurrencia de Licencias Conocidas}
Un aspecto clave del análisis fue la ocurrencia conjunta de licencias en los archivos. Se descubrió que ciertas combinaciones, como Apache-2.0 y MIT o GPL-2.0 y MIT, eran comunes. Estas combinaciones reflejan la preferencia de los desarrolladores por licencias compatibles entre sí. Véase la Tabla~\ref{tab:licenses-scancode-together} para un resumen de las combinaciones de licencias más frecuentes.

\section{Análisis de Licencias: Anotación}
\label{sec:res_analisis_licencias}

Este capítulo se centra en el análisis de licencias FLOSS mediante la técnica de anotación manual. El objetivo es obtener una comprensión detallada de las características y clasificaciones de las licencias presentes en un subconjunto representativo de documentos. Esta metodología permite identificar las cláusulas clave de cada licencia, así como patrones, similitudes y diferencias entre las diversas licencias FLOSS~\cite{GonzalezBarahona2023}.

\subsection{Preguntas de Investigación}

A lo largo del análisis se plantearon las siguientes preguntas de investigación:

\begin{itemize}
	\item \textbf{RQ1}: ¿Cuántos archivos del conjunto de datos contienen una sola licencia?
	\item \textbf{RQ2}: ¿Por qué existe una diversidad tan grande de licencias en el conjunto de datos?
	\item \textbf{RQ3}: ¿Qué patrones se observan en la diversidad de licencias?
	\item \textbf{RQ4}: ¿Qué licencias peculiares se identifican en el análisis manual?
	\item \textbf{RQ5}: ¿Cómo se consideran los archivos que no contienen licencias?
\end{itemize}

\subsection{Metodología}
El proceso de anotación implicó la selección de un subconjunto de archivos obtenidos de Software Heritage, que luego fueron analizados manualmente. Los documentos fueron revisados para identificar aquellos que contenían el texto completo de una licencia, y se excluyeron los archivos con múltiples licencias para evitar duplicidades. Véase la Figura~\ref{fig:proces_anota} para un esquema del proceso de anotación manual utilizado~\cite{Pietri2019}.

\subsection{Resultados y Análisis}

Los resultados revelaron que un 56.87\% de los documentos contenían una sola licencia, con una precisión del 1.07\% (véase Tabla~\ref{tab:documents-single-license}). Este análisis permite comprender la prevalencia de las licencias más comunes, como MIT, Apache-2.0 y GPL-3.0. También se identificaron licencias menos comunes o variantes con modificaciones menores, lo que subraya la necesidad de herramientas más sofisticadas para la detección automática de licencias~\cite{nexB2022}.

Además, se encontraron ejemplos de licencias inusuales, como la "Dankware License v1", que ilustra cómo algunas licencias son adaptadas a contextos específicos por los desarrolladores (véase Figura~\ref{fig:dankware}).


\section{Discusión}
\label{sec:res_discusion}

Este capítulo se centra en la discusión de los principales hallazgos obtenidos en la investigación, basados tanto en la SLR como en el análisis empírico de las licencias FLOSS mediante anotación manual. También se abordan las amenazas a la validez del estudio y el interés potencial del conjunto de datos generado para futuros estudios en el campo del software libre y de código abierto.

\subsection{Amenazas a la Validez}
En toda investigación empírica existen diversas amenazas a la validez que deben ser consideradas. De acuerdo con3~\cite{Wohlin2012}, se identifican cuatro tipos principales de amenazas: validez de constructo, validez interna, validez externa y validez de conclusión.

\begin{itemize}
	\item \textbf{Validez de constructo:} Se refiere a la adecuación de las métricas utilizadas en el estudio y su capacidad para medir de manera correcta lo que se pretende. En esta investigación, se han seguido protocolos estrictos para la recolección de datos y la anotación manual, asegurando la precisión en la identificación y clasificación de licencias.
	\item \textbf{Validez interna:} Relacionada con la precisión de los resultados y la reducción de posibles errores sistemáticos. La metodología aplicada minimiza los sesgos durante la selección de documentos y el uso de herramientas automáticas como ScanCode.
	\item \textbf{Validez externa:} Esta amenaza se refiere a la capacidad de generalizar los resultados. Aunque el conjunto de datos está basado en archivos extraídos de Software Heritage, se espera que los resultados sean aplicables a otros repositorios de software libre y abierto. No obstante, se reconoce que algunas licencias menos comunes pueden no haber sido representadas de manera suficiente.
	\item \textbf{Validez de conclusión:} Relacionada con la fiabilidad de las conclusiones alcanzadas. En este aspecto, los resultados se basan en un conjunto amplio de datos anotados manualmente, lo que permite garantizar que las conclusiones reflejen de manera precisa la realidad observada.
\end{itemize}

\subsection{Discusión de la SLR}
La SLR reveló que existen diversas técnicas y herramientas para la identificación y clasificación de licencias FLOSS, pero también mostró limitaciones significativas en la capacidad de estas herramientas para gestionar la gran diversidad de licencias encontradas en los repositorios. Las herramientas más mencionadas en la literatura fueron ScanCode y FOSSology, ambas con un uso extensivo en la industria, aunque con limitaciones cuando se trata de identificar variantes de licencias o licencias menos comunes~\cite{nexB2022a}.

\subsection{Discusión sobre el Estudio Empírico}
El estudio empírico basado en la anotación manual permitió identificar una amplia diversidad de licencias FLOSS, con un 56,87\% de los archivos analizados que contenían el texto completo de una sola licencia. Este hallazgo subraya la importancia de desarrollar herramientas más avanzadas que permitan automatizar de manera más eficaz la clasificación de licencias y detectar variantes. Además, se identificaron licencias peculiares, como la "Dankware License v1" (véase Figura~\ref{fig:dankware}), que ilustran cómo algunas licencias se adaptan a contextos específicos creados por los desarrolladores.

\section{Conclusiones}
\label{sec:res_conclusiones}

La presente tesis ha contribuido significativamente al estudio de la diversidad de licencias FLOSS, abordando su clasificación, identificación y el impacto que tienen en el desarrollo y distribución de software libre y de código abierto. A continuación se destacan las conclusiones más importantes:

\subsection{Conclusiones Principales}

\begin{itemize}
	\item \textbf{Diversidad de Licencias FLOSS}: Se ha demostrado que la notable diversidad de licencias FLOSS, resultado de la evolución y adaptación del ecosistema de software libre, responde a la necesidad de ajustarse a diferentes contextos legales, culturales y técnicos. El análisis realizado ha permitido una comprensión más profunda de la complejidad del panorama actual de licencias y sus implicaciones en el desarrollo colaborativo.
	
	\item \textbf{Clasificación e Identificación de Licencias}: El uso de herramientas automáticas como ScanCode ha mostrado ser útil para la identificación de licencias, pero presenta limitaciones en la detección de variantes o modificaciones mínimas. Esto resalta la necesidad de mejorar las herramientas de automatización y complementarlas con métodos manuales cuando sea necesario.
	
	\item \textbf{Impacto en el Desarrollo de Software Libre}: La correcta gestión de licencias FLOSS es clave para el desarrollo sostenible del software libre. Esta investigación ha proporcionado conocimientos valiosos sobre cómo las licencias influyen en la interoperabilidad y la colaboración entre desarrolladores, lo que podría ayudar a la industria y a la comunidad académica a gestionar mejor el cumplimiento de las licencias.
	
	\item \textbf{Conjunto de Datos Anotados}: Uno de los principales aportes de esta tesis es la creación de un conjunto de datos anotado manualmente con licencias FLOSS, que sirve como referencia para investigaciones futuras y la mejora de herramientas automáticas. Este conjunto de datos es replicable y ha sido puesto a disposición de la comunidad científica para su uso en otros estudios~\cite{GonzalezBarahona2023}.
\end{itemize}

\section{Trabajos Futuros}
\label{sec:res_futuro}

La investigación realizada abre múltiples líneas de trabajo futuras. Se prevé:

\begin{itemize}
	\item \textbf{Actualizar la SLR}: Se procederá a revisar y afinar el contenido de la Revisión Sistemática de la Literatura (SLR) ya elaborada, con el propósito de preparar el artículo para su envío a una revista indexada en JCR, asegurando así la difusión de los resultados y su contribución al avance del conocimiento en este campo.
	
	\item \textbf{Actualización del Conjunto de Datos}: Se planifica mantener actualizado el conjunto de datos en colaboración con Software Heritage, asegurando que la colección de documentos y licencias se amplíe y se valide continuamente.
	
	\item \textbf{Uso de Inteligencia Artificial Generativa}: Se propone evaluar el uso de IA generativa (por ejemplo, ChatGPT) para la identificación y clasificación de licencias en grandes colecciones de documentos, contrastando los resultados con el conjunto de datos anotado manualmente para mejorar la precisión y eficiencia del proceso.
\end{itemize}




