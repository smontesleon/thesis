\section{Introducción}
\label{sec:intro_datos}
En la sección \nameref{subsec:swheritagedataset}, se menciona que Software Heritage pone a disposición pública conjuntos de datos obtenidos de la preservación del software público, entre los que hemos identificado un Conjunto de Datos de licencias de nuestro interés.\footnote{\url{https://annex.softwareheritage.org/public/dataset/licenseblobs/}} Desde el 21 de marzo de 2019, Software Heritage ha publicado varias versiones. En esta tesis, se utiliza la versión publicada el 25 de abril de 2022.\footnote{\url{https://annex.softwareheritage.org/public/dataset/licenseblobs/2022-04-25/}} Esta %\num{6859189}
versión específica incluye alrededor de \num{6} millones de  documentos únicos de posibles licencias, como se detalla en~\cite{Zacchiroli2022}.

Así, en la \nameref{chap:slr} realizada, se destacó que las colecciones de licencias publicadas por Software Heritage son las más grandes en términos de archivos que probablemente contienen información sobre licencias.

Software Heritage describe que la colección de documentos se compone de todas las versiones de todos los archivos de Software Heritage con nombres comúnmente utilizados para indicar la licencia de algún código fuente. La colección incluye muchos tipos diferentes de documentos: la mayoría relacionados con licencias de software, pero también hay otros tipos. En el caso de documentos relacionados con licencias de software, estos pueden incluir el texto completo de una única licencia, o solo un aviso con una referencia a la licencia real. Los documentos también pueden contener, en el caso de compilaciones de software o software que incluya archivos de diferentes proyectos, una lista de licencias y avisos de licencia. Aunque en la mayoría de los casos los documentos son archivos de texto sin formato, también se encuentran otros formatos en el conjunto de datos, como PDF y HTML.

Este capítulo describe en detalle la colección de documentos de licencias y cómo fue desarrollada por el equipo de Software Heritage para esta investigación. Los datos recopilados han sido de gran utilidad, como se demuestra en el siguiente capítulo, donde se desarrolla la parte medular de la presente tesis.

En las siguientes secciones se examina la colección de documentos desde diversas perspectivas: la metodología de obtención, la descripción detallada de los documentos, los metadatos asociados, la detección automática de licencias mediante ScanCode, las licencias identificadas y su concurrencia.

\section{Obtención de la colección de documentos}
\label{sec:obtcolecciondatos}
El proceso inicial por parte del equipo de Software Heritage para la obtención de licencias se basó en una combinación de conveniencia y estrategia. Conveniencia, porque el filtrado SQL basado en el nombre de archivo se realiza utilizando el conjunto de datos de grafo preexistente de Software Heritage~\cite{Pietri2019}. Esto ha capturado la mayoría de los archivos de licencia (\texttt{LICENSE.txt}, \texttt{LICENSE.md} o \texttt{LICENSE.rst}).

El equipo de Software Heritage, basándose en su experiencia con archivos de licencia, utilizó la siguiente expresión regular de nombre de archivo:
\begin{verbatim}
	^([a-z0-9._-]+\.)?(copying|licen(c|s)(e|ing)|notice
	|copyright|disclaimer|authors)(\.[a-z0-9\._-]+)?$
\end{verbatim}

Usando esta expresión, escribieron una consulta SQL\footnote{La consulta SQL completa está disponible como parte del paquete de replicación del conjunto de datos~\cite{GonzalezBarahona2023}, en el archivo \texttt{replication-package.tar.gz}.} para recuperar el SWHID, el checksum SHA1 y el nombre de archivo de todos los blobs de archivos asociados con al menos un nombre de archivo que coincida con la expresión regular anterior. Dado que un documento puede haber sido encontrado por Software Heritage en muchos repositorios diferentes, con muchos nombres de archivo diferentes, los nombres de archivo recuperados con esta consulta son muy variados, aunque ningún documento fue recuperado si ninguno de los nombres de archivo asociados con él coincidía con la expresión. La consulta SQL se realizó en el conjunto de datos del grafo Software Heritage~\cite{Pietri2019} alojado en Amazon Athena (versión 2022-04-25).

La expresión regular elegida es relativamente laxa y, por lo tanto, coincide con muchos archivos que contienen datos distintos de textos de licencia o avisos de licencia. Esto ha hecho posible filtrar los blobs del conjunto de datos basados en los nombres de archivo utilizando los metadatos de \texttt{fileinfo}, pero es complicado extender el conjunto de datos hacia abajo para agregar todos los blobs de interés.

Luego se recuperan todos los blobs seleccionados del archivo de Software Heritage~\cite{DiCosmo2017} y los archivan en un único archivo \texttt{tar}. Además, se detalla que este proceso se puede replicar de forma independiente utilizando cualquier copia de archivo o espejo. El archivo \texttt{tar} es una materialización de la Colección de Documentos, y se puede considerar un punto de partida de la parte esencial de la presente tesis.
%, que conllevará a obtener un  nuevo conjunto de datos.

\section{Descripción de la colección de documentos}
\label{sec:descolecciondatos}

La ``colección de documentos'' se compone de \num{6859189} documentos, enviados en un único archivo tar \texttt{(blobs.tar.zst)}
comprimido~\cite{Collet2021} y con un peso aproximado de 13 GB. Cada documento es un ``blob'' (una secuencia única de bytes) correspondiente a una versión de un archivo cuyo nombre se incluye en la lista de nombres de archivos recuperada inicialmente por Software Heritage. El mismo blob puede aparecer en diferentes versiones de diferentes proyectos y con diferentes nombres de archivo, si todo su contenido es idéntico byte a byte. Por ejemplo, si una licencia se incluye en diferentes repositorios, exactamente en el mismo formato binario pero con nombres diferentes, todos estos archivos estarán representados en la ``colección de documentos'' mediante un único blob. Por el contrario, si una licencia está presente en diferentes formatos binarios, incluso si difieren solo en un byte (una
nueva línea agregada, por ejemplo), cada uno de esos archivos será un blob diferente.

Los documentos (blobs) se organizan en un archivo \texttt{tar} en una estructura de directorio compartido de dos niveles, basada en la suma de comprobación SHA1 de cada archivo. El nombre de archivo utilizado para cada documento es su suma de comprobación SHA1 serializada en formato hexadecimal. Los nombres de los directorios de primer nivel se componen de los dos primeros caracteres de todos los nombres de archivos de cada uno de ellos, y los nombres de los directorios de segundo nivel se componen de los dos segundos caracteres de todos los nombres de archivos de cada uno de ellos. Por ejemplo, la ruta del siguiente documento en el archivo expandido: \\ \texttt{blobs/02/52/0252d93ad297ec183a567ee813ab8c8d61ece655} corresponde a un documento de licencia cuya suma de comprobación SHA1 es \\ \texttt{0252d93ad297ec183a567ee813ab8c8d61ece655}.
Por lo tanto, los documentos se deduplican (eliminan duplicados) completamente en el conjunto de datos según las sumas de verificación SHA1: cada documento (blob) aparecerá solo una vez en la colección.

El conjunto de datos también incluye \texttt{blobs-sample20k.tar.zst}, un archivo más pequeño que contiene sólo \num{20000} archivos de licencia seleccionados al azar. Este conjunto de datos permitió realizar una inspección rápida y familiarizarse con los blobs.

\section{Metadatos y caracterización}
\label{sec:descripcarat}
Los metadatos de todos los documentos de la colección se proporcionan como un conjunto de archivos textuales CSV~\cite{Shafranovich2005} y JSON, comprimidos con Zstandard. La Figura~\ref{fig:methodology}, muestra los procesos desarrollados de dichos archivos.
\begin{figure}[h]
	\center
	\includegraphics[width=0.68\textwidth, height=0.35\textwidth]{4-1-conjuntodatos/img/methodology.jpg}
	\caption[Conjunto de Datos de Software Heritage]{Línea de construcción del conjunto de datos por Software Heritage}
	\label{fig:methodology}
\end{figure}

Cada archivo CSV corresponde a una tabla en el modelo relacional que se muestra en la Figura~\ref{fig:metadata-schema}. Los archivos CSV se pueden utilizar como tales (por ejemplo, importarse en marcos de datos R o Pandas) o importarse fácilmente a un sistema de gestión de bases de datos real (ver Figura~\ref{fig:usu_csv}).
Se pueden hacer referencias cruzadas a los metadatos con los documentos reales (en \texttt{blobs.tar.zst}) utilizando sumas de verificación de blobs como claves.

\begin{figure}[h]
	\center
	\includegraphics[width=0.85\textwidth]{5-metodologia/img/schema.jpg}
	\caption[Modelo relacional]{Modelo de datos relacional para metadatos de archivos~\cite{GonzalezBarahona2023}}
	\label{fig:metadata-schema}
\end{figure}

\begin{figure}[h]
	\center
	\includegraphics[width=0.95\textwidth, height=0.285\textwidth]{4-1-conjuntodatos/img/uso_cvs.jpg}
	\caption{Ejemplo de uso CSV con Pandas}
	\label{fig:usu_csv}
\end{figure}

Los archivos textuales CSV que se puede encontrar son: i) blobs, ii) fileinfo ,iii) scancode, vi) sample\_origins,y v) nb\_origins.

Cada archivo tabla/CSV captura metadatos. A continuación se describen los de interés para la investigación:

\begin{itemize}
	\item \texttt{\bfseries blobs} (archivo CSV: \texttt{license-blobs.csv.zst}) es el índice maestro de todos los documentos/archivos en el conjunto de datos. Cada fila en el archivo corresponde a un nombre de archivo para un documento dado\footnote{Si el documento se encontró bajo varios nombres de archivo diferentes, como podría suceder, aparecerá en el índice una vez por cada nombre de archivo diferente.} y contiene tres columnas:
	
	\begin{itemize}
		\item \texttt{sha1}: el SHA1 la comprobación de suma del documento (blob).
		\item \texttt{swhid}: el identificador persistente de Software Heritage (SWHID)~\cite{DiCosmo2018} del documento (blob). Los SWHID son identificadores estándar, persistentes e intrínsecos que pueden hacer referencia a varios tipos de artefactos de software (archivos, directorios, commits, versiones, etc.) comúnmente encontrados en VCS. Por ejemplo, el SWHID para (el archivo que contiene) una forma popular del texto de la GPL versión 3 es:\\
		\texttt{swh:1:cnt:94a9ed024d3859793618152ea559a168bbcbb5e2}.
		Similarmente a las comprobaciones de sumas SHA1 simples.
		%, los SWHID son calculados aplicando una función de comprobación de sumas criptográficas (actualmente: SHA1) de la manifestación digital, o contenido, de los artefactos de software. Los SWHID están explícitamente tipificados, versionados y son más expresivos que los SHA1 simples. 
		
		Los SWHID versión 1 (usados en este conjunto de datos) también son compatibles con los identificadores de objeto usados por el popular VCS Git, mientras que los SHA1 no lo son, debido al \textit{salting} agregado por Git antes de calcular los SHA1.
		En el contexto de este conjunto de datos, tanto los SWHID como los SHA1 se utilizan como claves para documentos de licencia, dependiendo de las tablas. Por lo tanto, el índice principal del conjunto de datos contiene \emph{both} identificadores para cada blob y puede ser utilizado como una tabla de traducción entre los dos.
		
		\item \texttt{filename}: el \texttt{filename} dado a un documento dado en un contexto dado (por ejemplo, uno o más commits en un repositorio Git público). 
		
		Tanto \texttt{swhid} como \texttt{sha1} son utilizados por otras tablas como objetivos de clave externa. No hay una columna de clave primaria única en esta tabla, debido a múltiples nombres de archivo asociados a cada documento.
	
	
	\item \texttt{\bfseries fileinfo} (archivo CSV: \texttt{blobs-fileinfo.csv.zst}) proporciona información básica y medidas de tamaño sobre los documentos (ver Figura~\ref{fig:csv_info}). Las columnas principales en este archivo son:
	\begin{figure}[h]
		\center
		\includegraphics[width=0.9\textwidth, height=0.24\textwidth]{4-1-conjuntodatos/img/csv_info.jpg}
		\caption[Información del archivo (fileinfo.csv)]{Información de  fileinfo.csv usando Pandas}
		\label{fig:csv_info}
	\end{figure}
	
	
	\begin{itemize}
		\item \texttt{sha1}: identificador del documento, referencia cruzada al archivo \texttt{blobs}.
		\item \texttt{mime\_type}, \texttt{encoding}: tipo MIME del documento y codificación de caracteres, detectados por \texttt{libmagic}~\cite{OpenGroup2018}.
		\item \texttt{size}: tamaño del documento en bytes.
		\item \texttt{line\_count}, \texttt{word\_count}: informan del tamaño del archivo en líneas y palabras (separadas por espacios en blanco), respectivamente, para archivos de texto.
	\end{itemize}
	
	\item \texttt{\bfseries scancode} (archivo CSV: \texttt{blobs-scancode.csv.zst} y archivo NDJSON\linebreak \texttt{blobs-scancode.ndjson.zst}) informa sobre la(s) licencia(s) contenida(s) en un documento dado, detectada(s) por la herramienta ScanCode~\cite{nexB2022, Ombredanne2020}.\footnote{{Versión utilizada: \texttt{ScanCode 31.2.1}.}} Pueden detectarse múltiples textos de licencia, o avisos de licencia, dentro de un solo documento, debido a la inclusión de múltiples textos de licencia o a diferentes niveles de confianza reportados por ScanCode (ver Figura~\ref{fig:csv_scancode}). Las columnas principales en este archivo son:
	
	
		\begin{figure}[h]
		\center
		\includegraphics[width=0.9\textwidth, height=0.23\textwidth]{4-1-conjuntodatos/img/csv_scancode.jpg}
		\caption{Información del archivo (scancode.csv) usando Pandas}
		\label{fig:csv_scancode}
    	\end{figure}
	
		
	\begin{itemize}
		\item \texttt{sha1}: identificador del documento, referencia cruzada al archivo \texttt{blobs}.
		\item \texttt{license}: licencia encontrada (ya sea como texto completo, o como aviso de licencia) en el documento, expresada usando el identificador estándar de la industria SPDX~\cite{Stewart2010,Gandhi2018} (por ejemplo, \texttt{``GPL-3.0-only''}).
		\item \texttt{score}: nivel de confianza de ScanCode como un flotante en el rango $[0,100]$ (siendo 100 la máxima confianza).
		\item \texttt{full}: columna ``virtual'' que contiene los resultados completos de ScanCode para cada documento en el conjunto de datos. Esta columna es virtual en el sentido de que no está realmente presente en el archivo CSV, sino que se materializa en el archivo JSON delimitado por saltos de línea. El archivo  \texttt{blobs-scancode.ndjson.zst} contiene un documento JSON por línea, donde cada archivo JSON contiene los resultados completos de ScanCode para un documento de licencia en el conjunto de datos. El documento JSON es un diccionario con tres claves:
		\begin{itemize}
			\item \texttt{sha1}: documento SHA1.
			\item \texttt{licenses}: salida completa de \texttt{scancode --license}, es decir, información completa sobre las licencias detectadas por ScanCode.\footnote{Detalles sobre el esquema JSON: \ \url{https://scancode-toolkit.readthedocs.io/en/stable/cli-reference/output-format.html} (accedido el 2022-11-09)}
			\item \texttt{copyrights}: salida completa de \texttt{scancode --copyright}, es decir, información completa sobre los avisos de derechos de autor detectados por ScanCode.
		\end{itemize}
	\end{itemize}
\end{itemize}
\end{itemize}

