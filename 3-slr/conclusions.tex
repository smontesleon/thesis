
\section{Validación}
\label{section:valida_rsl}
Siguiendo los principales tipos de amenazas a la validez en Ingeniería de Software Experimental~(ESE) propuestos por ~\cite{Wohlin2012}, se discuten los cuatro tipos: amenazas a la conclusión, amenazas internas, amenazas de construcción y amenazas externas con alcance específico de este estudio, estas amenazas no tienen un impacto significativo.

Consecuentemente, la amenaza a la validez de la conclusión se aborda adecuadamente, lo que nos permite afirmar con confianza que la aplicación del tratamiento de investigación está directamente relacionada con un resultado real.

La validez interna se refiere a la medida en que una conclusión causal basada en un estudio es respaldada por la minimización de errores sistemáticos. En nuestro caso, hemos buscado reducir esta amenaza siguiendo los procedimientos propuestos por~\cite{Tebes2019} para llevar a cabo una SLR. Además, hemos proporcionado artefactos o paquetes de replicación para permitir que terceros repliquen o reproduzcan nuestros estudios.

Si bien se ha seleccionado bases de datos ampliamente utilizadas como Scopus, IEEE Xplore y WOS como fuentes de publicaciones relacionadas con la \textit{``identificación y clasificación de licencias FOSS''}. Puede existir otras publicaciones no indexadas. También es posible que estudios previos ya estén incluidos en otros artículos, lo que podría resultar en la omisión de algunos trabajos previamente citados.

Un factor adicional que podría afectar la validez interna es la madurez del campo de estudio. Nuestro enfoque de clasificación utilizando ML puede haberse llevado a cabo en una etapa temprana, lo que podría limitar las conclusiones que se pueden extraer. Sin embargo, creemos que el período de investigación de 22 años cubierto en nuestro estudio es lo suficientemente extenso como para extraer lecciones válidas.

La validez de construcción se refiere a la precisión con la que un estudio de investigación mide lo que pretende medir. En este documento, hemos evaluado el impacto considerando el número de publicaciones e herramientas identificadas, así como la reproducibilidad a través de la disponibilidad de un conjunto de datos de replicación o una descripción y conjunto de datos detallados. Sin embargo, es importante reconocer el potencial de errores humanos en este proceso, con el objetivo de observar grupos de investigación especializados en el campo y evaluar su nivel de actividad.

La validez externa se refiere a la medida en que los resultados de un estudio de investigación pueden generalizarse a otros contextos. En el caso específico de nuestro estudio sobre la \textit{``identificación y clasificación de licencias FOSS''}, es importante señalar que no podemos reclamar la generalizabilidad de nuestros resultados a todo el campo de ESE. Sin embargo, es importante reconocer el valor de los estudios de caso como el que hemos realizado. A través de nuestro estudio, pudimos identificar los componentes o técnicas utilizados en el desarrollo de software al identificar licencias FOSS en el Proyecto de Investigación y Desarrollo PID~(392)~\cite{Ombredanne2020}, así como la aplicación de ML para la clasificación.

Si bien los resultados específicos pueden no ser generalizables a otros contextos de ESE, los hallazgos y la metodología empleados en nuestro estudio pueden proporcionar información valiosa para investigaciones futuras en el campo.

En un contexto de evaluación diferente, considerando el creciente uso de la Inteligencia Artificial~(IA), utilizamos la plataforma "Elicit: The AI Research Assistant"\footnote{\url{https://elicit.org/} Elicit utiliza ML para ayudarte con tu investigación: encontrar artículos, extraer afirmaciones clave, resumir, generar ideas, y más.}, como un caso práctico el 12 de julio de 2023~(Elicit encontrará respuestas de 175 millones de artículos). Se aplica la cadena de búsqueda de la Tabla~\ref{tab:SLR-A1-A2} de la misma manera que lo hicimos en cada base de datos para obtener artículos relevantes iniciales. Esta plataforma web se considera un sólido punto de partida para llevar a cabo una SLR.

Respecto al conjunto de datos obtenido\footnote{\url{https://drive.google.com/drive/folders/11CiOvuuXIJKiVoeJN8YZpL4UBYmFq0Dc}}, al compararlo con los $ 43 $ artículos identificados en nuestra investigación y aplicar los filtros de búsqueda, obtuvimos 77 documentos. De estos $ 77 $ documentos, $ 15 $ están directamente relacionados con nuestra investigación. Además, encontramos que $ 13 $ de estos $ 15 $ artículos se alineaban con nuestros documentos seleccionados para nuestra investigación.

Estos hallazgos refuerzan aún más la validez y relevancia de nuestros resultados, demostrando la convergencia entre la información obtenida de diferentes fuentes. Es importante destacar que estos resultados respaldan la importancia y aplicabilidad de nuestra investigación en el contexto de la Inteligencia Artificial, ya que identificamos artículos relevantes tanto de la plataforma web seleccionada como de nuestros propios hallazgos.

\section{Conclusiones}
\label{section:conclu_rsl}
Se ha realizado un estudio de caso sobre la práctica de ESE a través de una SLR sobre la identificación y clasificación de licencias FOSS. Revisamos un conjunto de 1790 artículos, aplicando estrategias de evaluación, métodos de recopilación de datos y otros aspectos relacionados con las evaluaciones, lo que resultó en una muestra final de $ 43 $ estudios.

El estudio arroja luz sobre algunos de los desafíos enfrentados en la investigación de ESE al abordar la identificación de licencias FLOSS y las técnicas y metodologías utilizadas. Se identifica que muchas publicaciones en este campo carecen de reproducibilidad debido a la falta de disponibilidad de las herramientas y conjuntos de datos utilizados. Durante el desarrollo de la SLR, fue posible identificar en el documento ``Metrics\_all\_Documents.ods'' y bajo la etiqueta ``Dataset'' que el conjunto de datos más grande utilizado y accesible por el PID 1256~\cite{Zacchiroli2022} consiste en 6 millones de archivos que pesan un total de $ 13 $ GB. Este conjunto de datos fue empaquetado a partir de 20`21 y está disponible en el siguiente repositorio.

Además, se observa que cuando se trata de la identificación de licencias FLOSS, la mayoría de los investigadores confían en herramientas existentes, mientras que solo un número limitado ha desarrollado nuevas herramientas, y pocos grupos de investigación están involucrados en esta área. En nuestro análisis de los documentos, identificamos un total de $ 42 $ herramientas de software, de las cuales 38 son accesibles para su uso.

En cuanto a la clasificación de licencias FOSS, se encontró que herramientas como Ninka, Fossology y Scancode pueden realizar esta tarea. Sin embargo, nuestro enfoque se centra en el uso de ML para la clasificación, y solo identificamos un artículo que aborda específicamente este tema, lo que indica que la investigación en esta área todavía está en sus primeras etapas.

En resumen, el estudio destaca que los investigadores trabajan con diversas fuentes de datos para la identificación y clasificación de licencias FOSS. También hemos identificado una vasta colección de datos para abordar este tema relacionado con licencias, y los investigadores emplean técnicas estadísticas clásicas y validación manual, el Coeficiente Cohen de Kappa se ha empleado para confirmar la validez de la validación manual.

\begin{itemize}
	\item Abordar la identificación de licencias FLOSS y qué software utilizar; y
	En cuanto a la clasificación de licencias FLOSS y el uso de Machine Learning, todavía hay mucho espacio para la exploración y experimentación.
	\item Así también para futuras investigaciones, es importante abordar las limitaciones de este estudio y ampliarlo a otros dominios. Existe una clara necesidad de utilizar conjuntos de datos grandes y accesibles, para aplicar técnicas de ML en la clasificación de licencias FLOSS.
\end{itemize}El estudio de SLR ha proporcionado los siguientes aportes a la presente tesis:




