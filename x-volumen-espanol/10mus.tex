\subsection*{Dashboards de datos en realidad virtual}


En este capítulo se presenta un experimento controlado que compara dos enfoques de visualización de datos: un panel de visualización en 2D utilizando \kibana frente a una visualización inmersiva en realidad virtual (RV) utilizando \babia. Se exploran los beneficios y desafíos asociados con la visualización de datos en RV y se investiga su efectividad y eficiencia en la comprensión y análisis de datos relacionados con el desarrollo de software. \kibana es una herramienta popular utilizada para visualizar datos, mientras que \babia es una herramienta desarrollada por Bitergia y la Universidad Rey Juan Carlos que permite la visualización de datos en 3D tanto en pantallas como en dispositivos de RV.

El experimento se realizó con la participación de 32 personas de diferentes ámbitos, tanto académicos como industriales. Se diseñaron preguntas específicas que requerían que los participantes utilizaran la información de las visualizaciones para proporcionar respuestas precisas. Cada participante experimentó tanto la visualización en 2D como la visualización en RV en un orden aleatorio, utilizando datos de proyectos diferentes en cada entorno. Se evaluaron la corrección de las respuestas y el tiempo necesario para responder en ambos entornos.

Las contribuciones de este capítulo incluyen la investigación de la efectividad y eficiencia comparativa de la visualización en RV y en 2D en pantalla para la comprensión de datos de ingeniería de software, el uso de un diseño de experimento registrado previamente para garantizar la transparencia y reproducibilidad, y la colaboración con la industria para desarrollar preguntas personalizadas y utilizar datos del mundo real. Esto proporciona información valiosa sobre el potencial de la visualización en RV en el análisis de datos de desarrollo de software y puede ayudar a mejorar la calidad, eficiencia y toma de decisiones en esta área.

\subsubsection*{Implementación}


El capítulo de "Implementación" presenta el diseño y la ejecución del experimento realizado en el estudio. Se sigue el diseño de un experimento controlado siguiendo los estándares empíricos de ACM SIGSOFT. El experimento consiste en una serie de tareas que los participantes realizan en dos entornos diferentes: en pantalla y en realidad virtual. Cada tarea se repite dos veces para evaluar el impacto del aprendizaje y la fatiga. Las variables principales medidas son el tiempo y la corrección de las respuestas. Se utilizan dashboards basados en Kibana y escenas de realidad virtual \babia para proporcionar visualizaciones de datos a los participantes. Antes del experimento, los sujetos reciben un breve entrenamiento. Además, se describe el proceso de recuperación y procesamiento de datos utilizando \grimoirelab y \elasticsearch.

\begin{enumerate}
    \item \textbf{Issues Timing}: displays information about the time to close issues.
    \item \textbf{Pull Requests Timing}: displays information about the timing of pull requests.
    \end{enumerate}
    
    \begin{figure}[H]
        \begin{subfigure}[b]{\columnwidth}
            \centering
            \includegraphics[width=\linewidth]{10-mus/img/prtiming.png}
            \label{10-mus:fig:kibanadashboard1es}
        \end{subfigure}
    
        \vspace{0.5em}
    
        \begin{subfigure}[b]{\columnwidth}
            \centering
            \includegraphics[width=\linewidth]{10-mus/img/Issuestiming.png}
            \label{10-mus:fig:kibanadashboard2es}
        \end{subfigure}
        \caption{Layout of the \kibana dashboards used for the experiment: Pull Requests Timing (top) and Issues Timing (bottom). Both are for the CHAOSS project as of late June 2022. While the data in the figure is not visible, the layout itself holds significance as it provides an understanding of the visual presentation and organization of the dashboards.}
        \label{10-mus:fig:kibanadashboardes}
    \end{figure}

El experimento se realiza con 32 participantes y permite comparar el rendimiento en diferentes entornos y con diferentes conjuntos de datos. Se utilizan auriculares Oculus Quest 2 para el entorno de realidad virtual y un navegador web en una pantalla de 13 a 15 pulgadas para el entorno en pantalla. Los participantes deben hablar en voz alta durante todo el experimento, y se graba un video para verificar las respuestas y el tiempo registrado por el supervisor. Se utiliza un formulario de retroalimentación para recopilar comentarios y posibles mejoras.

En cuanto a la visualización de datos, se utilizan dashboards en \kibana y escenas de realidad virtual \babia. Los dashboards de \kibana muestran información sobre el tiempo de cierre de problemas y solicitudes de extracción. Las escenas de \babia proporcionan visualizaciones en 3D para explorar los datos de manera más inmersiva. Se utiliza \grimoirelab para recuperar y procesar los datos de los repositorios y \elasticsearch para almacenarlos. El conjunto completo de dashboards de \kibana utilizados en el experimento se ha validado y probado previamente por los clientes de Bitergia, asegurando su idoneidad y eficacia en tareas de análisis de datos.


\begin{figure}[H]

    \centering
    \includegraphics[width=\linewidth]{10-mus/img/Issuesprvr.png}
    \caption{A \babia dashboard used in the experiment. Visualizations are in shelves, placed in a room mimiking a museum room. Data related to pull requests is visualized in bottom shelves, data related to issues is in top shelves.}
    \label{10-mus:fig:babiaenvironmentses}
\end{figure}

\subsubsection*{Experimento}

Se siguieron las pautas de informe propuestas por Jedlitschka \etal~\cite{Jedlitschka} para garantizar una presentación integral y transparente de nuestra metodología experimental. Estas pautas brindan un marco estructurado para informar estudios experimentales, que abarcan aspectos clave como el diseño de investigación, los participantes, la recopilación de datos y el análisis estadístico. Siguiendo estas pautas, nuestro objetivo era mejorar la claridad, la replicabilidad y la calidad general de nuestro estudio.

El objetivo principal del experimento es evaluar si la visualización de procesos de desarrollo de software, específicamente métricas relacionadas con la revisión de código moderna y la gestión de problemas, es igualmente efectiva y satisfactoria cuando se presenta en escenas de realidad virtual (RV) en comparación con pantallas 2D tradicionales.

La pregunta de investigación principal de nuestro estudio es:

\begin{quote}
RQ: {\em "¿La comprensión de los procesos de desarrollo de software, a través de la visualización de sus métricas, es al menos tan buena como en las pantallas 2D cuando se presenta en escenas de RV?"}.
\end{quote}

Esta pregunta pone a prueba la hipótesis de que presentar visualizaciones en RV, donde el espacio disponible es mucho más amplio (puedes tener visualizaciones a tu alrededor, colocarlas en diferentes alturas), permitirá una mejor y más rápida comprensión. La hipótesis es discutible porque existen factores que juegan en su contra, como las dificultades que la perspectiva y la distancia pueden causar para la percepción adecuada de las magnitudes.

Para responder a la pregunta de investigación y validar (o no) la hipótesis, nos centraremos en aspectos específicos y medibles de las respuestas proporcionadas por los sujetos: precisión (como un indicador de corrección) y tiempo de finalización (como un indicador de eficiencia). Así, podemos refinar nuestra pregunta principal en dos:

\begin{quote}
RQ1: {\em "¿Las respuestas obtenidas en RV proporcionan una corrección similar en comparación con las obtenidas en la pantalla?"}
\end{quote}

\begin{quote}
RQ2: {\em "¿Las respuestas obtenidas en RV proporcionan un tiempo de finalización similar en comparación con las obtenidas en la pantalla?"}
\end{quote}

La corrección se mide comparando la diferencia entre la respuesta proporcionada y la respuesta correcta, y el tiempo empleado en responder por el período de tiempo necesario para producir la respuesta en unidades de tiempo (es decir, en segundos).

Nuestro experimento involucró a 32 participantes tanto de la academia como de la industria. Dividimos aleatoriamente a los participantes en cuatro grupos de ocho personas. Los participantes en cada uno de los grupos realizaron primero el experimento con datos de uno de los proyectos en uno de los entornos (en pantalla o en RV) y luego con datos del otro.


Detalles de las tareas y procedimiento se encuentran en la Sección~\ref{10-mus:sec:experiment}.


\subsubsection{Resultados, discusión y conclusiones}

Después de analizar los resultados, podemos concluir que \babia (VR) es al menos tan útil en términos de corrección y tiempo de finalización como \kibana (\onscreen). Sin embargo, hay diferentes puntos y casos específicos que debemos discutir a partir de los resultados, la retroalimentación y otras ideas.

\textbf{Factor de hábito.} La retroalimentación a favor de \onscreen está relacionada principalmente con el hábito que los participantes tienen con el uso de la computadora. Hoy en día, la computadora es una herramienta ampliamente utilizada en la mayoría de los trabajos modernos, por lo que el uso de una pantalla, un mouse y un teclado es mucho más común que los auriculares y controladores de VR. A pesar de tener más hábito con la pantalla, los resultados no están muy alejados. Nuestra hipótesis es que a medida que los entornos de VR se vuelvan más comunes, o los desarrolladores se familiaricen más con la tecnología de VR para realizar estas tareas, los resultados pueden cambiar a favor de VR. Replicar este experimento en el futuro podría ser interesante para confirmar si esto es cierto.

\textbf{Dos rondas de las mismas tareas.} Es evidente que tanto en la corrección como en el tiempo de finalización, se observaron mejoras en la segunda ronda, independientemente del entorno. Este resultado es esperado, ya que los participantes adquieren familiaridad con las visualizaciones y tareas a lo largo del tiempo. Aunque el entorno y el proyecto (con diferentes niveles de complejidad) cambien, los participantes adquieren conocimiento sobre dónde buscar y los objetivos de cada tarea. Las tareas fueron diseñadas cuidadosamente para reflejar preguntas comunes abordadas por Bitergia en entornos de desarrollo de software \emph{reales}. Vale la pena destacar que una capacitación previa más intensiva, que explique específicamente los datos mostrados en cada visualización, podría potencialmente normalizar los tiempos de finalización entre las dos rondas. Esta observación indica que ambos entornos son efectivos para aprender a realizar este tipo de tarea.

\textbf{Entorno de VR aislado.} La inmersión en VR tiene la ventaja de que controlas todo lo que te rodea en un espacio casi ilimitado. Esto significa que puedes colocar cosas de interés en todos los aspectos y controlar aquellos aspectos que pueden distraer al participante durante un experimento. Esto es algo que varios participantes mencionaron a favor de VR. Aunque también señalaron algunas partes negativas, destacaron que en VR tienes un enfoque más controlado y no sientes distracciones externas, ya que prácticamente todo lo que ves es visualización en todas las direcciones. Este es un punto muy importante, ya que en VR hemos combinado las vistas de problemas y solicitudes de extracción, pero aún así, el entorno no se aprovechó al 100\%. Por lo tanto, este punto es algo que se debe investigar y, sin duda, se debe aprovechar más para mostrar más información en futuros experimentos.

\textbf{Tareas que favorecen a VR.} Específicamente, en la \task{5} encontramos una diferencia sustancial a favor de VR. Esta tarea está diseñada específicamente para correlacionar datos de problemas y solicitudes de extracción, primero buscando remitentes en la visualización de "Días promedio abiertos para problemas" y luego buscando esos mismos remitentes en la visualización correspondiente de solicitudes de extracción. Como en muchos proyectos de software, hay perfiles de usuarios que pueden crear problemas y solicitudes de extracción, pero también pueden crear problemas sin solicitudes de extracción, porque, por ejemplo, no tienen un perfil de desarrollador (o viceversa). Solo en la \task{5}, al buscar remitentes en la visualización de solicitudes de extracción, no se encontraron porque no habían creado solicitudes de extracción. Esto se puede identificar claramente cuando tienes ambas visualizaciones disponibles. En el entorno \onscreen, tienes que moverte entre paneles en diferentes páginas web para correlacionar la información. En VR, en cambio, tienes una visualización directamente sobre la otra, pudiendo correlacionar la información de manera más rápida, como se puede ver en los resultados. Esto nos da una idea del tipo de tareas que se resuelven más fácilmente en el entorno de VR. Definir escenarios diseñados para facilitar la finalización de ese tipo de tareas en VR puede llevar a la creación de paneles de VR más eficientes y poderosos que los posibles \onscreen.

\textbf{Tareas que favorecen a un proyecto.} A primera vista, puede parecer que ciertas tareas están diseñadas para favorecer a un proyecto en particular según su tamaño. Sin embargo, es importante destacar que Bitergia, como diseñador de los paneles y colaborador clave en el diseño de las tareas, colabora con clientes cuyos intereses abarcan proyectos de diferentes magnitudes. En consecuencia, las tareas y preguntas se elaboran cuidadosamente para asegurar que el tamaño del proyecto no influya en los resultados. En nuestro estudio, el impacto principal del tamaño de los datos se encuentra en el rendimiento de la aplicación en sí. Con las limitaciones tecnológicas actuales, los paneles de VR pueden mostrar velocidades de renderizado más lentas que los paneles \onscreen para conjuntos de datos más grandes. Sin embargo, en nuestro experimento no observamos diferencias significativas en el rendimiento entre los dos proyectos, ya que las visualizaciones y preguntas se seleccionaron cuidadosamente para garantizar un rendimiento consistente y comparable. Con respecto a la discrepancia en los resultados de la \task{1} entre los proyectos, la atribuimos al tiempo de reacción inicial de los participantes al encontrarse con los datos por primera vez. Esta variación podría estar relacionada con los diferentes tamaños de los datos. Sin embargo, con un tamaño de muestra de participantes más grande, anticipamos que esta diferencia disminuirá, como demuestran los resultados de las otras tareas.

\textbf{VR frente a \onscreen según los participantes.} Después del análisis cualitativo de la encuesta de retroalimentación, nos dimos cuenta de que los participantes valoran estar familiarizados con el entorno por encima de otros factores (colores, diseño, etc.). En general, todos se sintieron más cómodos en la pantalla y reportaron que percibían las tareas como más fáciles en este entorno. Es interesante cómo en VR varios sujetos detallaron algunas ventajas, como el uso del espacio disponible, algo que incluso destacaron como una desventaja \onscreen. Otro aspecto muy interesante de VR sobre \onscreen es el hecho de que los participantes se sintieron más concentrados en VR porque no tenían distracciones a su alrededor, y la información estaba más dispersa en el espacio, algo que la metáfora del museo refleja muy bien. Esto también se aplica a la colocación de las visualizaciones en estantes para una mejor sensación de comodidad y familiaridad. Hasta el día de hoy, no hay un uso generalizado de VR, y mucho menos para este tipo de aplicaciones. Las aplicaciones como \babia necesitan más desarrollo para mejorar los problemas de visualización, rendimiento e interacción. Por lo tanto, en cierta medida, este experimento podría ser una línea de base: una vez que la VR esté más extendida, la interacción del usuario y las metáforas se mejoren en VR, y los sujetos estén más acostumbrados a las interacciones y metáforas específicas utilizadas en el panel de VR, tal vez un experimento similar muestre mejores resultados para VR. En cualquier caso, también es importante notar que, con respecto a la corrección, no hubo diferencias percibidas, y ambos entornos funcionaron bien. Dado que \kibana es de última generación en cuanto a mostrar visualizaciones para interactuar con datos, creemos que esta es una muy buena noticia para la visualización de datos en VR.

\textbf{Metáfora de VR.} El entorno de VR permite el uso de diferentes metáforas para el diseño de las visualizaciones. En este caso, se ha elegido la metáfora del museo, colocando las visualizaciones en un entorno similar al de un museo, con una sala limitada y diferentes elementos a diversas alturas con sus respectivos títulos/afiches. Aún no tenemos suficiente retroalimentación para saber si esta metáfora es la adecuada para este tipo de visualizaciones y datos, por lo que utilizar una metáfora diferente puede cambiar drásticamente los resultados. Lo que este experimento ha mostrado es que la metáfora del museo para estos datos y tareas es similar en términos de efectividad y eficiencia al entorno \kibana \onscreen. El uso de movimientos naturales, como agacharse, mover los brazos y gestos de la cabeza, es algo a explorar para mejorar la metáfora a utilizar.

\textbf{Interacciones en el entorno \onscreen.} La interacción con \kibana se basa en el teclado y el mouse, una interacción muy madura que ha sido ampliamente utilizada por todos los participantes que han realizado el experimento. Es una interacción natural que aún ha sido limitada para una comparación justa con \babia. Las mejores formas de interactuar con las visualizaciones en VR aún se están investigando, y a medida que se realicen experimentos como este, aparecerán formas mejores y más naturales de interactuar, similares al entorno \onscreen. Además, hemos limitado el uso de \onscreen a un monitor, por lo que el uso de más de un monitor podría influir en los resultados, debido a una mayor correlación de los datos.

\textbf{\babia como biblioteca.} La biblioteca \babia en sí misma es una herramienta muy flexible y adaptable. Aunque se ha utilizado en el entorno de VR para este experimento, se puede utilizar en otros entornos, como en un navegador web \onscreen o en otro entorno de VR con diferentes metáforas o características. La biblioteca \babia es lo suficientemente versátil como para adaptarse a diferentes necesidades y contextos. Por lo tanto, este experimento también proporciona información sobre la utilidad de \babia como biblioteca en general.

En resumen, este experimento nos ha permitido comparar la eficacia y eficiencia de \babia en un entorno de VR con \kibana en un entorno \onscreen. Aunque ambos entornos demostraron ser efectivos para realizar tareas de visualización de datos, hubo algunas diferencias notables en términos de preferencia y tiempo de finalización en ciertas tareas. El entorno de VR ofrece una inmersión única y una mayor concentración, mientras que el entorno \onscreen es más familiar y cómodo para la mayoría de los participantes. Sin embargo, a medida que la tecnología de VR avance y se perfeccione, es probable que las ventajas del entorno de VR se vuelvan más prominentes y los resultados cambien. Además, se deben explorar y mejorar las interacciones en el entorno de VR para proporcionar una experiencia más natural y fluida. En general, este experimento sienta las bases para futuras investigaciones y desarrollo de aplicaciones de visualización de datos en entornos de VR.
