\subsection*{Visualizando ecosistemas de dependencias}

Los sistemas de software modernos están compuestos por módulos interdependientes, y comprender estas dependencias es crucial para su desarrollo y mantenimiento. El uso de componentes de software de código abierto (FOSS) ha llevado al surgimiento de registros de software, como el registro del \tool{Node Package Manager} (\npm), que se ha convertido en el principal mecanismo para compartir módulos en el desarrollo web. El crecimiento de las dependencias en \npm ha generado la necesidad de visualizar y comprender el ecosistema de dependencias. Según estudios~\cite{williams2012unfortunate}, hasta el 80\% del código en las aplicaciones de software actuales proviene de bibliotecas y frameworks.

La visualización de las dependencias de software proporciona una forma poderosa de comprender la complejidad de los sistemas modernos~\cite{koschke2003software}. Aunque los formatos de texto y tablas son útiles para presentar información de manera estructurada, carecen de la capacidad para transmitir las relaciones matizadas entre los componentes del ecosistema. Por el contrario, las representaciones visuales permiten a los desarrolladores ver el ecosistema completo de manera intuitiva y comprensible, identificando áreas sensibles al cambio y tomando decisiones informadas~\cite{koschke2003software}. Estas visualizaciones también fomentan la comunicación y colaboración efectiva entre desarrolladores, impulsando la innovación y mejora continua.

Existen diversas formas de visualizar las dependencias de software. La estructura de árbol es comúnmente utilizada, donde los módulos se organizan jerárquicamente según sus dependencias~\cite{Falke}. Otras opciones incluyen los Treemaps y las Matrices de Estructura de Dependencia. Las visualizaciones de grafo o red también son poderosas para representar las dependencias, destacando el acoplamiento y el flujo de información~\cite{graphdep}. Sin embargo, comprender y navegar por estas visualizaciones puede ser desafiante, por lo que es fundamental diseñarlas cuidadosamente para adaptarse a las necesidades y experiencia de los usuarios. En este contexto, se ha propuesto el uso de una metáfora de "ciudad elevada" en realidad virtual~\cite{codecity}, donde los módulos se representan como edificios y las dependencias se visualizan mediante la elevación de vecindarios de edificios.

En resumen, comprender y visualizar las dependencias de software es esencial para el desarrollo moderno. Las visualizaciones permiten comprender la complejidad del ecosistema, identificar áreas sensibles al cambio y promover la colaboración y la toma de decisiones informadas. Diferentes técnicas, como las estructuras de árbol, los grafos y las metáforas visuales, ofrecen enfoques variados para abordar este desafío. Al elegir una técnica de visualización, es importante considerar las características y necesidades específicas del sistema y de los usuarios involucrados.

\subsubsection{La metáfora de la ciudad elevada}

La ciudad está compuesta por edificios y distritos. Cada edificio representa un paquete, correspondiente a un paquete de \npm que es una dependencia transitoria de la aplicación. Cada distrito está representado por una plataforma que incluye edificios para todas las dependencias directas de un paquete. El nivel del distrito corresponde al nivel de dependencia, siendo más alto a medida que las dependencias son más profundas. Los edificios del primer nivel son las dependencias directas encontradas en el archivo \textit{package.json} del proyecto.

Cada nivel tiene un degradado de color, desde verde oscuro hasta verde claro, para facilitar la identificación de los niveles de dependencia. El ancho vertical de las plataformas que representan los distritos siempre es el mismo. Si un edificio se encuentra en un distrito, su base comienza desde la plataforma, lo que indica que el paquete es una dependencia del edificio debajo del distrito.


\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.49\linewidth}
        \centering
        \includegraphics[width=\textwidth]{7-dep/imgs/buildingdependency.png}
        \caption{Distrito de primer nivel.}
        \label{7-dep:fig:y equals xes}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.49\linewidth}
        \centering
        \includegraphics[width=\textwidth]{7-dep/imgs/buildingdependency2.png}
        \caption{Distrito de tercer nivel.}
        \label{7-dep:fig:three sin xes}
    \end{subfigure}
    \caption{Ejemplos de edificios que pertenecen a un distrito: edificios dentro del marco rojo pertenecen al distrito resaltado en amarillo/marrón.}
    \label{7-dep:fig:buildingquarteres}
\end{figure}

Cuando un edificio, debido a sus dependencias salientes, "genera" un distrito, la base del distrito se encuentra debajo del edificio y el edificio "atraviesa" su base. Si el edificio no es lo suficientemente alto como para atravesar la base de su distrito, se muestra una línea desde la parte superior del edificio hasta su base. Por lo tanto, si un edificio no atraviesa un distrito o no tiene una línea encima de él, significa que el paquete no tiene dependencias.

\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.49\columnwidth}
        \centering
        \includegraphics[width=\textwidth]{7-dep/imgs/buildingdependencyowner.png}
        \caption{Paquete (en marco rojo) lo suficientemente alto para atravesar el distrito de sus dependencias (en azul).}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.49\columnwidth}
        \centering
        \includegraphics[width=\textwidth]{7-dep/imgs/buildingshort.png}
        \caption{Edificios cortos con líneas amarillas al distrito donde se encuentran sus dependencias.}
    \end{subfigure}
    \caption{Ejemplos de edificios donde se originan sus dependencias.}
    \label{7-dep:fig:ownerses}
\end{figure}


\textbf{Métricas}

Tres métricas se pueden asignar a un edificio: \textit{altura}, \textit{área} y \textit{color}.

\begin{itemize}

    \item La métrica asignada a la altura siempre es \textit{age\_days}: la edad del paquete (en días).

    \item Hay tres métricas que se pueden asignar al área:

          \begin{itemize}
              \item \textbf{loc/age}: líneas de código del paquete dividido por la edad del paquete (en días).
              \item \textbf{size/age}: tamaño del paquete (en bytes) dividido por la edad del paquete (en días).
              \item \textbf{ncommits/age}: número de commits del repositorio git del paquete dividido por la edad del paquete (en días).
          \end{itemize}

          Todas estas métricas se dividen por la edad para que el volumen del edificio represente una métrica significativa: líneas de código, tamaño o número de commits.

    \item Hay varias métricas que se pueden asignar al color. Si la métrica es categórica, cada valor tendrá un color definido. Si la métrica es numérica, el color del edificio seguirá una paleta continua de azul a rojo. El usuario puede seleccionar cualquiera de estas métricas en tiempo real:

          \begin{itemize}
              \item \textbf{license} (categórico): licencia del paquete.
              \item \textbf{timesInstalled} (numérico): cuántas veces se instala un paquete. Para las dependencias de \npm, si se utiliza el mismo paquete como dependencia en varias versiones, cada una de estas versiones se instalará. Por lo tanto, esta métrica también representa el número de versiones diferentes del paquete en la lista de dependencias transitorias.
              \item \textbf{timesAppear} (numérico): cuántas veces aparece un paquete como dependencia, independientemente de la versión. Esto también es el número de edificios que se pueden encontrar en toda la ciudad para ese paquete.
              \item \textbf{last\_act\_days} (numérico): el número de días desde el último commit en el repositorio de un paquete.
              \item \textbf{ncommits\_ly} (numérico): el número de commits durante el último año en un repositorio de paquete.
              \item \textbf{ncommiters\_ly} (numérico): el número de colaboradores diferentes durante el último año en un repositorio de paquete.
              \item \textbf{nvuln} (numérico): el número de vulnerabilidades de un paquete.
              \item \textbf{nissues\_ratio} (numérico): el número de incidencias cerradas dividido por el número total de incidencias en el repositorio de un paquete.
          \end{itemize}

\end{itemize}


\textbf{Iteracciones}

Mientras el usuario está inmerso en RV (realidad virtual) o RA (realidad aumentada), puede ver un panel con selectores para las fuentes de datos, las métricas para la base y el color de los edificios, y para representarlos como sólidos, transparentes o en forma de alambre (ver Figura \ref{7-dep:fig:uivres}). Este panel puede ocultarse al presionar el botón central del controlador. Para seleccionar en el panel, el usuario puede "disparar" con el raycaster del controlador derecho en los selectores.

\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth, keepaspectratio]{7-dep/imgs/uivr.png}
    \caption{Interfaz de Usuario en el control izquierdo.}
    \label{7-dep:fig:uivres}
\end{figure}

Existen casos en los que un paquete es una dependencia directa de más de un paquete. Estos paquetes "repetidos" pueden mostrarse como edificios transparentes y/o en forma de alambre, como se muestra en Figura \ref{7-dep:fig:repeatedes}. Esta opción se puede activar/desactivar utilizando la primera fila llamada \textit{Atributos} de la interfaz de usuario.

\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{\columnwidth}
        \centering
        \includegraphics[width=\textwidth]{7-dep/imgs/transparency.png}
        \caption{Edificios repetidos transparentes.}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{\columnwidth}
        \centering
        \includegraphics[width=\textwidth]{7-dep/imgs/wireframe.png}
        \caption{Edificios repetidos mostrando solo los bordes.}
    \end{subfigure}
    \caption{Ejemplos de transparencia y marcos para mostrar los edificios repetidos.}
    \label{7-dep:fig:repeatedes}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth, keepaspectratio]{7-dep/imgs/replicas.png}
    \caption{Un paquete y sus réplicas en color blanco cuando se señalan/disparan.}
    \label{7-dep:fig:whitedepes}
\end{figure}


Por defecto, estas opciones hacen transparentes o en forma de alambre todos los edificios repetidos. Para encontrar los paquetes repetidos de un edificio específico, el usuario puede apuntar o "disparar" a un edificio: sus edificios repetidos se resaltarán en blanco. Figura \ref{7-dep:fig:whitedepes} muestra este comportamiento. Si se apunta a un distrito, aparece una caja gris transparente para resaltarlo, con un panel en negro que muestra la ruta de su nivel de dependencia.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\columnwidth, keepaspectratio]{7-dep/imgs/infopanel.png}
    \caption{Panel de información de métricas.}
    \label{7-dep:fig:metricsmpaneles}
\end{figure}

Cerca de la ciudad, hay un panel con un resumen de las métricas, y si la métrica de color es categórica, cerca de ella habrá otro panel con el color definido y sus valores. Encima de eso, como se muestra en Figura \ref{7-dep:fig:metricsmpaneles}, hay un botón para cerrar todas las leyendas abiertas en la ciudad, con el objetivo de limpiarla si muchas están abiertas.




\subsubsection{Vistas}

Dependiendo de las métricas que se asignen a los colores de los edificios en la ciudad elevada, podemos identificar cuatro "vistas". Además, para validar la metáfora de la ciudad elevada en \babia, preparamos un pequeño experimento con 4 profesionales de la industria en el cual le dejamos un entorno de entrenamiento con datos no significativos durante un tiempo limitado para que se acostumbraran a la visualización y métricas y luego un tiempo determinado en un proyecto real en el cual tendrían que contar la información que obtenían de cada vista. Podemos detallar la siguiente discusión tras el experimento. Las vistas y los resultados del experimento, son las siguientes:

{\bf Licencia.} Con la métrica \textit{license} seleccionada como color, cada edificio tiene un color determinado por su licencia. En el lado derecho de la escena, hay una leyenda de colores con las diferentes licencias encontradas en los paquetes. En esta vista, el usuario puede verificar fácilmente, por ejemplo, las licencias más utilizadas en el proyecto o si una determinada rama de dependencias tiene licencias no compatibles.

\begin{mdframed}[linewidth=1.2pt, backgroundcolor=gray!5, linecolor=black]
    En general, los comentarios sobre la vista de licencias sugieren que es una vista valiosa para gestionar las licencias de software y garantizar el cumplimiento de las políticas del proyecto. Al proporcionar una visualización clara e intuitiva de las licencias de software, este enfoque puede ayudar a los desarrolladores a tomar decisiones informadas sobre qué dependencias utilizar y asegurarse de que sus proyectos de software cumplan con la legalidad.
\end{mdframed}

{\bf Replicación.} Esta vista incluye asignar las métricas \textit{timesInstalled} o \textit{timesAppear} a los colores. Tener el mismo paquete en varias versiones entre las dependencias instaladas generalmente es un indicio negativo que puede causar problemas. Asignar \textit{timesInstalled} a los colores facilita identificar esos paquetes y en qué partes del árbol de dependencias se encuentran (cuando se combinan con las características de alambre y transparencia). Asignar \textit{timesAppear} a los colores ayuda a identificar rápidamente los paquetes que se instalan porque son dependencias de muchas otras dependencias.

\begin{mdframed}[linewidth=1.2pt, backgroundcolor=gray!5, linecolor=black]
    En general, los comentarios sobre la vista de replicación sugieren que es una vista valiosa para identificar dependencias populares y fundamentales, así como posibles problemas de rendimiento. Al proporcionar una visualización clara e intuitiva de la replicación de paquetes, este enfoque puede ayudar a los desarrolladores a tomar decisiones informadas sobre qué dependencias priorizar para el mantenimiento y asegurarse de que sus proyectos de software sean eficientes y rendimiento óptimo.
\end{mdframed}


{\bf Actividad.} Esta vista incluye asignar \textit{last\_act\_days}, \textit{ncommits\_ly} o \textit{ncommiters\_ly} a los colores. Su objetivo es ayudar en el análisis de cuán activo es el desarrollo de los paquetes, de modo que sea fácil identificar qué paquetes o qué partes del árbol de dependencias pueden estar abandonados o tener una actividad de mantenimiento muy baja. Una actividad absoluta saludable (ya sea el número de días desde la última actividad, o los commits o colaboradores activos durante el último año) puede ser diferente para paquetes de diferentes tamaños. Pero ver estas métricas en combinación con el tamaño del paquete ayudará a identificar paquetes probablemente abandonados o con un mantenimiento deficiente.

\begin{mdframed}[linewidth=1.2pt, backgroundcolor=gray!5, linecolor=black]
    En general, los comentarios sobre la vista de actividad sugieren que es una herramienta valiosa para identificar posibles problemas con las subdependencias y para tomar decisiones informadas sobre qué paquetes priorizar para el mantenimiento. Al proporcionar una visualización clara e intuitiva de la actividad de los paquetes, este enfoque puede ayudar a los desarrolladores a asegurarse de que sus proyectos de software sean estables, confiables y estén actualizados.
\end{mdframed}


{\bf Vulnerabilidades.} Esta vista incluye asignar \textit{nvuln} y \textit{nissues\_ratio} a los colores. \textit{nvuln} muestra el número de vulnerabilidades en el paquete (extraídas con \textit{npm\ audit}). Por lo tanto, ayuda a encontrar qué paquetes o qué ramas en el árbol de dependencias pueden ser vulnerables a problemas de seguridad. \textit{nissues\_ratio} muestra la proporción de problemas cerrados respecto al número total de problemas en el repositorio del paquete. Los problemas, mejoras o preguntas suelen ser rastreados a través de problemas (\textit{issues})~\cite{kononenko2018studying,maddila2019predicting,yu2015wait}, que se cierran cuando se resuelven. Por lo tanto, la fracción de problemas cerrados es un indicador de cómo se están gestionando los problemas en un paquete, lo que ayuda a encontrar dependencias problemáticas probables.

\begin{mdframed}[linewidth=1.2pt, backgroundcolor=gray!5, linecolor=black]
    En general, la vista de vulnerabilidades demostró ser una herramienta fundamental para garantizar la seguridad y estabilidad de los ecosistemas de software. Al proporcionar una interfaz fácil de usar e interactiva, permitió a los desarrolladores identificar y abordar rápidamente posibles vulnerabilidades de seguridad y supervisar el estado de las correcciones. Combinar esta vista con otras visualizaciones ofreció información adicional sobre la salud del ecosistema y proporcionó una imagen más completa de los posibles riesgos y problemas.
\end{mdframed}


{\bf Combinando vistas.} Los participantes encontraron valiosa la capacidad de combinar diferentes vistas para obtener información más completa sobre el ecosistema de software. La combinación de las vistas de replicación, vulnerabilidades y actividad les permitió identificar problemas potenciales y tomar decisiones informadas sobre el mantenimiento de los paquetes. También se destacó la utilidad de las características de transparencia y alambre para resaltar paquetes únicos. Estos resultados resaltan la importancia de proporcionar opciones flexibles de visualización y sugieren posibles áreas de investigación futura.

\begin{mdframed}[linewidth=1.2pt, backgroundcolor=gray!5, linecolor=black]
    En general, la retroalimentación de la evaluación del enfoque de visualización del ecosistema de dependencias indica que es una herramienta prometedora para analizar y gestionar las dependencias de software, y que existe potencial para desarrollar y refinar aún más el enfoque para satisfacer las necesidades y preferencias específicas de diferentes desarrolladores.
\end{mdframed}

\subsubsection{Conclusiones}


Los resultados mostraron la utilidad del enfoque en términos de análisis de licencias, comunidad, vulnerabilidades y uso de los paquetes. La retroalimentación posterior ayudó a identificar los puntos clave de nuestro enfoque, así como otros comentarios que tendremos en cuenta en el futuro. Si bien la retroalimentación inicial de los participantes sobre la herramienta es positiva, es fundamental realizar un estudio más completo en el futuro para validar su utilidad y eficacia. Un estudio empírico bien diseñado, con un tamaño de muestra más grande y un método de investigación adecuado, proporcionaría información más significativa sobre las fortalezas y limitaciones de la herramienta. También ayudaría a identificar áreas de mejora y determinar si las métricas seleccionadas son adecuadas para gestionar el ecosistema de dependencias. Además, un estudio empírico podría proporcionar evidencia más confiable sobre la utilidad de la herramienta y su impacto potencial en la gestión de proyectos.

