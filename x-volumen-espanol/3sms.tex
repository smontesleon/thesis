
\subsection*{La influencia de la metáfora de la ciudad y sus derivados en la visualización de software}

La metáfora de la ciudad es un enfoque popular en la visualización de software para representar sistemas de software complejos. Proporciona una forma simple e intuitiva para que los desarrolladores comprendan la estructura y las relaciones entre los componentes de software, utilizando la analogía de una ciudad con edificios, barrios y otras estructuras que representan los componentes de software. Esta metáfora ofrece una visión general de alto nivel del sistema de software, lo que facilita la identificación de problemas y deficiencias, mejora la colaboración y comunicación entre desarrolladores y apoya el mantenimiento y evolución del software. Sin embargo, también presenta desafíos, como la dificultad de representar de manera precisa sistemas de software complejos y dinámicos en una visualización estática, y puede no ser adecuada para ciertos tipos de sistemas de software. A pesar de estas limitaciones, la metáfora de la ciudad es una herramienta valiosa en la visualización de software, con potencial para desempeñar un papel importante en el futuro de esta área.

Este capítulo presenta un estudio de mapeo sistemático (SMS) sobre el uso de la metáfora de la ciudad en la visualización de software. Los SMS proporcionan una visión general estructurada y completa de la literatura existente, permitiendo identificar brechas, tendencias y oportunidades de investigación futura. Al mapear la investigación existente sobre la metáfora de la ciudad, se puede comprender cómo se ha utilizado esta metáfora en la visualización de software, cuáles son los enfoques, herramientas y técnicas más comunes, y qué derivados e implementaciones nuevas existen. Este estudio también ayuda a identificar preguntas de investigación que aún no se han abordado por completo, proporcionando una guía para futuras investigaciones. Con el creciente interés en la visualización de software y la creciente complejidad de los sistemas de software, la metáfora de la ciudad es un enfoque prometedor que merece una mayor investigación y evaluación.


\subsubsection*{Metodología llevaba a cabo para el SMS}

Un estudio de mapeo sistemático (SMS) es un tipo de método de investigación utilizado para identificar, evaluar críticamente y sintetizar toda la evidencia empírica relevante sobre una pregunta de investigación específica. Su objetivo es proporcionar una visión general exhaustiva del estado actual del conocimiento, identificar brechas en la literatura y orientar la investigación futura destacando áreas de consenso, controversia y tendencias emergentes. Siguiendo un proceso riguroso y sistemático, el estudio busca reducir el sesgo y garantizar que los resultados sean confiables y sólidos. Seguimos las pautas de SMS propuestas por Kitchenham \etal~\cite{Kitchenham} y Petersen \etal~\cite{Petersen}.

Este SMS tiene como objetivo estudiar y analizar el uso de la metáfora de la ciudad y sus derivados en el campo de la visualización de software, incluyendo artículos y herramientas. En consecuencia, formulamos las siguientes preguntas de investigación:

\smsrqI: \textbf{\smsrqIdescriptiones}

\smsrqII: \textbf{\smsrqIIdescriptiones}

\smsrqIII: \textbf{\smsrqIIIdescriptiones}

\smsrqIV: \textbf{\smsrqIVdescriptiones}


Una vez definidas las preguntas de investigación, presentamos los criterios de inclusión y exclusión para el SMS. Además, describimos la estrategia de búsqueda utilizada para los estudios primarios y herramientas, la fuente de búsqueda y las razones para eliminar artículos de la lista.

Los criterios de inclusión abordan todos los estudios publicados escritos en inglés que citan i) la publicación que presenta la primera visualización de software utilizando la metáfora de la ciudad, realizada por Munro \etal~\cite{sworld}, o ii) el trabajo presentado por Wettel \etal~\cite{codecity}, considerado un trabajo seminal en el campo.

Antes de aceptar un artículo en el SMS, excluimos publicaciones duplicadas, es decir, una versión menos madura (conferencia, taller, tesis de doctorado, etc.) de una versión más madura (generalmente una publicación en una revista). En esos casos, solo consideramos la versión más madura. Cuando encontramos una versión corta y una versión larga de la misma publicación, elegimos la versión más larga. Sin embargo, en aquellos casos en los que la publicación es una tesis de doctorado y existe una publicación relacionada (revisada por pares) en un taller, conferencia o revista, hemos descartado la tesis a favor de la última, ya que las publicaciones de conferencias y revistas están revisadas por pares y las tesis de doctorado no lo están. También se han excluido documentos que son falsos positivos (es decir, no son una publicación científica real, informe interno, etc.). Utilizamos Google Scholar tras hacer una comparación de resultados contra diferentes bases de datos, ya que en Google Scholar encontramos mayores resultados, además de indexar los resultados de las otras bases de datos analizadas.


\subsubsection*{Evaluación de la calidad del estudio}

Esta sección explica cómo obtuvimos los datos para mostrar una imagen general del uso de la metáfora de la ciudad y sus derivados en la investigación de visualización de software.

\textbf{Criterios de evaluación de calidad:} Nuestro enfoque para el estudio de la evaluación de calidad se basa en el concepto de calidad de Kitchenham y Charter~\cite{Kitchenham}. Por lo tanto, nuestra evaluación se centra en identificar solo aquellos estudios que incluyen factores relacionados con la visualización de software utilizando la metáfora de la ciudad (o un derivado de la misma).

\textit{Fase 1: Validación de la calidad del conjunto de datos:} Para validar la base de datos de estudios y su calidad correspondiente, elegimos cuatro estudios~\cite{quality1,metricity,quality3,quality4} de este campo de investigación, utilizando la consulta \textit{(codecity" OR city metaphor" OR code cit*") AND software AND (visualizat* OR 3d" )} en la base de datos de Google Scholar, y verificamos que las referencias de estos artículos estuvieran dentro de la base de datos que hemos formado. Estos cuatro estudios son de diferentes años, uno es una publicación en una revista y los otros son publicaciones de diferentes años en la conferencia VISSOFT, dedicada exclusivamente a la visualización de software.

\textit{Fase 2: Extraer solo publicaciones relacionadas con el tema:} Con esta base de datos de estudios validada en cuanto a calidad y que incluye solo las versiones más maduras si hay varias, como se describe en los criterios de inclusión, realizamos un análisis superficial de cada uno de los estudios y descartamos los estudios que no presentan visualizaciones en 3D, excepto si presentan una visualización basada en la metáfora de la ciudad o una nueva metáfora, o una clara evolución de esta en 2D. La base de datos final después de este proceso está compuesta por 169 estudios.

\textbf{Análisis de las categorías para extraer los estudios:} Antes de proceder a la lectura exhaustiva de los 169 estudios, los autores llevaron a cabo un estudio preliminar para definir las categorías y la información a extraer de los estudios. Para ello, dos autores eligieron un subconjunto de estudios, diferentes entre sí, y los analizaron en profundidad para encontrar las categorías adecuadas para extraer en todos los demás estudios. Una vez analizados estos subconjuntos, cada autor propuso una serie de categorías y, con la unión de ellas, se definió la lista final.

\textbf{Categorización de la información de los estudios:} Hemos leído y analizado los 169 estudios, extraído información de ellos y los hemos dividido en categorías. Más detalles sobre las categorías en el Capítulo \ref{chap:sms}.

\subsubsection{Resultados}

\textbf{\smsrqI: \smsrqIdescriptiones}

Nuestra muestra final incluyó 169 artículos. Según la Figura \ref{sms:fig:pubtimees}, se observa que el número de publicaciones ha aumentado a lo largo de los años, con un punto de inflexión claro en 2007. Esto se debe a la mejora de las tecnologías y a una mayor accesibilidad para desarrollar prototipos, lo que ha permitido que más investigadores experimenten y desarrollen visualizaciones de software que utilizan la metáfora de la ciudad. La tendencia muestra que, en los últimos 11 años, ha habido más de cinco publicaciones por año, lo que sugiere que este tema es estable y activo en la comunidad de investigación. Además, el análisis revela que más del 75\% de los artículos presentan avances en el estado del arte, mientras que otros exploran variaciones o mejoras de la metáfora de la ciudad utilizando la herramienta más citada, \textit{codecity}. También se destacan nuevas implementaciones, mejoras de herramientas existentes y nuevos casos de uso de la visualización de la metáfora de la ciudad o sus derivados.

\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth, keepaspectratio]{3-sms/img/studiesevol.png}
    \caption{Número de publicaciones a lo largo del tiempo.}
    \label{sms:fig:pubtimees}
\end{figure}

La importancia del diseño de la distribución en la visualización de software se muestra en la Figura \ref{sms:fig:layoutes}. Menos de la mitad de los estudios proporcionan una explicación detallada del algoritmo utilizado para la visualización de software, lo que sugiere que algunos de estos estudios pueden haber confiado en algoritmos de colocación preexistentes en lugar de desarrollar sus propios algoritmos. Sin embargo, se observa un interés continuo en desarrollar y mejorar los algoritmos de colocación, ya que al menos una publicación por año desde 2001 ha proporcionado una descripción detallada del algoritmo utilizado. Además, desde 2014, al menos dos publicaciones por año describen estos algoritmos, lo que indica un crecimiento constante y un interés continuo en este campo.

\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth, keepaspectratio]{3-sms/img/layout.png}
    \caption{Numero de publicactiones que describen el algoritmo de posicionamiento (izquierda) y como se han distribuido a lo largo del tiempo (derecha).}
    \label{sms:fig:layoutes}
\end{figure}

En resumen, la metáfora de la ciudad ha sido una herramienta poderosa para la visualización de software durante varios años. Ha habido un crecimiento constante en el número de publicaciones y la diversidad de metáforas utilizadas, aunque la metáfora de la ciudad sigue siendo predominante. Estas visualizaciones proporcionan una manera intuitiva de comprender y comunicar estructuras de software complejas, permitiendo a los usuarios explorar el código como si estuvieran caminando por una ciudad. Además, las variaciones y derivados de la metáfora de la ciudad ofrecen diferentes enfoques para visualizar sistemas de software, ampliando su accesibilidad y comprensión para una audiencia más amplia.


\textbf{\smsrqII: \smsrqIIdescriptiones}

El análisis de los artículos en este campo reveló que más del 50\% de los 265 autores únicos han publicado solo un artículo. Los autores restantes, que han publicado múltiples artículos, están principalmente concentrados en una subcomunidad de investigadores, mostrado en Figura \ref{sms:fig:networkes}. Se destaca la presencia de autores que han publicado más de cuatro artículos, incluyendo a M. Lanza, considerado uno de los principales exponentes de la comunidad, como se muestra en la Tabla \ref{sms:tab:authorsnpubses}. Aunque se observa cierta colaboración e intercambio de ideas entre investigadores de diferentes grupos, la concentración de investigadores activos en una pequeña subcomunidad plantea preocupaciones sobre la diversidad y representación en el campo.

\begin{table}[H]
    \begin{center}
        \caption{Autores con más de 4 publicaciones, junto al número total de publicaciones en este ámbito.}\label{sms:tab:authorsnpubses}
        \begin{tabular}{lr}
            \toprule
            Author               & pubs \\
            \midrule
            M Lanza              & 16   \\
            W Hasselbring        & 11   \\
            R Wettel             & 10   \\
            F Fittkau            & 7    \\
            U Erra               & 7    \\
            G Scanniello         & 7    \\
            R Minelli            & 6    \\
            JM Gonzalez-Barahona & 5    \\
            R Koschke            & 5    \\
            A Krause             & 5    \\
            D Moreno-Lumbreras   & 5    \\
            C Zirkelbach         & 5    \\
            \bottomrule
        \end{tabular}
    \end{center}
\end{table}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\columnwidth, keepaspectratio]{3-sms/img/all.png}
    \caption{Red de la comunidad que publica en este ámbito.}
    \label{sms:fig:networkes}
\end{figure}


El campo de visualización de software está experimentando un crecimiento constante, con nuevas publicaciones e innovaciones emergiendo cada año. Los investigadores están explorando enfoques como realidad aumentada y aprendizaje automático, y buscan integrar las herramientas de visualización de software en el proceso de desarrollo para facilitar el análisis y optimización del código. La demanda de visualización de software en industrias como desarrollo de software, ciberseguridad y ciencia de datos impulsa la expansión de este campo y su contribución a la informática.

En resumen, la comunidad de visualización de software muestra una concentración de autores en una subcomunidad, con una cantidad significativa de ellos publicando múltiples artículos. Aunque se observa colaboración, es importante considerar la diversidad y representación en el campo. El crecimiento continuo y las innovaciones en la visualización de software responden a la creciente complejidad de los sistemas y la necesidad de herramientas efectivas en diversas industrias.


\textbf{\smsrqIII: \smsrqIIIdescriptiones}


Se identificaron un total de 77 herramientas o enfoques en el campo de la visualización de software. Sin embargo, la mayoría de estas herramientas solo han sido mencionadas en una publicación, y solo 17 de ellas aparecen en más de una publicación, lo que indica que estas herramientas son más maduras que el resto. Un análisis de las tendencias en la producción de herramientas de software muestra un aumento constante en el número de herramientas únicas mencionadas por año, mostrado en la Figura \ref{sms:fig:diftoolses}. Esta métrica refleja el crecimiento y desarrollo del campo, así como el interés continuo y la inversión en el desarrollo de nuevas herramientas de visualización de software.

\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth, keepaspectratio]{3-sms/img/diftools.png}
    \caption{Número de herramientas únicas con una publicación, por año.}
    \label{sms:fig:diftoolses}
\end{figure}

Al examinar las publicaciones relacionadas con el uso de herramientas de visualización de software, se observa que la mayoría no hace referencia al código en la publicación misma, lo que dificulta la replicación de los estudios. Además, la disponibilidad del código fuente de las herramientas también es limitada, lo que plantea desafíos adicionales para la reproducibilidad de los estudios, mostrado en la Figura \ref{sms:fig:rq5paperses} y en la Figura \ref{sms:fig:rq5toolses}. Estos hallazgos destacan la importancia de mejorar la documentación y el intercambio de código en el campo de la visualización de software para garantizar la accesibilidad y reproducibilidad de la investigación.

\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth, keepaspectratio]{3-sms/img/rq5figure.png}
    \caption{Publicaciones con referencias a la herramienta utilizada, si la herramienta estaba disponible al momento de realizar este análisis y si la publicación hace referencia directa al código fuente.}
    \label{sms:fig:rq5paperses}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth, keepaspectratio]{3-sms/img/rq5figure2.png}
    \caption{Herramientas con al menos una referencia en todas sus publicaciones, si estaban disponibles al momento de realizar este análisis y si la herramienta tiene una referencia al código fuente en una de sus publicaciones.}
    \label{sms:fig:rq5toolses}
\end{figure}


Los avances tecnológicos han facilitado a los investigadores el desarrollo de herramientas de software, lo que ha contribuido al aumento en su producción en el campo de la visualización de software. Esta creciente disponibilidad de herramientas está impulsando el avance del estado del arte en el campo y proporcionando soluciones prácticas para que los desarrolladores de software mejoren la calidad y mantenibilidad de su código. Sin embargo, es fundamental que las herramientas utilizadas en los estudios estén referenciadas, disponibles y sean reproducibles. Esto permite a otros investigadores comprender y ampliar el trabajo existente, al tiempo que garantiza que los autores originales reciban el crédito adecuado por sus contribuciones. Además, la disponibilidad y la reproducibilidad del software utilizado en la investigación permiten la validación y prueba por parte de otros investigadores, lo que mejora la calidad y confiabilidad general de la investigación en este campo.

En resumen, el campo de la visualización de software ha experimentado un aumento en el número de herramientas producidas, pero muchas de ellas aún no se consideran maduras. La accesibilidad y disponibilidad del código fuente de estas herramientas sigue siendo un desafío importante, lo que requiere una mayor atención a la documentación y el intercambio de código. A pesar de estos desafíos, las herramientas de visualización de software siguen desempeñando un papel crucial en la mejora de la calidad y mantenibilidad del código, y se espera que el campo continúe creciendo y haciendo contribuciones significativas en el futuro.



\textbf{\smsrqIV: \smsrqIVdescriptiones}

Al examinar de cerca las Figuras \ref{sms:fig:toolsfeatures1es}, \ref{sms:fig:toolsfeatures2es} y \ref{sms:fig:toolsfeatures3es}, se evidencia que más del 80\% de las herramientas de visualización de software analizadas en este estudio están diseñadas para funcionar en una pantalla 2D convencional. El uso de tecnologías de Realidad Virtual (VR) y Realidad Aumentada (AR) en la visualización de software ha despertado un gran interés en los últimos años. Como se muestra en la Figura \ref{sms:fig:vrartoolses}, la tendencia muestra un aumento en el uso de estas tecnologías, lo cual se atribuye a su reciente aparición en el campo. Estas tecnologías ofrecen beneficios potenciales como una mayor inmersión, interactividad y participación del usuario. El potencial de estas tecnologías en la visualización de software es enorme, ya que pueden proporcionar nuevas e innovadoras formas de visualizar datos complejos, facilitando su comprensión. Además, con los avances tecnológicos recientes, los dispositivos de VR y AR se han vuelto más accesibles y su costo ha disminuido significativamente, lo que los hace más viables para su uso general. El uso de tecnologías basadas en web para VR y AR también ha aumentado, lo que permite la visualización de datos en cualquier dispositivo con un navegador web moderno. Por lo tanto, está claro que el uso de tecnologías de VR y AR en la visualización de software es un camino prometedor para la investigación, y estudios futuros podrían explorar aún más sus beneficios en este campo.

\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth, keepaspectratio]{3-sms/img/VR_AR_years.png}
    \caption{Número de herramientas que utilizaron realidad virtual o aumentada en diferentes años, a partir de 2008 en adelante.}
    \label{sms:fig:vrartoolses}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\columnwidth, keepaspectratio]{3-sms/img/rq6figure1.png}
    \caption{Diferentes características de las herramientas. Número de herramientas que funcionan en una pantalla convencional 2D (izquierda), número de herramientas que funcionan en realidad aumentada, realidad virtual o no funcionan en ninguna de ellas (centro), y número de herramientas que proponen un marco modular (derecha).}
    \label{sms:fig:toolsfeatures1es}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\columnwidth, keepaspectratio]{3-sms/img/rq6figure2.png}
    \caption{Diferentes características de las herramientas. Número de herramientas que son configurables (izquierda), número de herramientas que permiten interacción (centro), y número de herramientas que permiten navegación dentro de la visualización (derecha).}
    \label{sms:fig:toolsfeatures2es}
\end{figure}


\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\columnwidth, keepaspectratio]{3-sms/img/rq6figure3.png}
    \caption{Diferentes características de las herramientas. Número de herramientas que permiten filtrado de datos (izquierda), número de herramientas distribuidas según la cantidad de métricas disponibles para usar (centro), y número de herramientas distribuidas según la cantidad de métricas que se pueden representar simultáneamente (derecha).}
    \label{sms:fig:toolsfeatures3es}
\end{figure}

En cuanto a la cantidad de métricas disponibles y la capacidad de representar múltiples métricas simultáneamente, estas son referencias importantes en la visualización de software. En las Figuras \ref{sms:fig:toolsfeatures1es}, \ref{sms:fig:toolsfeatures2es} y \ref{sms:fig:toolsfeatures3es} se observa un gran porcentaje de herramientas que superan las cinco métricas disponibles. Sin embargo, solo unas pocas pueden representar tantas métricas simultáneamente, siendo tres métricas simultáneas la mayoría.

De nuestro análisis de las herramientas de visualización de software, observamos que el uso de entornos de realidad virtual y aumentada (VR/AR) sigue siendo un enfoque relativamente nuevo. A pesar de los beneficios significativos que VR/AR puede aportar a la visualización de software, como una mayor inmersión y una percepción mejorada, todavía existen varios desafíos que deben abordarse, como la necesidad de hardware y software especializados y el desarrollo de técnicas de interacción 3D efectivas. Sin embargo, los investigadores están explorando activamente estos desafíos y ideando nuevas formas de utilizar VR/AR para la visualización de software. A medida que avanza la tecnología y surgen nuevas tecnologías de inmersión, como la realidad extendida (XR), anticipamos que más herramientas incorporarán estas tecnologías, revolucionando potencialmente la forma en que analizamos y comprendemos el código de software. Como resultado, creemos que la tendencia actual de utilizar la tecnología XR en la visualización de software continuará creciendo y es probable que se desarrollen e implementen más herramientas en entornos XR.

\subsubsection{Conclusiones}

Nuestro estudio reveló varias conclusiones significativas. En primer lugar, se observó un crecimiento constante en el campo de la visualización de software, evidenciado por el aumento en el número de publicaciones a lo largo del tiempo. Esto demuestra un creciente interés y actividad en la comunidad de investigación. Además, la metáfora de la ciudad sigue siendo ampliamente utilizada en la visualización de software, con numerosos estudios que presentan nuevas implementaciones y variaciones de esta metáfora.

Sin embargo, también se identificaron desafíos y áreas de mejora. Por un lado, se encontró que la disponibilidad de las herramientas y las referencias al código fuente eran limitadas, lo que sugiere la necesidad de una mayor transparencia y accesibilidad en la comunidad. Además, se observó una falta de exploración de entornos más allá de las pantallas 2D, como la realidad extendida, lo que indica un área prometedora para futuras investigaciones.

En general, nuestro estudio proporciona una visión general valiosa de la evolución de la visualización de software y destaca la importancia de seguir investigando y desarrollando nuevas aproximaciones y herramientas en este campo. Esperamos que esta investigación inspire a los investigadores y desarrolladores a explorar nuevas metáforas y enfoques, y que contribuya al avance y la comprensión de la visualización de software.