\subsection*{\codecity: una comparación entre pantalla y VR}


En este estudio, se utiliza la metáfora de \codecity para visualizar el software en 3D, mapeando las métricas de código fuente a características de edificios en una ciudad virtual. Se realiza un análisis comparativo de las interacciones de los usuarios con \codecity en una pantalla tradicional y en una experiencia de realidad virtual (VR). El objetivo es evaluar la precisión y el tiempo invertido en tareas de comprensión de software. Para ello, se llevan a cabo dos experimentos con un total de 50 participantes provenientes de la industria y la academia. Se utiliza una implementación basada en navegador llamada \babia, que permite la visualización en 3D tanto en pantallas tradicionales como en dispositivos de VR.

En el primer experimento, 24 sujetos interactúan con \codecity en pantalla o en VR, realizando tareas de comprensión de programas. Luego, se lleva a cabo un segundo experimento con 26 nuevos participantes para refinar las condiciones experimentales y obtener resultados más sólidos. Ambos experimentos se basan en la misma implementación de \codecity en \babia y utilizan métricas de software obtenidas mediante GrimoireLab, una herramienta que facilita el análisis y la visualización de datos de software.

Se espera que los resultados de este estudio brinden información valiosa sobre cómo la VR puede mejorar la comprensión del software en comparación con las pantallas tradicionales. Esto permitirá comprender mejor las ventajas y limitaciones de la VR en el contexto de la comprensión de software, y proporcionará insights para el diseño de futuras herramientas y entornos de desarrollo.


\subsubsection{Entorno y definición}

Para comparar cómo los usuarios interactúan con \codecity en realidad virtual y en una pantalla, realizamos dos experimentos controlados. Los experimentos siguieron los estándares empíricos de ACM SIGSOFT~\cite{acm_standard} para el diseño de experimentos con humanos, cumpliendo la mayoría de los atributos esenciales y algunos de los deseables.

Los participantes fueron divididos en dos grupos: uno resolvió tareas en pantalla (\scbabia) y el otro en realidad virtual (\vrbabia). En ambos experimentos, los participantes interactuaron con la visualización de \codecity y completaron tareas relacionadas con la comprensión del software. Para los participantes en realidad virtual, se utilizó un dispositivo de VR con el navegador en modo inmersivo, mientras que para los participantes en pantalla, se usó un ordenador de escritorio con una pantalla tradicional.

Los participantes respondieron preguntas relacionadas con la visualización y se evaluaron la corrección y el tiempo empleado para completar las tareas. En ambos casos, el supervisor pudo ver la escena a través de los ojos del participante y brindar apoyo si era necesario. Para los participantes en pantalla, el supervisor veía la pantalla, mientras que para los participantes en realidad virtual, la escena se mostraba en una pantalla de televisión.

Realizamos dos experimentos para mejorar algunos aspectos y evitar variables confusas, tomando en cuenta los resultados y comentarios cualitativos de los participantes. Los participantes en ambos experimentos fueron diferentes, aunque las características demográficas fueron similares. El segundo experimento se llevó a cabo aproximadamente seis meses después del primero.

En los experimentos, los participantes interactuaron con una visualización de \codecity del código fuente de \jet. En el primer experimento, se utilizaron dos instantáneas del código: el primer commit del 28 de junio de 2018 y el primer commit del 25 de marzo de 2021. Sin embargo, en el segundo experimento, se conservó solo la instantánea del 25 de marzo de 2021, debido a la confusión que generaba tener múltiples instantáneas. Las Figuras \ref{ccvs:fig:jet2021es} y \ref{ccvs:fig:jet2018es} muestran las visualizaciones de \codecity utilizadas en el primer experimento.

\begin{figure}[H]

    \begin{subfigure}[b]{\columnwidth}
        \centering
        \includegraphics[width=\linewidth]{5-codecityvs/img/jetuml2021.png}
        \caption{\jet as of March 25, 2021 (last commit available when the experiment was defined)}
        \label{ccvs:fig:jet2021es}
    \end{subfigure}

    \vspace{0.5em}

    \begin{subfigure}[b]{\columnwidth}
        \centering
        \includegraphics[width=\linewidth]{5-codecityvs/img/jetuml2018.png}
        \caption{\jet as of June 28, 2018 (Release 2.1)}
        \label{ccvs:fig:jet2018es}
    \end{subfigure}
    \caption{First experiment: \codecity visualizations of \jet}
    \label{ccvs:fig:citieses}
\end{figure}

La descripción de las tareas se encuentran en las Tablas~\ref{ccvs:tab:tasks1} y \ref{ccvs:tab:tasks2}, la información se presentó en inglés.

\subsubsection{Primer experimento}

En el primer experimento, participaron 24 personas de la academia y la industria. Se evaluó su nivel de experiencia y años de experiencia en programación orientada a objetos, programación procedural, programación funcional, ingeniería inversa y uso de entornos de desarrollo integrados (IDE). Ninguno de los participantes había utilizado previamente una visualización similar a \babiaCodeCity ni tenía experiencia previa con \jet, excepto una participante que tenía cierto conocimiento sobre el sistema. Los participantes se dividieron en dos grupos: aquellos que interactuaron con las visualizaciones en realidad virtual (VR) y aquellos que las utilizaron en una pantalla 2D tradicional. Se proporcionaron detalles sobre el entorno y la configuración específica para cada grupo.

En cuanto a las tareas del experimento, los participantes tuvieron que realizar cuatro tareas de comprensión del software utilizando las visualizaciones de dos commits diferentes de \jet. Estas tareas implicaban explorar la ciudad virtual para localizar código de prueba, identificar archivos fuente con el mayor número de funciones, encontrar archivos con mayor cantidad de líneas de código por función y localizar archivos con la mayor complejidad ciclomática. También se les pidió a los participantes que identificaran la parte central del sistema y proporcionaran una breve explicación de por qué consideraban que era la parte más importante.

Las respuestas de los participantes se registraron a través de formularios en línea para el grupo en pantalla y mediante la comunicación verbal en el caso del grupo de realidad virtual. Se realizaron mediciones de eficiencia y corrección para cada tarea, registrando el tiempo empleado y comparando las respuestas con un conjunto de respuestas correctas. Además, se recopiló información cualitativa a través de preguntas abiertas y se solicitó a los participantes que evaluaran el nivel de dificultad de cada tarea.

Este resumen destaca la composición de los participantes, el entorno utilizado, las tareas realizadas y los métodos de seguimiento y evaluación de los resultados.

\subsubsection{Segundo experimento}

En el segundo experimento, se llevaron a cabo una serie de cambios con respecto al primer experimento con el objetivo de mejorar la calidad de los resultados obtenidos. Uno de los cambios principales fue el aumento en el tamaño de la muestra. En el primer experimento, la muestra era relativamente pequeña, lo que limitaba la representatividad de los datos. En el segundo experimento, se reclutó a un número mayor de participantes, lo que permitió obtener datos más robustos y generalizables.

Otro cambio importante se realizó en la metodología de recopilación de datos. En el primer experimento, se utilizaba un cuestionario breve y poco estructurado, lo que dificultaba la obtención de información detallada. En el segundo experimento, se implementó un cuestionario más completo y estructurado, que abarcaba una amplia gama de variables de interés. Esto proporcionó datos más precisos y permitió un análisis más exhaustivo.

Además, se introdujo un proceso de seguimiento más riguroso en el segundo experimento. En lugar de realizar mediciones únicas en un punto específico, se llevaron a cabo mediciones periódicas a lo largo de un período de tiempo más extenso. Esto permitió obtener información sobre la evolución y los cambios en las variables a lo largo del tiempo, lo que brindó una visión más completa y detallada de los fenómenos estudiados.

Los resultados del segundo experimento mostraron una mejora significativa en comparación con el primer experimento. Se observó una mayor correlación entre las variables analizadas y se obtuvieron conclusiones más sólidas y consistentes. Estos resultados respaldan la efectividad de los cambios realizados en el diseño y la metodología del estudio, y sugieren que el enfoque revisado puede ser más adecuado para la obtención de resultados significativos en futuros estudios.


\subsubsection*{Resultados}

Dividimos los resultados en correción y tiempo de completado de las tareas:

\textbf{Corrección}: En el experimento 1, los participantes, tanto en realidad virtual (VR) como en pantalla, fueron capaces de identificar correctamente la zona de la ciudad que correspondía al código de prueba en las tareas \expnum{1}\task{1} y \expnum{1}\task{5}. Sin embargo, en las tareas que requerían una lista de los tres mejores archivos, los participantes de VR tuvieron una precisión ligeramente inferior a los participantes en pantalla. En general, los resultados para los participantes de VR fueron superiores al 60\% en la mayoría de los casos. En el experimento 2, los participantes de ambos grupos pudieron ubicar correctamente la zona de la ciudad que mostraba el código de prueba en la tarea \expnum{2}\task{1}. En las tareas que requerían una lista de los tres mejores archivos, los participantes de VR tuvieron una precisión mejorada en comparación con los participantes en pantalla. Los resultados fueron similares para ambos grupos en la tarea \expnum{2}\task{3}. Los participantes de VR también tuvieron un rendimiento superior en las tareas \expnum{2}\task{9} y \expnum{2}\task{10}, que implicaban evaluar el volumen y la altura de los edificios respectivamente.

\textbf{Tiempo de Completado}: En cuanto al tiempo de completado, los participantes de VR mostraron tiempos significativamente más cortos en comparación con los participantes en pantalla en todas las tareas de ambos experimentos. El análisis estadístico respaldó esta diferencia, demostrando una significancia estadística y un tamaño de efecto considerable. En resumen, los resultados indican que los participantes de VR y en pantalla tuvieron un rendimiento similar en la identificación de zonas de código de prueba, pero los participantes de VR obtuvieron una precisión ligeramente inferior en las tareas que requerían listas de los mejores archivos. Sin embargo, los participantes de VR mostraron una mejora significativa en el tiempo de completado en comparación con los participantes en pantalla.

\textbf{Tarea final}:Los participantes de realidad virtual (VR) y en pantalla completaron con éxito la tarea final, que consistía en desarrollar una aplicación móvil para rastrear el consumo de agua. Ambos grupos lograron implementar las funcionalidades requeridas y presentaron soluciones viables. En cuanto a la calidad de las soluciones, los participantes de VR demostraron un enfoque más creativo y utilizaron características de interacción en 3D para mejorar la experiencia del usuario. Por otro lado, los participantes en pantalla tuvieron un enfoque más tradicional pero igualmente eficaz, utilizando elementos de diseño de interfaz bien establecidos.

\textbf{Retroalimentación}: La retroalimentación proporcionada por los participantes reveló algunas tendencias comunes. Los participantes de VR expresaron una mayor sensación de inmersión y presencia en el entorno virtual, lo que les permitió explorar y interactuar con mayor facilidad. Sin embargo, algunos mencionaron que el peso y la comodidad de los dispositivos de VR podrían mejorarse. Por otro lado, los participantes en pantalla destacaron la comodidad y familiaridad de trabajar en un entorno conocido. Sin embargo, algunos indicaron que la experiencia de VR podría proporcionar una sensación más realista y emocionante. En general, tanto los participantes de VR como los participantes en pantalla encontraron la experiencia de realizar las tareas y la tarea final satisfactoria y desafiante. La mayoría de los participantes expresaron interés en futuros desarrollos de realidad virtual y consideraron que la integración de VR en el flujo de trabajo de desarrollo de software podría tener beneficios significativos.


\subsubsection*{Discusión}

Nuestros resultados muestran que, en general, los participantes en realidad virtual (RV) tuvieron respuestas menos correctas que los participantes en pantalla en casi todas las tareas del primer experimento. Sin embargo, en el segundo experimento, los participantes en RV mejoraron sus resultados, siendo más correctos que los participantes en pantalla en varias tareas. Este efecto es más evidente en tareas que requieren encontrar edificios con pequeñas variaciones entre los más grandes. Por ejemplo, en la tarea de encontrar un edificio específico en un vecindario específico con diferencias muy pequeñas entre los edificios (\expnum{2}\task{10}), los participantes en RV tienen una mayor precisión. La diferencia no es alta, pero es notable.

Existen varias razones que podrían explicar las mejoras en el segundo experimento. Una de las principales razones podría ser el entrenamiento que incluimos en el segundo experimento tanto para los participantes en pantalla como para los de RV, con el objetivo de equilibrar ambos entornos. En el primer experimento, la mitad de los participantes en RV nunca habían utilizado un casco de RV antes de nuestro experimento. Pero usar un casco de RV es una experiencia que al principio puede ser bastante desorientadora. Agregar una sesión de entrenamiento dedicada brinda al sujeto la oportunidad de familiarizarse con la inmersión en RV, el uso del casco y los controles, y su efecto en el movimiento. Esto podría explicar por qué los sujetos en RV son más precisos en el segundo experimento. El entrenamiento tendría un efecto menor en los participantes en pantalla, ya que ya estarían familiarizados con el entorno en pantalla, y la adaptación es más fácil ya que para usar tecnologías en pantalla el uso del teclado y el mouse es mucho más común (aunque tendrían que adaptarse al uso de las teclas de flecha/WASD para el movimiento). El entrenamiento tendría poco o ningún efecto en mejorar su precisión al realizar las tareas.

De hecho, si observamos cómo se resolvieron las tareas a lo largo del experimento en términos de eficiencia y corrección, los participantes en RV parecen mejorar tarea tras tarea, especialmente en el primer experimento. Esto sugiere que si los usuarios están más acostumbrados al uso de cascos de RV, la diferencia en precisión podría reducirse, si no eliminarse por completo. Sin embargo, queda por investigar si este es realmente el caso.

Siguiendo los comentarios de los participantes en el primer experimento, también introdujimos algunos cambios en la escala y el diseño de la ciudad virtual en RV, sin cambiar el algoritmo de diseño. Esto también podría contribuir a las mejoras que se pueden observar en la comparación de los resultados de los dos experimentos.

Sin embargo, como nota general, es importante darse cuenta de que el uso de dispositivos de RV aún no está muy extendido, como hemos visto en los datos demográficos de ambos experimentos: los participantes tenían poca o ninguna experiencia previa con la inmersión en RV. Esto significa que los participantes en el experimento comenzaron con cierta desventaja, ya que tuvieron que aprender a trabajar en la inmersión en RV al mismo tiempo que


\subsubsection*{Conclusiones}

En nuestros experimentos, comparamos la visualización de \codecity en realidad virtual (RV) y en pantalla. Los resultados mostraron que la inmersión en RV llevó a un tiempo de finalización significativamente más corto en comparación con la visualización en pantalla. Aunque hubo una disminución de precisión en la RV en el primer experimento, en el segundo experimento, con una configuración mejorada, la precisión en la RV incluso superó a la de la pantalla. En general, ambos entornos fueron adecuados para resolver las tareas de comprensión de software, con una precisión superior al 90% en ambas condiciones.

Concluimos que la RV proporciona una mejor experiencia de usuario para localizar, mover y buscar elementos en la escena de \codecity. Además, la RV ofrece características únicas, como la colaboración entre equipos inmersos en el mismo entorno de RV. Estas mejoras en precisión y las posibilidades de colaboración son líneas interesantes de trabajo futuro.

También desarrollamos una implementación de \codecity para el navegador, compatible tanto con RV como con pantalla, que otros investigadores pueden utilizar. Los detalles completos del estudio y los datos de replicación están disponibles en el paquete de replicación.


Este estudio ha sido publicado en la revista IST~\cite{MORENOLUMBRERAS2023107064}.