\subsection*{BabiaXR: visualización de datos en VR para la web}

La visualización de software es un campo intensivo en recursos que requiere tiempo y esfuerzo significativos para desarrollar herramientas efectivas. En el contexto de esta tesis, gran parte de la investigación se ha dedicado al desarrollo de \babia . El objetivo de \babia es explorar el análisis de datos en el desarrollo de software dentro de entornos inmersivos de realidad virtual. Esto lo sitúa en la confluencia de múltiples áreas de investigación e interés industrial, como el análisis de datos, el análisis del proceso de desarrollo de software y la realidad virtual inmersiva. El objetivo principal de \babia es construir un sistema inmersivo completo en realidad virtual que facilite el análisis del desarrollo de software. Este sistema debe ser capaz de realizar, visualizar e interactuar con una amplia gama de técnicas de análisis comúnmente utilizadas, así como con enfoques de análisis novedosos propuestos en la literatura científica.

En cuanto a las visualizaciones de código fuente, este enfoque se ha centrado principalmente en el análisis, adaptación y mejora de \codecity, una metáfora conocida para visualizar métricas de código fuente en un entorno 3D. \codecity, introducido inicialmente por Knight y Munro en 1999~\cite{sworld}, ha inspirado diversas implementaciones, como \codecity, \textit{GoCity}, \textit{VR City} y \textit{VR City 2.0}~\cite{codecity,gocity,ref_vr_city,vegas}. \codecity representa los sistemas de software como ciudades y mapea las métricas de los artefactos de código (por ejemplo, clases, archivos) a características de los edificios (altura, tamaño, color), brindando una experiencia intuitiva y visualizando la complejidad estructural subyacente~\cite{ref_article4}.

Uno de los aportes significativos de este enfoque es la implementación de código abierto de \codecity en \babia, que se puede ejecutar en cualquier navegador moderno, incluidos los dispositivos de realidad virtual. Al hacer que las visualizaciones de realidad virtual sean accesibles a través de tecnologías web estándar, \babia busca democratizar el uso de la realidad virtual en la visualización de software. En general, \babia busca cerrar la brecha entre el análisis de datos, el análisis del proceso de desarrollo de software y la realidad virtual inmersiva al proporcionar un sistema integral para analizar el desarrollo de software en entornos de realidad virtual, aprovechando el poder de \codecity y haciendo que las visualizaciones de realidad virtual sean más accesibles.

\subsubsection{Principales caracterísicas de BabiaXR}

\babia\footnote{\babia: \babiaurl} es un conjunto de herramientas para la visualización de datos en 3D en el navegador. \babia se basa en \aframe\footnote{\aframe: \urltt{https://aframe.io}} un marco web abierto para construir experiencias en 3D, realidad aumentada (\ie AR) y realidad virtual en el navegador. \aframe extiende \texttt{HTML} con nuevas entidades que permiten construir escenas en 3D como si fueran documentos \texttt{HTML}, utilizando técnicas comunes para cualquier desarrollador web front-end. \aframe se basa en \tool{Three.js}\footnote{\tool{Three.js}: \urltt{https://threejs.org}} que utiliza la API \webgl disponible en todos los navegadores modernos.

\babia amplía \aframe proporcionando componentes para crear visualizaciones, simplificar la recuperación de datos y gestionar datos (\eg filtrado de datos o asignación de campos a características de visualización). Las escenas construidas con \babia se pueden mostrar en pantalla o en dispositivos de realidad virtual, incluidos los visores de consumo. La figura \ref{4-babia:fig:vis_babiaes} muestra un ejemplo de una escena construida con \babia. \babia es de código abierto: su código fuente está disponible en \tool{GitLab}\footnote{\babiarepo} y se puede instalar con \tool{npm}.\footnote{\babianpm}

\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth, keepaspectratio]{4-babiaxr/img/babia_example.png}
    \caption{Ejemplo de una escena de \babia}
    \label{4-babia:fig:vis_babiaes}
\end{figure}

Los componentes de \babia están divididos en las siguientes secciones:

\begin{itemize}
    \item Los componentes \textit{Querier} son responsables de realizar consultas y obtener datos de una fuente especificada.
    \item El componente \textit{Filterdata} es responsable de filtrar los datos obtenidos del \textit{Querier}. Es un componente interesado que depende del \textit{Querier} para recuperar los datos.
    \item El componente \textit{Visualizer} abarca todos los componentes responsables de visualizar los datos obtenidos del \textit{Filterdata} o del \textit{Querier}.
    \item \babia también incluye componentes para facilitar la definición de la escena para llevar a cabo experimentos. Estas características incluyen la creación automática del entorno experimental, una cámara automática para la visualización en pantalla y XR, accesibilidad del lado del cliente, medición del tiempo de finalización, registro de respuestas, registro de posición y rotación, y posicionamiento automático de tareas.
    \item Además de los componentes mencionados anteriormente, \babia incluye varios otros componentes diseñados para abordar problemas específicos e incorporar características adicionales que fueron necesarias durante el desarrollo de la tesis. Estos componentes amplían las capacidades de \babia y mejoran la funcionalidad de la plataforma.
\end{itemize}


\subsubsection{Multiusuario en \babia}

La funcionalidad multiusuario en realidad virtual es de gran importancia científica, ya que mejora la colaboración, la interacción social y las experiencias inmersivas. Permite a varios usuarios habitar simultáneamente el mismo entorno virtual, lo que abre posibilidades en educación, entrenamiento, entretenimiento y teleconferencias. Los entornos multiusuario promueven la colaboración, el aprendizaje colaborativo y mejoran la retención de conocimientos. Además, fomentan la interacción social y la sensación de presencia, permitiendo a personas dispersas trabajar juntas como si estuvieran físicamente presentes. En \babia, la funcionalidad multiusuario se logra a través del componente \textit{networked-aframe}, que establece conexiones en red y facilita la comunicación en tiempo real, brindando una experiencia inmersiva y promoviendo la colaboración en entornos virtuales.

\subsubsection*{\codecity en \babia}


\babia ofrece dos visualizaciones basadas en \codecity. Presentamos \babiaCodeCity, que sirve como una reimaginación de la \codecity original, incorporando un algoritmo diferente para la disposición de la ciudad. Además, \babiaCodeCity presenta la visualización en 3D dentro de un navegador web, ofreciendo una accesibilidad mejorada en comparación con la aplicación de escritorio \tool{Smalltalk}. Para la disposición de la ciudad, \babia utiliza un sofisticado \emph{algoritmo en espiral}, donde el elemento inicial se posiciona en el centro de la espiral, mientras que los elementos subsiguientes se disponen elegantemente alrededor de él.

Este algoritmo opera de manera recursiva en todos los niveles de la arquitectura del software, permitiendo la creación de una disposición que abarca distritos, compuestos por subdistritos, y continúa hasta que los edificios se representan en el nivel más profundo. Un ejemplo ilustrativo de una escena de \babiaCodeCity se muestra en la \figref{4-babia:fig:babia_gltfES}.

\begingroup
\begin{figure}[H]
    \centering
    \includegraphics[width=0.95\columnwidth, keepaspectratio]{4-babiaxr/img/city_example.png}
    \caption{Ejemplo de una escena de \babiaCodeCity}
    \label{4-babia:fig:babia_gltfES}
\end{figure}
\endgroup

Las escenas de \babiaCodeCity son interactivas, brindando a los usuarios una experiencia fluida. Al pasar el cursor sobre un edificio, aparece rápidamente un tooltip que muestra el nombre del artefacto de software correspondiente junto con sus valores métricos asociados (por ejemplo, número de funciones, líneas de código y Número de Complejidad Ciclomática~\cite{complexity}). El tooltip desaparece convenientemente cuando el cursor se aleja del edificio, pero los usuarios tienen la opción de habilitar tooltips fijados haciendo clic en un edificio de interés. Para obtener información sobre un distrito (es decir, una carpeta), los usuarios simplemente pueden hacer clic en él. Estas características interactivas son consistentes tanto si los usuarios interactúan con \babiaCodeCity en una pantalla (usando el cursor del mouse) como en realidad virtual (usando el controlador de un auricular de VR como cursor). Al igual que en la \codecity original, \babiaCodeCity aprovecha la asignación de valores métricos de software a características distintivas en la visualización. En la versión actual de \babiaCodeCity, cada edificio corresponde a un archivo, con su área base proporcional al número de funciones, su altura representa las líneas de código por función, y su color refleja el valor de la Complejidad Ciclomática (transicionando gradualmente de azul a rojo en una escala de colores).