\section{Amenazas a la validez}
\label{section:validez}

\cite{Wohlin2012} identifica cuatro principales tipos de amenazas a la validez en la investigación empírica en ingeniería de software: constructo, interna, externa y de conclusión.

\textbf{La validez de constructo}:~se refiere a la adecuación de las métricas utilizadas en el enfoque de investigación empírica y su capacidad para ser cuantificadas. En el marco de esta tesis, este aspecto de la validez se centra en las medidas y conceptos empleados, así como en los posibles sesgos que podrían influir en los resultados de las métricas durante el proceso de obtención de datos.

\textbf{La validez interna}:~se relaciona con el grado en que una conclusión causal está justificada, lo cual depende de la medida en que un estudio minimiza los errores sistemáticos. En este sentido, se han seguido los procedimientos para llevar a cabo una SLR descritos por ~\cite{madeyski2017}, reduciendo al mínimo la interacción con herramientas que puedan sesgar los resultados. Además, se proporcionan paquetes de replicación para permitir que terceros inspeccionen las fuentes.

La \textbf{la validez externa}:~se refiere al grado en que los resultados de la tesis pueden generalizarse a otros contextos. El estudio empírico se centra en un caso específico: ``Identificación y clasificación de licencias FLOSS'', por lo que no se puede afirmar que los resultados sean generalizables a otros proyectos. No obstante, se destaca la importancia de no subestimar el valor de estos estudios de caso para futuras investigaciones.

\textbf{La validez de la conclusión}:~se relaciona con la fiabilidad de las conclusiones alcanzadas en relación con las relaciones entre los datos. Este aspecto no afecta el enfoque de la investigación.


\subsection{Validez interna}
\label{subsection:validezint}
Las amenazas internas a la validez son:

\begin{enumerate}
	\item En la Revisión Sistemática de la Literatura:
	\begin{itemize}
		\item Puede haber un sesgo de selección por haber elegido IEEE, Scopus y WoS como fuente de todas las publicaciones sobre identificcación y clasificación de licencias; pueden existir otras publicaciones que no están indexados por estas revisas, a diferencias de otras como Google Scholar que indexa pre-prints.
		\item Es posible que existan publicaciones que utilicen y/o desarrollado algún algoritmo o herramienta, y no citen la publicación original o sus mejoras.
		\item La madurez del campo también podría afectar si el estudio se realizó en una etapa demasiado temprana para sacar conclusiones. Sin embargo, con más de 22 años es suficiente tiempo que permita extraer lecciones válidas de su estudio.
		\item El estudio ha permitido identificar los componentes o técnicas utilizadas en el desarrollo de software a la hora de identificar licencias FLOSS como el de \cite{Ombredanne2020}, así como la aplicación de ML para la clasificación.
		
		
	\end{itemize}
	
	\item En el Estudio Empírico de Anotación de licencias (conjunto de datos): 
	\begin{itemize}
		\item Los conjuntos de datos construidos a partir de grandes cantidades de datos del mundo real 
		tienden a ser ruidosos y contener datos falsos (archivos que no son licencias, en este caso) 
		entre datos legítimos. En este conjunto de datos, en lugar de intentar limpiar exhaustivamente 
		los documentos de licencias, incurriendo en el riesgo de falsos negativos, hemos decidido \emph{aumentarlos} 
		con metadatos adicionales que permitan a los investigadores filtrar datos posteriormente.
		\item Se ha descrito en la Sección~\nameref{chap:dataswh} cómo restringir el patrón de nombres 
		de archivo si se desea. De manera similar, los investigadores pueden filtrar por 
		tipos MIME (por ejemplo, si solo están interesados en archivos de texto) o por métricas de 
		longitud (por ejemplo, solo mantener archivos de una sola línea para centrarse en copyright o etiquetas SPDX machine-readable). 
		\item Una pequeña inconsistencia en el conjunto de datos proviene de la incompletitud de los 
		metadatos relacionados con el origen (orígenes de las muestras y número de orígenes), que 
		faltan en una fracción muy pequeña de documentos en el conjunto de 
		datos ($\DataBlobNoOriginCount{}$ blobs, o $\DataBlobNoOriginPct$\% del corpus completo). La 
		cantidad es tan marginal que no la consideramos una amenaza significativa para la validez.
		\item Además, debido a la facilidad de falsificar marcas de tiempo en Git~\cite{Flint2021, Rousseau2020}, algunos 
		metadatos del primer commit son falsos, con marcas de tiempo establecidas en la época UNIX. Tanto 
		la cobertura de los metadatos (que sigue siendo muy alta) como la calidad de las marcas de tiempo 
		pueden mejorarse cruzando los blobs de licencias con fuentes de datos externas gracias a los 
		identificadores persistentes utilizados en el conjunto de datos como claves.
		
	\end{itemize}
	
	
	
%	\item 
\end{enumerate}

\subsection{Validez de constructo}
\label{subsubsection:validezconst}
La validez de constructo es el grado en que un trabajo de investigación representa lo que se espera que medida. En el contexto de esta tesis:

\begin{enumerate}
	\item En la SLR:  
		\begin{itemize}
			\item El impacto del proceso de identificación y clasificación de licencias FLOSS se mide por el número de publicaciones por año que lo utilizan, así como por los tipos y la diversidad de lugares que publican dichos manuscritos.
			\item La reproducibilidad de las publicaciones que emplean el proceso de identificación y clasificación de licencias FLOSS se mide por la disponibilidad de una descripción detallada y un conjunto de datos o un paquete de replicación.
			\item Dado que los estudios sobre identificación y clasificación de licencias FLOSS no han sido replicados o reproducidos en esta SLR, se debe considerar el efecto en la reproducibilidad. Sin embargo, para mitigar esto, se enumeran un límite superior de publicaciones que pueden ser reproducibles.		
			
		\end{itemize}
	\item En el Estudio Empírico de Anotación de licencias (conjunto de datos): 
		\begin{itemize}
			\item No hay garantía de que todos los blobs de licencias en el conjunto de datos contengan textos de licencias considerados open/free por OSI/FSF, solo que provienen de código público. Si se acepta confiar en ScanCode como verdad absoluta, los metadatos de \texttt{scancode} en el conjunto de datos se pueden utilizar para filtrar licencias aprobadas por OSI/FSF. Para hacerlo, la lista de licencias SPDX machine/readable\footnote{\url{https://spdx.org/licenses/}, accedido el 2022-11-10} puede asociar los valores SPDX en el conjunto de datos para identificar aquellas licencias consideradas Software Libre por la FSF y que están aprobadas por la OSI. De lo contrario, las determinaciones de software open/free  deberán ser realizadas de forma independiente por los usuarios del conjunto de datos.
			\item Se considera nombres de archivos que probablemente incluyan información sobre licencias y, en la medida de lo posible, solo información sobre licencias. Por lo tanto, hemos omitido archivos que en muchos casos \emph{también} incluyen información sobre licencias, como archivos \texttt{README} o \texttt{README.md}, pero que en muchos otros no lo hacen y usualmente incluyen otro texto no relacionado con licencias. También hemos omitido archivos de código fuente, que en muchos casos incluyen un texto o aviso de licencia como un comentario en el encabezado del archivo. Esto significa que el conjunto de datos no incluye todos los archivos en \SWH con información sobre licencias, sino solo aquellos que consideramos más probables de tener información sobre licencias. 
			\item Los textos de licencias y los avisos de licencias que solo aparecen dentro de archivos fuente o en archivos con nombres no capturados por las presentes heurísticas tienen muchas probabilidades de estar subrepresentados en el conjunto de datos. Esto se aplica, por ejemplo, tanto al aviso recomendado de GPL {\em ``This program is free software [\ldots] under the terms of the GNU General Public License [\ldots]''} como a las etiquetas SPDX~\cite{Stewart2010} como ``SPDX-License-Identifier: GPL-3.0-or-later'' cuando se incluyen solo como comentarios al principio de los archivos de código fuente. Se considera que incluir esos archivos aumentaría significativamente el ruido en el conjunto de datos, al incluir archivos sin información de licencias. Además, identificar de manera fiable las licencias en esos casos sería más difícil, ya que están mezcladas con otros textos de naturaleza muy variada.
			%\item 
		\end{itemize}
	%\item 
\end{enumerate}

%Analizamos manualmente 43 de 1... artículos durante la SLR y 8 mil ..archivos de posibles licencias para etiquetarlas durante el estudio empírico, por lo que podrían haberse producido errores humanos. Esta tesis proporciona paquetes de replicación con los datos brutos utilizados en el SLR y el estudio empírico. Así, los investigadores tienen la posibilidad de mejorar, comprobar y replicar el trabajo de esta tesis.


\subsection{Validez externa}
\label{subsection:validezext}
Las amenazas externas más importantes están relacionadas con las peculiaridades de los proyectos:

\begin{enumerate}
	\item En la SLR:
		\begin{itemize}
			\item Los resultados sobre identificación y clasificación de licencias FLOSS no se pueden generalizar a  en investigación de ESE porque era un caso de estudio.				
		\end{itemize}
	\item En el Estudio Empírico de Anotación de licencias (conjunto de datos):
		\begin{itemize}
			%\item El uso de diff es la forma más extendida de proporcionar información de diferencias cuando se busca la diferencia entre dos archivos. Sin embargo, otras formas se debe considerar para proporcionar diferencias de la información.
			\item Por su propia naturaleza, el conjunto de datos proporciona una instantánea incompleta de la realidad; por lo tanto, no se afirma tener plena generalidad ni representatividad de todas las variantes de licencias existentes. La realidad es un objetivo en movimiento, con nuevas variantes de licencias que se publican constantemente como parte del código público. El archivo del que se partió tampoco es que abarca completamente un todo. Aún así, y hasta se conoce, este es hasta la fecha el conjunto de datos más grande disponible públicamente de variantes de licencias (de código público). A Futuro Software Heritage plantea mitigar este riesgo al hacer disponibles periódicamente nuevas versiones del conjunto de datos.
			%, como hemos hecho hasta ahora y una vez más con la nueva versión del conjunto de datos documentada en este artículo.
		
			
		\end{itemize}
%	\item 
\end{enumerate}


