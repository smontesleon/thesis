\section{Respuesta a las preguntas de investigación }
\label{sec:resppreguntasidfloss}% sec:findings

En esta sección se presentan las respuestas a las preguntas de investigación planteadas en la subsección~\nameref{sec:preguntasidfloss}, basadas en el análisis cuantitativo y cualitativo del conjunto de datos.


\subsection{RQ1: ¿Cuántas licencias distintas contiene el conjunto de datos?}
\label{subsec:findings-rq1}

\revchange{Para comprender esta \textbf{RQ},  el interés es saber cuántos de los archivos en la ``colección de documentos'' contienen el texto completo de una sola licencia. Dado que cada archivo en la colección es (por construcción del conjunto de datos) diferente, el número de estos archivos será el número de textos de licencia distintos (con variaciones muy pequeñas o muy grandes entre ellos).}

\revchange{
	La respuesta a esta pregunta mediante la anotación manual del subconjunto aleatorio. Se lee todos los documentos en esta muestra y se evalúa cuáles correspondían al texto completo de una licencia y no contenían otra información de licencia. Como se presenta en la subsección~\nameref{subsec:descripdatasetmanual}, la fracción de archivos con el texto completo de una sola licencia es del $56.87$\%. Se considera esto como una distribución binomial (con 1 para \texttt{licen}=Verdadero (true) y \num{0} para \texttt{licen}=Falso (false)), se puede calcular su media ($0.5687$) y su desviación estándar ($0.4952$). Con estos números, permite calcular el margen de error para un nivel de confianza del $99$\% (usando $2.575$ como valor z). El margen de error resultante es $0.0107$, o $1.07$\%. En otras palabras, permite estimar que el número de archivos compuestos por el texto de una sola licencia es del $56.87$\%, con un margen de error del $1.07$\% (ver Tabla~\ref{tab:documents-single-license}).
}

\begin{table}
	\caption[Documentos con un solo texto de licencia]{Documentos con un solo texto de licencia en la muestra y en toda la colección, con estimación de error (nivel de confianza del $99$\%)}
	
	\label{tab:documents-single-license}
	\centering
	\begin{tabular}{l|r|r}
		\textbf{Conjunto}   & \textbf{Total de documentos} & \textbf{Documentos con texto de licencia único}  \\ \hline
		Muestra & \DocumentsManualCount & $56.87\%\pm1.07\% (4608\pm87)$ \\
		Colección & $6.859$ millones &  $56.87\%\pm1.07\% (3.900 \mbox{ millones} \pm \num{73391})$ \\
	\end{tabular}
\end{table}

Es importante notar que este resultado es un límite inferior del número de documentos con licencia completa publicados alguna vez. Se entiende con certeza que el número podría ser mayor, porque también tenemos (en nuestra colección) licencias de texto completo en documentos con múltiples licencias. Pero aún no se ha analizado cuántas otras licencias de texto completo diferentes hay en ellos. Y, por supuesto, podría haber algunas otras licencias de texto completo diferentes en documentos que no están en la colección. Esto puede ser porque están en documentos de \SWH con nombres de archivo no capturados por la consulta aplicada, o porque no están archivados en \SWH.

%Sin embargo, este número es realmente alto. 
En comparación con las aproximadamente \num{500} licencias reconocidas por SPDX, o las \num{2000} licencias en la ``ScanCode LicenseDB'', el número de variantes de licencias de texto completo en nuestra colección es enorme. 

%Con \textbf{RQ2} se explora algunas de las razones que llevan a esta diversidad.

\subsection{RQ2: ¿Por qué hay tantas licencias distintas en el conjunto de datos?}

%Se responde a esta pregunta utilizando el siguiente enfoque:

La Inspección manual de la muestra permitió identificar que muchos documentos eran similares a las licencias ``canónicas'' (por ejemplo, aquellas reconocidas por SPDX). En particular, se identifica una cantidad muy grande de variantes de la licencia MIT, que en muchos casos solo difieren en los avisos de derechos de autor: debido a la estructura de la licencia, es habitual que un archivo de licencia con una mención de la licencia MIT incluya uno o más avisos de derechos de autor. Esta es la causa de muchas variantes ligeramente diferentes del texto de la licencia.

Los cambios leves en el texto de la licencia ocurren para todas las licencias, pero ocurren con menos frecuencia en licencias más largas, como las versiones de GPL. Una razón es que la licencia GPL suele estar almacenada en un solo archivo, con avisos de derechos de autor en otros archivos; esta también es la práctica recomendada por la licencia misma. En licencias más cortas no hemos encontrado esto. Por ejemplo, el texto de la licencia MIT (y también de otras licencias más cortas, como BSD) es lo suficientemente pequeño como para ser incluido junto con los avisos de derechos de autor.

Se cuantifica el número de variantes usando ScanCode, y se encuentra que existen \num{3031} variantes de la licencia MIT en nuestra muestra de un total de \num{4248} licencias, o $71.35$\% del total de variedad de documentos. 

La siguiente licencia con más variantes es BSD, que sumando las variantes de BSD-2-clauses y BSD-3-clauses, da \num{288}, o $6.77$\%. En contraste, GPL-2 y GPL-3 juntas solo dan \num{128} documentos, o $3.01$\%.

Solo como ilustración de las muchas formas en que se puede encontrar la licencia MIT (y otras), consideramos el contenido de tres archivos diferentes en la Muestra Anotada: (i) el texto de la licencia MIT, en un archivo con un encabezado en japonés y una lista de créditos; (ii) una plantilla HTML que incluye el texto de la licencia MIT; y (iii) la licencia MIT como texto citado.

Resumiendo, se ofrece una evidencia de los avisos de derechos de autor de una sola línea, los espacios en blanco y las mayúsculas/minúsculas son una causa muy importante de la diversidad de documentos que tienen licencias de texto completo. Sin embargo, aún se necesita más trabajo para averiguar exactamente en qué casos los documentos son realmente diferentes en licencias, no solo diferentes en aspectos ``cosméticos'', sino en el texto real del texto de la licencia.


\subsection{RQ3: ¿Qué patrones existen para que existan tantas licencias?}
\label{subsec:findings-rq3}

Para poder responder a esta pregunta, nos basamos en la ``Normalización del texto en los documentos''. Decidimos verificar si la hipótesis de que los avisos de derechos de autor son la causa de una gran cantidad de documentos al ejecutar los documentos a través de algunas heurísticas de normalización. Estas heurísticas eliminan la mayoría de los avisos de derechos de autor encontrados en una sola línea (es decir, líneas con variantes de la palabra ``\textit{copyright}'', tal vez una lista de palabras, y tal vez un año, como en ``\textit{Copyright 2012 The Foo Project Developers}'').

Estas heurísticas también normalizan a minúsculas y eliminan caracteres en blanco. Al usarlas, se encuentra que el número total de documentos diferentes en la muestra anotada se redujo a \DocumentsManualNormalizedCount, de un total de \DocumentsManualCount{} ($53.94$\%).

Extendiendo estos resultados a toda la colección, eso significaría una reducción de aproximadamente $\DataBlobCountApproxM\ $, millones de documentos a alrededor de $3.7$ millones de documentos. Estas heurísticas, por su construcción, reducen la diversidad principalmente en documentos con texto de licencia. Teniendo esto en cuenta, junto con nuestra estimación anterior del número total de archivos con un solo texto de licencia como $3.9$ millones, podemos afirmar que las variaciones en mayúsculas/minúsculas y espacios en blanco en los avisos de derechos de autor son la principal causa de variedad en documentos con texto de licencia.

\emph{Anotación automática con ScanCode}. ScanCode verifica patrones en el texto que identifican licencias. El hecho de que ScanCode identificara con precisión una fracción muy grande de las licencias significa que esos patrones funcionaron bien, y que esos textos de licencia identificados deberían ser similares a las licencias ``canónicas''. De hecho, el $70$\% de las licencias en el conjunto de datos son identificadas por ScanCode con una puntuación de 100 en una escala de 0 a 100; solo el $15$\% con una puntuación igual o menor a 90; la puntuación de detección promedio en todo el conjunto de datos es 93. Nuevamente, esto apunta en la dirección de que pequeñas variaciones (en avisos de derechos de autor, espacios en blanco, etc.) son la principal razón de la diversidad de licencias.

\subsection{RQ4: ¿Qué tipo de licencias peculiares se ha identificado?}
\label{subsec:findings-rq4}

La respuesta a esta pregunta se obtiene en base a la fracción de archivos con el texto completo de una sola licencia que es del $56.87$\%, de la muestra anotada. La inspección manual de la muestra permitió que se identificara que muchos documentos no eran similares a las licencias ``canónicas'' (por ejemplo, aquellas reconocidas por SPDX). Hemos identificado textos que se pueden considerar licencias por su estructura, como se muestra en la sección~\nameref{subsec:licenparticular} considerándolos como licencias peculiares cómo: i) Licencia LGPL-3.0, ii) Con algunas notas para donaciones y oferta de una licencia comercial Figura~\ref{fig:cases-donation1}), iii) Licencia específica, iv) licencia claramente privativa (ver Figura~\ref{fig:cases-proprietary1}), vi) Licencia MIT escrita en HTML, vii) Licencia en chino, viii) en formato HTML., ix) Licencias como cadenas en código C++, x) dankware license v1 (ver Figura~\ref{fig:dankware}).

\subsection{RQ5: ¿Cómo se consideran los archivos que no son licencias?}
\label{subsec:findings-rq5}

\revchange{
	La respuesta a esta pregunta se obtuvo mediante la anotación manual del subconjunto aleatorio. Se leyeron todos los documentos en esta muestra y se evaluó cuáles no contenían texto de licencia alguno. Como se presenta en la subsección~\nameref{subsec:filenolicenses}, la fracción de archivos sin texto de licencia es del $27.17$\%. Considerando esto como una distribución binomial (con 0 para \texttt{nlicense}=Falso (false), \texttt{notice}=Falso (false), \texttt{multi}=Falso (false), \texttt{copy}=Falso (false) y \num{1} para \texttt{selse}=Verdadero (true)), se puede calcular su media ($0.2717$) y su desviación estándar ($0.4453$). Con estos números, se puede calcular el margen de error para un nivel de confianza del $99$\% (usando $2.575$ como valor z). El margen de error resultante es $0.0154$, o $1.54$\%. En otras palabras, se puede estimar que el número de archivos que no contienen texto de licencia es del $27.17$\%, con un margen de error del $1.54$\% (ver Tabla~\ref{tab:nolicense}).
}


\begin{table}
	\caption[Documentos que no son licencias]{Documentos que no son licencias  en la muestra y en toda la colección, con estimación de error (nivel de confianza del 99\%)}
	
	\label{tab:nolicense}
	\centering
	\begin{tabular}{l|r|r}
		\textbf{Conjunto}   & \textbf{Total de documentos} & \textbf{Documentos no son licencia}  \\ \hline
		Muestra & \DocumentsManualCount & $27.17\%\pm1.54\% (2202\pm125)$ \\
		colección & $6.859$ millones & $27.17\%\pm1.54\% (1.864 \mbox{ millones} \pm \num{105,587})$ \\
	\end{tabular}
\end{table}


Lo que se ha descrito es una razón principal para comprender existe tantos archivos diferentes en el conjunto de datos, sino también algo a tener en cuenta al usar el conjunto de datos. Para la mayoría de los análisis al respecto, todos esos archivos ``\texttt{other}'' deben excluirse, o cuando eso no sea posible, tenerlo en cuenta al discutir resultados.

