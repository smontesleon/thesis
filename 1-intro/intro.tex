\chapter{Introducción}
\label{chap:introduc} 

\section{Contextualización}
\label{sec:context}

En los años de 1960, cuando industrias, bancos e instituciones educativas y de investigación podían permitirse adquirir un computador (\textit{hardware}), estos eran desarrollados por empresas como IBM e incluían el software necesario. Mientras el usuario mantuviera un contrato de soporte, podía acceder al catálogo de software proporcionado por el fabricante. En consecuencia, no se solía percibir a los programas como productos independientes desde una perspectiva comercial~\cite{Haigh2002Software}. Esto comenzó a cambiar en 1969, cuando IBM separó el hardware de los programas informáticos en sus ventas de ordenadores. Los programas empezaron a comercializarse bajo diversos términos de uso, conocidos como ``licencias de software''. Los usuarios habituales sabían que debían pagar por un sistema operativo, vendido por empresas como Microsoft, y por otros programas necesarios para usar el ordenador. Así comenzó a consolidarse el software comercial, que más tarde se denominaría software privativo, debido a las restricciones impuestas sobre su uso.

En 1984, Richard Stallman estableció la \textit{Free Software Foundation} (FSF) con la finalidad de poner a disposición del público software libre~\cite{Stallman2002}. Stallman promovía que el software debía permitir a los usuarios: i) ejecutarlo, ii) estudiarlo, iii) copiarlo y iv) modificarlo. Para garantizar esto, sugirió que dichos términos se declararan en un archivo junto al código fuente, lo que dio origen al concepto de software libre. Estos principios quedaron formalizados entre las primeras licencias de software libre, la Licencia Pública General (GNU-GPL) en 1989~\cite{lawrence2004open}.

Posteriormente, surgieron otras iniciativas y proyectos, como \textit{Open Source Initiative} (OSI), Linux Foundation, Debian y Creative Commons, etc., que promovieron la distribución de software bajo distintos términos. Aunque algunos de estos términos son similares, especialmente en cuanto a la condición de compartir el código fuente, se dio paso al surgimiento de numerosas licencias para distribuir el software. Las licencias de software libre y \textit{open source} más conocidas se denominan licencias canónicas. A lo largo de este texto, nos referiremos al software que utiliza diversas licencias bajo este concepto como \textit{Free Libre y Open Source} (FLOSS). Los desarrolladores, guiados por estas licencias y sus condiciones, suelen adaptarlas a sus necesidades, lo que ha generado una gran diversidad de licencias~\cite{laurent2004}. 

Internet permitió inicialmente el intercambio de software a través de diversas vías, entre ellas los repositorios, que se han convertido en herramientas clave para almacenar y gestionar código. Entre los repositorios más conocidos se encuentran Git, Subversion (SVN) y Mercurial, utilizados tanto a nivel local como en plataformas en la nube. Estos repositorios dieron lugar a las denominadas forjas de desarrollo, que no solo permiten crear software, sino también ofrecen múltiples funcionalidades, como la compartición eficiente del código. Las forjas han optimizado la distribución del código fuente mediante plataformas especializadas. Entre las forjas más conocidas se encuentran SourceForge, GitHub, GitLab y Bitbucket, que se han consolidado como pilares fundamentales en el ecosistema del desarrollo de software en general, y del FLOSS en particular.

Las forjas de desarrollo se han convertido en un pilar esencial del avance tecnológico, extendiéndose a diversos ámbitos, desde el desarrollo web hasta la investigación científica y académica, impulsadas por las ventajas que ofrecen en términos de flexibilidad, transparencia y colaboración. Sin embargo, su crecimiento exponencial ha planteado nuevos desafíos en cuanto a la gestión y cumplimiento legal, especialmente en lo relativo a las licencias que regulan su distribución.

En los últimos años, el interés por las licencias de software ha aumentado significativamente entre los desarrolladores, quienes necesitan comprender mejor la legalidad de las licencias, dado que muchos de sus componentes incluyen código FLOSS. Se estima que actualmente más del 50\% del software existente~\cite{Ballhausen2019} contiene código FLOSS, por lo que es crucial entender cómo identificar las licencias en los repositorios para garantizar la legalidad y los derechos de autor.

Actualmente, contamos con escasos estudios que aborden la problemática de la identificación y clasificación de licencias FLOSS. Con el fin de proporcionar a la industria y a la academia una referencia sobre este tema, se ha considerado pertinente realizar un estudio sobre la ``\textit{Diversidad de licencias en Software Libre}''.


\section{Motivación}
\label{sec:motiva}

Hoy en día en el desarrollo de software, especialmente en FLOSS, es muy común y popular reutilizar componentes o incorporar fragmentos de código fuente, agregando valor en términos de funcionalidad y calidad. Esto implica lidiar con licencias de software para respetar dichos términos y derechos de autor. El incumplimiento de las licencias en el desarrollo de software FLOSS conlleva pérdida de reputación y altos costos de litigios para las organizaciones o la industria. Es importante considerar la existencia de varias herramientas para identificar el cumplimiento de términos de licencias FLOSS, pero desconocemos cuán robustas sean esas herramientas y cuál es la más idónea para llevar a cabo esa tarea, contribuyendo así a mantener la integridad y legitimidad de la comunidad de desarrollo de software.

La verificación se centra en las denominadas licencias canónicas, que se describen en la iniciativa \textit{Software Package Data eXchange} (SPDX).\footnote{\url{https://spdx.org/licenses/}}~La falta de herramientas que aborden los términos de licencias en un contexto más amplio dificulta la identificación y clasificación de licencias FLOSS, incluso en los repositorios de software. Por lo tanto, en el campo de la identificación y clasificación de licencias FLOSS, existe una clara ausencia de estudios exhaustivos que aborden estos desafíos de manera integral. 

Por ello, esta investigación cobra una importancia significativa, ya que busca no sólo llenar este vacío de conocimiento, sino también contribuir con un estado del arte sobre el tema, identificación de las mejores herramientas, metodologías más efectivas para la gestión (identificación y clasificación) de licencias en el entorno del desarrollo de software, y en particular las razones detrás de la gran variedad de licencias FLOSS. Al abordar estos desafíos, se espera que esta investigación no sólo beneficie a la academia, organizaciones y a la industria en términos de cumplimiento legal y reputación, también promueve la innovación y el desarrollo tecnológico en la comunidad global de desarrollo de software.

\section{Objetivos}
\label{sec:objetiv}

%Para cumplir con los objetivos de la tesis, se definen a continuación.
\subsection{Objetivo general}
\label{subsec:objetiv1}
Analizar y describir la mayor cantidad y diversidad de licencias FLOSS, clasificarlas y estudiarlas para conocer sus diferencias.

 \subsection{Objetivos específicos}
 \label{subsec:objetiv2}
 Para conseguir el objetivo principal, se han definido objetivos específicos que son un apoyo para llevar a cabo el estudio planteado:
 \begin{itemize}
 	\item Describir el estado del arte respecto a la identificación y clasificación de licencias.
 	\item Conocer la evolución de las técnicas aplicadas.
 	\item Identificar herramientas existentes.
 	\item Analizar un conjunto de documentos con tantas licencias posibles.
 	\item Clasificar todos los documentos con anotaciones que contengan 1 sola licencia.
 	\item Obtener un conjunto de datos con un análisis detallado de licencias.
 	 	 
 \end{itemize}


\section{Contribución}
\label{sec:contribu}
Las aportaciones principales de esta tesis se pueden sintetizar de la siguiente manera:
\begin{enumerate}
	\item  Un estudio que aborda como ha evolucionado la identificación y clasificación de licencias FLOSS, para lo cual se aplica una SLR que ha permitido proporcionado los siguientes resultados:
		
	\begin{itemize}
		\item Identificar técnicas empleadas para la identificación de licencias.
		\item Técnicas de evaluación.
		\item Identificar herramientas.
		\item Conjunto de datos como producto de la aplicación SLR.
	\end{itemize}
	
	
	
\item  Un análisis detallado de un conjunto de documentos de licencias proporcionado por Software Heritage, que ha permitido poner a disposición de la comunidad científica lo siguiente: 	
	\begin{itemize}
		\item Análisis detallado de un conjunto de documentos de licencias.
		\item Identificar por qué tanta diversidad de licencias.
		\item Análisis de muestras pormenorizado. 
		\item Clasificación.
		\item Variantes de licencias MIT.
		\item Resultados de un conjunto de datos con detalle.
	\end{itemize}

\end{enumerate}


\section{Estructura}
\label{sec:estructura}

La estructura restante de esta tesis se expone de la siguiente manera:

El \chapref{chap:concep}, ``\nameref{chap:concep}'', ofrece una descripción de los diferentes conceptos teóricos que involucran el desarrollo de la investigación, tales como software libre, código abierto (\textit{open source}), propiedad intelectual, derechos de autor y licencias FLOSS. Adicionalmente, se describen los tipos, la proliferación y la compatibilidad de las licencias.

El \chapref{chap:repositorios}, ``\nameref{chap:repositorios}'', presenta conceptos, clasificación y ejemplos de repositorios, destacando la importancia de identificar el lugar donde se almacena y publica la información de licencias FLOSS.

El \chapref{chap:slr}, ``\nameref{chap:slr}'', desarrolla una SLR respecto a la ``identificación y clasificación de licencias FLOSS'', esto facilita comprender la situación actual del tema investigado.

El \chapref{chap:dataswh}, ``\nameref{chap:dataswh}'', describe la fuente de datos y el trabajo realizado con el repositorio de `` Software Heritage'', que ayuda a comprender mejor las licencias utilizadas en el código fuente disponible públicamente.

El \chapref{chap:metodologia_dataset}, ``\nameref{chap:metodologia_dataset}'', desarrolla un estudio empírico para conocer la diversidad de licencias FLOSS, fundamentado en una anotación manual, partiendo del conjunto de datos de la iniciativa ``Software Heritage''.

%El \chapref{chap:resuls}, ``\nameref{chap:resuls}'', se muestran los resultados de la diversidad de licencias FLOSS, en inglés.



El \chapref{chap:discussion}, ``\nameref{chap:discussion}'', detalla las amenazas a la validez de esta tesis y discute cada uno de los resultados obtenidos.


El \chapref{chap:conclusions-es}, ``\nameref{chap:conclusions-es}'', presenta las conclusiones y posibles enfoques para trabajos futuros.

Finalmente, en el \chapref{chap:conclusions}, ``\nameref{chap:conclusions}'', presenta las conclusiones y posibles enfoques para trabajos futuros, en inglés.

